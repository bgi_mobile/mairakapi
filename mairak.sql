-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2019 at 07:27 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mairak`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user_id` int(30) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `user_name` varchar(60) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_email` varchar(60) NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `user_created_date` varchar(60) NOT NULL,
  `updated_by` varchar(250) NOT NULL,
  `updated_at` varchar(250) NOT NULL,
  `user_role` varchar(60) NOT NULL,
  `user_login_time` varchar(250) NOT NULL,
  `status` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user_id`, `first_name`, `last_name`, `user_name`, `user_password`, `user_email`, `created_by`, `user_created_date`, `updated_by`, `updated_at`, `user_role`, `user_login_time`, `status`) VALUES
(1, 'Admin', 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', '', '', '', '', 'A', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_roles`
--

CREATE TABLE `assigned_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 3),
(6, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(30) NOT NULL,
  `p_id` int(20) NOT NULL,
  `userid` varchar(10) NOT NULL,
  `qnty` int(20) NOT NULL,
  `price` float(10,2) NOT NULL,
  `lat` text NOT NULL,
  `lng` text NOT NULL,
  `datetime` date NOT NULL,
  `stat` enum('y','n') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `p_id`, `userid`, `qnty`, `price`, `lat`, `lng`, `datetime`, `stat`) VALUES
(138, 4, '41', 4, 24.00, '8.5193482', '76.9599165', '2018-12-18', 'y'),
(35, 1, '22', 1, 50.00, '0.0', '0.0', '2018-09-29', 'y'),
(140, 1, '136', 3, 33.75, '8.5701077', '76.97192235', '2018-12-19', 'y'),
(27, 1, '63', 3, 148.00, '0.0', '0.0', '2018-09-21', 'y'),
(28, 2, '63', 3, 75.00, '0.0', '0.0', '2018-09-21', 'y'),
(143, 2, '158', 4, 48.00, '8.5193472', '76.9599166', '2018-12-19', 'y'),
(144, 5, '104', 10, 70.00, '25.791227', '55.9435541', '2018-12-20', 'y'),
(129, 1, '105', 1, 11.25, '25.7912047', '55.9435565', '2018-12-17', 'y'),
(243, 3, '148', 5, 110.00, '0.00', '0.00', '2019-01-16', 'y'),
(192, 3, '175', 11, 88.55, '25.7911317', '55.9436579', '2019-01-03', 'y'),
(163, 3, '160', 3, 34.50, '8.51941437', '76.95991082', '2018-12-26', 'y'),
(206, 1, '160', 5, 56.25, '8.4873232', '76.9424561', '2019-01-03', 'y'),
(191, 1, '175', 7, 72.75, '25.7912351', '55.943525', '2019-01-03', 'y'),
(189, 3, '146', 3, 24.15, '8.5193504', '76.959916', '2019-01-02', 'y'),
(224, 1, '165', 6, 60.75, '25.7782325', '55.969355', '2019-01-05', 'y'),
(188, 1, '146', 4, 35.00, '8.5193505', '76.9599158', '2019-01-02', 'y'),
(228, 3, '170', 8, 92.00, '25.7909877', '55.9416008', '2019-01-12', 'y'),
(285, 14, 'null', 4, 46.00, '0.00', '0.00', '2019-01-29', 'y'),
(225, 2, '165', 7, 84.00, '25.7782325', '55.969355', '2019-01-05', 'y'),
(283, 8, 'null', 10, 120.00, '0.00', '0.00', '2019-01-29', 'y'),
(318, 7, '213', 1, 11.25, '25.253982874897396', '55.29483040025981', '2019-03-08', 'y'),
(282, 5, 'null', 10, 70.00, '0.00', '0.00', '2019-01-29', 'y'),
(253, 4, '182', 4, 44.00, '8.5193666', '76.9599151', '2019-01-21', 'y'),
(284, 13, 'null', 6, 69.00, '0.00', '0.00', '2019-01-29', 'y'),
(288, 7, 'null', 4, 45.00, '0.00', '0.00', '2019-01-29', 'y'),
(319, 8, '213', 1, 12.00, '25.254160018747427', '55.294090660457734', '2019-03-08', 'y'),
(317, 5, '213', 3, 21.00, '25.2539179', '55.2943945', '2019-03-07', 'y'),
(320, 13, '213', 1, 11.50, '25.25428559702556', '55.29488865617471', '2019-03-08', 'y'),
(361, 8, '177', 6, 72.00, '0.00', '0.00', '2019-05-29', 'y'),
(360, 7, '177', 4, 45.00, '0.00', '0.00', '2019-05-29', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1.5 litter', '2017-02-18 18:31:33', NULL, NULL),
(2, '500ml', '2017-02-18 18:32:02', NULL, NULL),
(3, '330ml', '2017-02-18 18:32:02', NULL, NULL),
(4, '200ml', '2017-02-18 18:33:36', NULL, NULL),
(5, '200ml(glass)', '2017-02-18 18:33:36', NULL, NULL),
(6, 'invocie ', '2018-01-11 12:00:07', '2018-01-11 12:00:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `credit_payments`
--

CREATE TABLE `credit_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer` int(11) NOT NULL,
  `creditbook_no` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `amount_split_completed` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `credit_payments`
--

INSERT INTO `credit_payments` (`id`, `date`, `customer`, `creditbook_no`, `amount`, `amount_split_completed`, `remarks`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2017-11-28 00:00:00', 2, 'cr7', '120.00', 0, 'ajmal', '2017-11-28 13:54:22', '2017-11-28 13:54:22', NULL),
(2, '2017-11-28 00:00:00', 2, 'cr8', '140.00', 0, 'aj', '2017-11-28 13:57:20', '2017-11-28 13:57:20', NULL),
(3, '2017-12-08 00:00:00', 6, 'cr1', '110.00', 0, 'aj', '2017-12-08 08:46:53', '2017-12-08 08:46:53', NULL),
(4, '2017-12-08 00:00:00', 6, 'cr2', '40.00', 0, 'ss', '2017-12-08 08:47:53', '2017-12-08 08:47:53', NULL),
(5, '2017-12-13 00:00:00', 5, 'cr88', '100.00', 0, 'est', '2017-12-13 07:34:23', '2017-12-13 07:34:23', NULL),
(6, '2018-01-10 05:00:00', 6, '125', '200.00', 0, 'shinad', '2018-01-10 14:46:15', '2018-01-10 14:46:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `credit_payment_details`
--

CREATE TABLE `credit_payment_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `credit_payment_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `amount_split_completed` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `credit_payment_details`
--

INSERT INTO `credit_payment_details` (`id`, `credit_payment_id`, `sale_id`, `amount`, `amount_split_completed`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 5, '120.00', 0, '2017-11-28 13:54:22', '2017-11-28 13:54:22', NULL),
(2, 2, 5, '30.00', 0, '2017-11-28 13:57:20', '2017-11-28 13:57:20', NULL),
(3, 3, 11, '110.00', 0, '2017-12-08 08:46:53', '2017-12-08 08:46:53', NULL),
(4, 4, 11, '40.00', 0, '2017-12-08 08:47:53', '2017-12-08 08:47:53', NULL),
(5, 2, 12, '50.00', 0, '2017-12-13 17:41:19', '2017-12-13 17:41:19', NULL),
(6, 6, 11, '200.00', 0, '2018-01-10 14:46:15', '2018-01-10 14:46:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `address_one` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `address_two` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `name_of_firm` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `image_extension` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_type` int(10) UNSIGNED NOT NULL,
  `agent_id` int(11) NOT NULL,
  `agent_type` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `customer_category` int(11) NOT NULL,
  `customer_group` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address_one`, `address_two`, `place`, `zip_code`, `phone`, `email`, `name_of_firm`, `image_extension`, `customer_type`, `agent_id`, `agent_type`, `customer_category`, `customer_group`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'temp1', 'tmp', 'tmpp', 'tmpp', '46446464', '1111111111', '', 'tmp', '', 2, 0, '', 0, 0, '2017-09-06 17:06:55', '2017-11-25 15:33:07', '2017-11-25 15:33:07'),
(2, 'AL KHOR GROCERY', 'MARIS', 'MARIS', 'MARIS', '000', '0553612634', '', 'AL KHOR GROCERY', '', 1, 0, '', 0, 0, '2017-09-17 13:25:07', '2017-09-17 13:25:07', NULL),
(3, 'SEA SIDE CAFETERIA', 'JULAN', 'JULAN', 'JULAN', '0000', '0527951174', '', 'SEA SIDE CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 13:41:48', '2017-09-30 13:41:48', NULL),
(4, 'AL FAHEM GROCERY', 'JULAN', 'JULAN', 'JULAN', '0000', '0552904185', '', 'AL FAHEM GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:43:12', '2017-09-30 13:43:12', NULL),
(5, 'AL FENJAN GROCERY', 'JULAN', 'JULAN', 'JULAN', '0000', '072237396', '', 'AL FENJAN GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:44:17', '2017-09-30 13:44:17', NULL),
(6, 'AL ZAHRA CAFETERIA', 'JULAN', 'JULAN', 'JULAN', '0000', '0558450059', '', 'AL ZHRA CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 13:46:24', '2017-09-30 13:46:24', NULL),
(7, 'AL WASAIT CAFETERIA', 'SHAMAL', 'SHAMAL', 'SHAMAL', '0000', '07223321', '', 'AL WASAIT CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 13:46:39', '2017-09-30 13:46:39', NULL),
(8, 'AL NAWARAS SUPER MARKET', 'KHARAN', 'KHARAN', 'KHARAN', '0000', '0557180773', '', 'AL NAWARAS SUPER MARKET', '', 2, 0, '', 0, 0, '2017-09-30 13:48:00', '2017-09-30 13:48:00', NULL),
(9, 'AL MAMOON GROCERY', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '0000', '0505603043', '', 'AL MAMOON GROCERY', '', 1, 0, '', 0, 0, '2017-09-30 13:48:11', '2017-09-30 13:48:11', NULL),
(10, 'AL HIKMA GROCERY', 'RAK', 'RAK', 'RAK', '0000', '0554779516', '', 'AL HIKMA GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:49:20', '2017-09-30 13:49:20', NULL),
(11, 'AL MADHAQ SUPERMARKET', 'AL JEER', 'AL JEER', 'AL JEER', '000', '072681921', '', 'AL MADHAQ SUPERMARKET', '', 2, 0, '', 0, 0, '2017-09-30 13:49:35', '2017-09-30 13:49:35', NULL),
(12, 'AL SILA RESTAUARNT', 'KHARAN', 'KHARAN', 'KHARAN', '0000', '0501953950', '', 'AL SILA RESTAUARNT', '', 2, 0, '', 0, 0, '2017-09-30 13:50:17', '2017-09-30 13:50:17', NULL),
(13, 'AL TAWOON RESTUARENT', 'AL JEER', 'AL JEER', 'AL JEER', '0000', '072632858', '', 'AL TAWOON RESTUARENT', '', 2, 0, '', 0, 0, '2017-09-30 13:50:49', '2017-09-30 13:50:49', NULL),
(14, 'MUMTHAS SUPERMARKET', 'SHAM', 'SHAM', 'SHAM', '000', '072666953', '', 'MUMTHAS SUPERMARKET', '', 2, 0, '', 0, 0, '2017-09-30 13:52:21', '2017-09-30 13:52:21', NULL),
(15, 'AL RAMS SUPER MARKET', 'JULAN', 'JULAN', 'JULAN', '0000', '0504300766', '', 'AL RAMS SUPER MARKET', '', 2, 0, '', 0, 0, '2017-09-30 13:53:14', '2017-09-30 13:53:14', NULL),
(16, 'LAMB ROASTING', 'RAK', 'RAK', 'RAS AL KHAIMAH', '000', '072333356', '', 'LAMB ROASTING', '', 2, 0, '', 0, 0, '2017-09-30 13:53:14', '2017-09-30 13:53:14', NULL),
(17, 'RAHEENA GROREY', 'JULAN', 'JULAN', 'JULAN', '0000', '072238570', '', 'RAHEENA GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:54:04', '2017-09-30 13:54:04', NULL),
(18, 'DIBBAH GROCERY', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '0554445097', '', 'DIBBAH GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:54:43', '2017-09-30 13:54:43', NULL),
(19, 'HASSAN REFAIE GROCERY', 'JULAN', 'JULAN', 'JULAN', '0000', '072235852', '', 'HASSAN AL REFAIE GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:56:15', '2017-09-30 13:56:15', NULL),
(20, 'AL ASALA RESTAUARNT', 'JULAN', 'JULAN', 'JULAN', '0000', '0503712918', '', 'AL ASALA RESTAUARNT', '', 2, 0, '', 0, 0, '2017-09-30 13:57:10', '2017-09-30 13:57:10', NULL),
(21, 'AL QUSAIDATH GROCERY', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '072223242', '', 'AL QUSAIDATH GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 13:58:46', '2017-09-30 13:58:46', NULL),
(22, 'AL AWEER SUPERMARKET', 'RAK', 'RAK', 'RAK', '00', '999999', '', 'AL AWEER SUPERMARKET', '', 1, 0, '', 0, 0, '2017-09-30 14:00:08', '2017-09-30 14:00:08', NULL),
(23, 'COMODOR FILAFEL', 'RAK', 'RAK', 'RAK', '000', '072337929', '', 'COMODOR FILAFEL', '', 2, 0, '', 0, 0, '2017-09-30 14:01:30', '2017-09-30 14:01:30', NULL),
(24, 'AURAK COLLEGE', 'BURAIRAT', 'BURAIRAT', 'BURAIRAT', '0000', '0526017066', '', 'AURAK COLLEGE', '', 2, 0, '', 0, 0, '2017-09-30 14:04:01', '2017-09-30 14:04:01', NULL),
(25, 'FAWZI SUPERMARKET', 'RAK', 'RAK', 'RAS AL KHAIMAH', '000', '072333102', '', 'FAWZI SUPERMARKET', '', 2, 0, '', 0, 0, '2017-09-30 14:08:17', '2017-09-30 14:08:17', NULL),
(26, 'UMM MASHAHIR', 'SHAMAL', 'SHAMAL', 'SHAMAL', '0000', '0555102010', '', '', '', 1, 0, '', 0, 0, '2017-09-30 14:09:58', '2017-09-30 14:09:58', NULL),
(27, 'AL SHAMAL CAFETERIA', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '999999999', '', 'AL SHAMAL CAFETERIA', '', 1, 0, '', 0, 0, '2017-09-30 14:11:11', '2017-09-30 14:11:11', NULL),
(28, 'SHAHINA RESTUARENT', 'RAK', 'RAK', 'RAK', '000', '072331757', '', 'SHAHINA RESTUARENT', '', 2, 0, '', 0, 0, '2017-09-30 14:13:00', '2017-09-30 14:13:00', NULL),
(29, 'AL SAD GROCERY', 'RAK', 'RAK', 'RAK', '0000', '072336339', '', 'AL SAD GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 14:14:08', '2017-09-30 14:14:08', NULL),
(30, 'ASRAR COFFE SHOP', 'SHAM', 'SHAM', 'SHAM', '0000', '8989898', '', 'ASRAR COFFE SHOP', '', 2, 0, '', 0, 0, '2017-09-30 14:15:14', '2017-09-30 14:15:14', NULL),
(31, 'TAMMAM GROCERY', 'SHAM', 'SHAM', 'SHAM', '0000', '072681362', '', 'TAMMAM GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 14:16:02', '2017-09-30 14:16:02', NULL),
(32, 'NEW MOHAMED QAHTAN GROCERY', 'AL JEER', 'AL JEER', 'AL JEER', '0000', '072681354', '', 'NEW MOHAMED QAHTAN GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 14:17:12', '2017-09-30 14:17:12', NULL),
(33, 'AL ASHWAQ GROCERY', 'AL JEER', 'AL JEER', 'AL JEER', '000', '072681156', '', 'AL ASHWAQ GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 14:25:08', '2017-09-30 14:25:08', NULL),
(34, 'AL ABYAD SUPERMARKET', 'AL JEER', 'AL JEER', 'AL JEER', '0000', '072681735', '', 'AL ABYAD SUPERMARKET', '', 2, 0, '', 0, 0, '2017-09-30 14:26:56', '2017-09-30 14:26:56', NULL),
(35, 'KHOR KHUWAIR GROCERY', 'KHOR KHUWAIR', 'KHOR KHUWAIR', 'KHOR KHUWAIR', '000', '072668081', '', 'KHOR KHUWAIR GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 14:27:55', '2017-09-30 14:27:55', NULL),
(36, 'SHAM AL NASEEM SUPERMARKET', 'SHAMAL', 'SHAMAL', 'SHAMAL', '000', '77777777', '', 'SHAM AL NASEEM SUPERMARKET', '', 2, 0, '', 0, 0, '2017-09-30 14:28:45', '2017-09-30 14:28:45', NULL),
(37, 'KHALAF SUPERMARKET', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '0555474404', '', 'KHALAF SUPERMARKET', '', 2, 0, '', 0, 0, '2017-09-30 14:29:44', '2017-09-30 14:29:44', NULL),
(38, 'CALICUT RESTUARENT', 'RAK', 'RAK', 'RAK', '000', '072333697', '', 'CALICUT RESTUARENT', '', 2, 0, '', 0, 0, '2017-09-30 14:31:55', '2017-09-30 14:31:55', NULL),
(39, 'MANDARIN 1', 'RAK', 'BEACH', 'RAS AL KHAIMAH', '0000', '072333513', '', 'MANDARIN 1', '', 2, 0, '', 0, 0, '2017-09-30 14:33:44', '2017-09-30 14:33:44', NULL),
(40, 'KHUSOOSI CAFETERIA', 'DAHAN', 'RAK', 'RAS AL KHAIMAH', '000', '072335373', '', 'KHUSOOSI CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 14:34:35', '2017-09-30 14:34:35', NULL),
(41, 'AL GHADEER GROCERY', 'RAK', 'RAK', 'RAK', '00', '072334919', '', 'AL GHADEER GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 14:37:48', '2017-09-30 14:37:48', NULL),
(42, 'AL TAQAM GROCERY', 'RAMS', 'RAMS', 'RAMS', '000', '072663870', '', 'AL TAQAM GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:28:13', '2017-09-30 15:28:13', NULL),
(43, 'FILIPINO GROCERY', 'NAKEEL', 'NAKHEEL', 'NAKHEEL', '000', '0567498778', '', 'FILIPINO GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:30:34', '2017-09-30 15:30:34', NULL),
(44, 'AL SADAQA GROCERY', 'RAMS', 'RAMS', 'RAMS', '000', '2662355', '', 'AL SADAQA GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:31:30', '2017-09-30 15:31:30', NULL),
(45, 'ABU SUBAIA GROCERY', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '0558705216', '', 'ABU SUBAIA', '', 2, 0, '', 0, 0, '2017-09-30 15:33:25', '2017-09-30 15:33:25', NULL),
(46, 'AL NAJLA CAFETERIA', 'RAK', 'RAK', 'RAK', '000', '072334092', '', 'AL NAJLA CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 15:34:10', '2017-09-30 15:34:10', NULL),
(47, 'AL BARAT GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072237365', '', 'AL BARAT GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:34:59', '2017-09-30 15:34:59', NULL),
(48, 'BOMOATH GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072238624', '', 'BOMOATH GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:35:52', '2017-09-30 15:35:52', NULL),
(49, 'THAHANI FOOD STUFF GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '0553262100', '', 'THAHANI FOOD STUFF GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:44:33', '2017-09-30 15:44:33', NULL),
(50, 'AL NAWAA GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072238687', '', 'AL NAWAA GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:45:32', '2017-09-30 15:45:32', NULL),
(51, 'AL MAZAYA GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '0558504688', '', 'AL MAZAYA GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:46:16', '2017-09-30 15:46:16', NULL),
(52, 'AL WARSH GROCERY', 'RAK', 'RAK', 'RAK', '000', '072333963', '', 'AL WARSH GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:46:57', '2017-09-30 15:46:57', NULL),
(53, 'NARJIS AL AKBAR RESTAUARNT', 'KHARAN', 'KHARAN', 'KHARAN', '000', '072441676', '', 'NARJIS AL AKBAR RESTAUARNT', '', 2, 0, '', 0, 0, '2017-09-30 15:48:11', '2017-09-30 15:48:11', NULL),
(54, 'ISTANBUL CAFETERIA', 'JULAN', 'JULAN', 'JULAN', '000', '072288895', '', 'ISTANBUL CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 15:48:53', '2017-09-30 15:48:53', NULL),
(55, 'AL MATAF GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072234165', '', 'AL MATAF GROCERY', '', 1, 0, '', 0, 0, '2017-09-30 15:49:41', '2017-09-30 15:49:41', NULL),
(56, 'AL JANOOB GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072238586', '', 'AL JANOOB GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:50:20', '2017-09-30 15:50:20', NULL),
(57, 'AL MASHEREQ GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072237264', '', 'AL MASHEREQ GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:51:27', '2017-09-30 15:51:27', NULL),
(58, 'MARJAN CAFETERIA', 'JULAN', 'JULAN', 'JULAN', '000', '072234133', '', 'MARJAN CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 15:52:06', '2017-09-30 15:52:06', NULL),
(59, 'FATHUL  AL JADEEDA GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072237173', '', 'FATHUL AL JADEEDA GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:53:06', '2017-09-30 15:53:06', NULL),
(60, 'AL YAQOOT GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '0504467288', '', 'AL YAQOOT GROCERY', '', 2, 0, '', 0, 0, '2017-09-30 15:54:07', '2017-09-30 15:54:07', NULL),
(61, 'AL  BADAR CAFETERIA', 'RAK', 'RAK', 'RAK', '000', '072330945', '', 'AL BADAR CAFETERIA', '', 2, 0, '', 0, 0, '2017-09-30 15:55:43', '2017-09-30 15:55:43', NULL),
(62, 'AL ISLAH GROCERY', 'DHAID', 'DHAID', 'DHAID', '000', '072354857', '', 'AL ISLAH GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:26:21', '2017-10-05 14:26:21', NULL),
(63, 'JAZEERA AL HAMRA CAFETERIA', 'JAZEERA', 'JAZEERA', 'JAZEERA', '000', '072445212', '', 'JAZEERA AL HAMRA CAFETERIA', '', 1, 0, '', 0, 0, '2017-10-05 14:27:43', '2017-10-05 14:27:43', NULL),
(64, 'TITANIC RESTUARENT', 'JAZEERA', 'JAZEERA', 'JAZEERA', '000', '0552687816', '', 'TITANIC RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-05 14:28:37', '2017-10-05 14:28:37', NULL),
(65, 'ABDALLA ABDAL KAREEM STORE', 'JAZEERA', 'JAZEERA', 'JAZEERA', '000', '0552871778', '', 'ABDALLA ABDAL KAREEM STORE', '', 2, 0, '', 0, 0, '2017-10-05 14:30:17', '2017-10-05 14:30:17', NULL),
(66, 'BURJ CAFETERIA', 'JAZEERA', 'JAZEERA', 'JAZEERA', '000', '0507870143', '', 'BURJ CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-05 14:31:01', '2017-10-05 14:31:01', NULL),
(67, 'OKAZ CAFETERIA', 'JAZEERA', 'JAZEERA', 'JAZEERA', '000', '072444158', '', 'OKAZ CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-05 14:32:36', '2017-10-05 14:32:36', NULL),
(68, 'AL SHUHAIB GROCERY', 'DHAID', 'DHAID', 'DHAID', '000', '072352012', '', 'AL SHUHAIB GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:33:54', '2017-10-05 14:33:54', NULL),
(69, 'AL SHUHAIB GROCERY', 'DHAID', 'DHAID', 'DHAID', '000', '0723456658', '', 'AL SHUHAIB GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:34:54', '2017-10-05 14:35:14', NULL),
(70, 'NEW PLAZA GROCERY', 'JULAN', 'JULAN', 'JULAN', '0000', '0508582765', '', 'NEW PLAZA GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:38:48', '2017-10-05 14:38:48', NULL),
(71, 'AL BISRA GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072236415', '', 'AL BISRA GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:43:23', '2017-10-05 14:43:23', NULL),
(72, 'AL RABOA GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072237164', '', 'AL RABOA GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:44:52', '2017-10-05 14:44:52', NULL),
(73, 'TELMEEZ GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '0506973546', '', 'TELMEEZ GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:47:49', '2017-10-05 14:47:49', NULL),
(74, 'SALEH RAMDAN GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '0526727635', '', 'SALEH RAMDAN GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:49:29', '2017-10-05 14:49:29', NULL),
(75, 'AL HAMAR GROCERY', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '0567216732', '', 'AL HAMAR GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:52:08', '2017-10-05 14:52:08', NULL),
(76, 'AL DHABOON GROCERY', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '072441827', '', 'AL DHABOON GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:56:15', '2017-10-05 14:56:15', NULL),
(77, 'AL SAFNA GROCERY', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '0000', '072443146', '', 'AL SAFNA GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 14:57:16', '2017-10-05 14:57:16', NULL),
(78, 'YOURS RESTUARENT', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '072461956', '', 'YOURS RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-05 14:58:37', '2017-10-05 14:58:37', NULL),
(79, 'PESHAWAR RESTUARENT', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '0527464277', '', 'PESHAWAR RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-05 15:01:05', '2017-10-05 15:01:05', NULL),
(80, 'AL MODAFIQ GROCERY', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '0000', '0558753663', '', 'AL MODAFIQ GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 15:02:59', '2017-10-05 15:02:59', NULL),
(81, 'FANOOS GROCERY', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '0507793851', '', 'FANOOS GROCERY', '', 2, 0, '', 0, 0, '2017-10-05 15:05:30', '2017-10-05 15:05:30', NULL),
(82, 'AL SHAGHI CAFETERIA', 'RAK', 'RAK', 'RAK', '000', '012333837', '', 'AL SHAGHI CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-05 15:07:21', '2017-10-05 15:07:21', NULL),
(83, 'AL HOAR GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '0508592598', '', 'AL HOAR GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 13:50:04', '2017-10-11 13:50:04', NULL),
(84, 'AL HILAL GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072239612', '', 'AL HILAL GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 13:51:24', '2017-10-11 13:51:24', NULL),
(85, 'AL TASMOH GROCERY', 'JULAN', 'JULAN', 'JULAN', '000', '072237195', '', 'AL TASMOH GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 13:53:22', '2017-10-11 13:53:22', NULL),
(86, 'AL SHATI AL FEEDI RESTUARENT', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '0505023918', '', 'AL SHATI AL FEEDI RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 13:57:04', '2017-10-11 13:57:04', NULL),
(87, 'RASHID ALI', 'SHAM', 'SHAM', 'SHAM', '000', '0503031050', '', '', '', 1, 0, '', 0, 0, '2017-10-11 13:59:40', '2017-10-11 13:59:40', NULL),
(88, 'AL KHOBARA SUPERMARKET', 'RAMS', 'RAMS', 'RAMS', '000', '0501717003', '', 'AL KHOBARA SUPERMARKET', '', 2, 0, '', 0, 0, '2017-10-11 14:05:30', '2017-10-11 14:52:41', NULL),
(89, 'RAGAD GROCERY', 'RAK', 'RAK', 'RAK', '000', '0551998041', '', 'RAGAD GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:06:55', '2017-10-11 14:06:55', NULL),
(90, 'MAGI RESTUARENT', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '072235833', '', 'MAGI RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 14:11:49', '2017-10-11 14:11:49', NULL),
(91, 'AL AALIA GROCERY', 'AL GAIL', 'AL GAIL', 'AL GAIL', '000', '072585879', '', 'AL AALIA GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:13:07', '2017-10-11 14:13:07', NULL),
(92, 'PATIALA PUNJABI RESTUARENT', 'AL GAIL', 'AL GAIL', 'AL GAIL', '0000', '0506333471', '', 'PATIALA PUNJABI RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 14:16:01', '2017-10-11 14:16:01', NULL),
(93, 'ALI KUTTI STORE GROCERY', 'RAK', 'RAK', 'RAK', '000', '072331334', '', 'ALI KUTTI STORE GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:17:50', '2017-10-11 14:17:50', NULL),
(94, 'CUSTOMER', 'RAMS', 'RAMS', 'RAMS', '000', '0509506656', '', '', '', 1, 0, '', 0, 0, '2017-10-11 14:19:12', '2017-10-11 14:19:12', NULL),
(95, 'GHANA CAFETERIA', 'SHAMAL', 'SHAMAL', 'SHAMAL', '0000', '0509949570', '', 'GHANA CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-11 14:20:08', '2017-10-11 14:20:08', NULL),
(96, 'AL BAYAN SUPERMARKET', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '072227477', '', 'AL BAYAN SUPERMARKET', '', 2, 0, '', 0, 0, '2017-10-11 14:21:38', '2017-10-11 14:21:38', NULL),
(97, 'ASIMA RESTUARENT', 'RAK', 'RAK', 'RAK', '000', '072337848', '', 'ASIMA RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 14:23:03', '2017-10-11 14:23:03', NULL),
(98, 'EMIRATES SUPERMARKET', 'JULAN', 'JULAN', 'JULAN', '000', '0506800385', '', 'EMIRATES SUPERMARKET', '', 2, 0, '', 0, 0, '2017-10-11 14:27:48', '2017-10-11 14:27:48', NULL),
(99, 'MALIK AL QAWA CAFETERIA', 'JULAN', 'JULAN', 'JULAN', '0000', '072234332', '', 'MALIK AL QAWA CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-11 14:29:04', '2017-10-11 14:29:04', NULL),
(100, 'GULF PEARL GROCERY', 'MARIS', 'MARIS', 'MARIS', '000', '072237517', '', 'GULF PEARL GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:30:12', '2017-10-11 14:30:12', NULL),
(101, 'HABHAB SUPERMARKET', 'KHATT', 'KHATT', 'KHATT', '0000', '072580023', '', 'HABHAB SUPERMARKET', '', 2, 0, '', 0, 0, '2017-10-11 14:31:22', '2017-10-11 14:31:22', NULL),
(102, 'AL LATHETHE CAFETERIA', 'KHARAN', 'KHARAN', 'KHARAN', '000', '0558345759', '', 'AL LATHETHE CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-11 14:35:17', '2017-10-11 14:35:17', NULL),
(103, 'SALWA GROCERY', 'RAK', 'RAK', 'RAK', '000', '072330263', '', 'SALWA GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:42:28', '2017-10-11 14:42:28', NULL),
(104, 'NAJLA CAFETERIA', 'RAK', 'RAK', 'RAK', '000', '66666666', '', 'NAJLA CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-11 14:43:37', '2017-10-11 14:50:45', '2017-10-11 14:50:45'),
(105, 'BADAR CAFETERIA', 'RAK', 'RAK', 'RAK', '000', '9897897655667', '', 'BADAR CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-11 14:44:09', '2017-10-11 14:44:09', NULL),
(106, 'AHLAN WA SAHLAN GROCERY', 'RAK', 'RAK', 'RAK', '000', '7656565', '', 'AHLAN WA SAHLAN GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:45:01', '2017-10-11 14:45:01', NULL),
(107, 'AL TABUEI JUICES', 'MARIS', 'MARIS', 'MARIS', '0000', '0553776023', '', 'AL TABUEI JUICES', '', 2, 0, '', 0, 0, '2017-10-11 14:46:13', '2017-10-11 14:46:13', NULL),
(108, 'METRO MANILA SUPERMARKET', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '878654', '', 'METRO MANILA SUPERMARKET', '', 2, 0, '', 0, 0, '2017-10-11 14:47:09', '2017-10-11 14:47:09', NULL),
(109, 'AL QATARI GROCERY', 'RAMS', 'RAMS', 'RAMS', '000', '072662681', '', 'AL QATARI GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:48:48', '2017-10-11 14:48:48', NULL),
(110, 'AL CORNICHE RESTUARENT', 'RAMS', 'RAMS', 'RAMS', '000', '5446466', '', 'AL CORNICHE RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 14:50:04', '2017-10-11 14:50:04', NULL),
(111, 'AL NAJAH GROCRY', 'RAMS', 'RAMS', 'RAMS', '000', '0507500685', '', 'AL NAJAH GROCRY', '', 2, 0, '', 0, 0, '2017-10-11 14:52:04', '2017-10-11 14:52:04', NULL),
(112, 'AL SYOOTHI RESTUARENT', 'MARIS', 'MARIS', 'MARIS', '000', '786876', '', 'AL SYOOTHI RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 14:53:24', '2017-10-11 14:53:24', NULL),
(113, 'AL ASWAQ GROCERY ', 'KHARAN', 'KHARAN', 'KHARAN', '000', '072443254', '', 'AL ASWAQ GROCERY ', '', 2, 0, '', 0, 0, '2017-10-11 14:55:05', '2017-10-11 14:55:05', NULL),
(114, 'AL SHIFA RESTUARENT', 'DIGDAGA', 'DIGDAGA', 'DIGDAGA', '000', '072443780', '', 'AL SHIFA RESTUARENT', '', 2, 0, '', 0, 0, '2017-10-11 14:56:03', '2017-10-11 14:56:03', NULL),
(115, 'AL GHELANI CAFETERIA', 'KHARAN', 'KHARAN', 'KHARAN', '0000', '0502957940', '', 'AL GHELANI CAFETERIA', '', 2, 0, '', 0, 0, '2017-10-11 14:57:13', '2017-10-11 14:57:13', NULL),
(116, 'DALMA GROCERY', 'DHAID', 'DHAID', 'DHAID', '0000', '072354792', '', 'DALMA GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 14:59:21', '2017-10-11 14:59:21', NULL),
(117, 'AL AQEED GROCERY', 'BURAIRAT', 'BURAIRAT', 'BURAIRAT', '0000', '072262138', '', 'AL AQEED GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 15:00:52', '2017-10-11 15:00:52', NULL),
(118, 'AL MISFA GROCERY', 'BURAIRAT', 'BURAIRAT', 'BURAIRAT', '0000', '0509705379', '', 'AL MISFA GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 15:02:14', '2017-10-11 15:02:14', NULL),
(119, 'AL ASAFEER GROCERY', 'DHAID', 'DHAID', 'DHAID', '000', '0503063212', '', 'AL ASAFEER GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 15:03:18', '2017-10-11 15:03:18', NULL),
(120, 'AL TOOT GROCERY', 'DHAID', 'DHAID', 'DHAID', '000', '0525708720', '', 'AL TOOT GROCERY', '', 2, 0, '', 0, 0, '2017-10-11 15:05:25', '2017-10-11 15:05:25', NULL),
(121, 'Rasha mohsen Govt Khuzam', 'Khuzam', 'Khuzam', 'RAK', '000', '0544522422', '', '', '', 1, 0, '', 0, 0, '2017-11-25 11:06:13', '2017-11-25 11:06:13', NULL),
(122, 'FIRAS MARKAZ ZAYED GOVT', 'MARKAZ ZAYED', 'MARKAZ ZAYED', 'MARKAZ ZAYED', '000', '0503382918', '', '', '', 1, 0, '', 0, 0, '2017-11-25 11:11:30', '2017-11-25 11:11:30', NULL),
(123, 'MAYS JULAN GOVT', 'Julan', 'Julan', 'Julan', '0000', '0509085850', '', '', '', 1, 0, '', 0, 0, '2017-11-25 11:15:01', '2017-11-25 11:15:01', NULL),
(124, 'CUSTOMER', 'DHAID', 'DHAID', 'DHAID', '000', '0505167775', '', '', '', 1, 0, '', 0, 0, '2017-11-25 15:31:51', '2017-11-25 15:31:51', NULL),
(125, 'ABU ABOOD', 'NAKHEEL', 'NAKHEEL', 'NAKHEEL', '000', '0504866399', '', '', '', 1, 0, '', 0, 0, '2017-11-25 15:32:46', '2017-11-25 15:32:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_categories`
--

CREATE TABLE `customer_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_groups`
--

CREATE TABLE `customer_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `remarks` varchar(160) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customer_phones`
--

CREATE TABLE `customer_phones` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_phones`
--

INSERT INTO `customer_phones` (`id`, `customer_id`, `phone`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, '0558519090', '2017-09-30 13:46:39', '2017-09-30 13:46:39', NULL),
(2, 4, '0555401027', '2017-09-30 13:52:13', '2017-09-30 13:52:13', NULL),
(3, 21, '0503722167', '2017-09-30 13:58:46', '2017-09-30 13:58:46', NULL),
(4, 23, '0501959899', '2017-09-30 14:01:30', '2017-09-30 14:01:30', NULL),
(5, 29, '0507103640', '2017-09-30 14:14:08', '2017-09-30 14:14:08', NULL),
(6, 33, '0501966799', '2017-09-30 14:25:08', '2017-09-30 14:25:08', NULL),
(7, 2, '072271261', '2017-09-30 14:30:34', '2017-09-30 14:30:34', NULL),
(8, 75, '072441997', '2017-10-05 14:52:49', '2017-10-05 14:52:49', NULL),
(9, 123, '0562001071', '2017-11-25 11:15:01', '2017-11-25 11:15:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deli_attendance`
--

CREATE TABLE `deli_attendance` (
  `id` int(22) NOT NULL,
  `staff_id` int(22) NOT NULL,
  `status` varchar(25) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `created_time` varchar(25) NOT NULL,
  `updated_time` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deli_attendance`
--

INSERT INTO `deli_attendance` (`id`, `staff_id`, `status`, `created_at`, `updated_at`, `created_time`, `updated_time`) VALUES
(7, 1, 'off', '2019-06-18 15:33:34', '2019-06-18 15:33:46', '', ''),
(8, 1, 'off', '2019-06-19 15:33:34', '2019-06-19 15:33:46', '', ''),
(9, 330, 'on', '2019-06-25 16:02:47', '2019-07-01 11:18:31', '', ''),
(13, 330, 'on', '01-July-2019', '01-July-2019', '11:56:04', '11:56:06');

-- --------------------------------------------------------

--
-- Table structure for table `deli_feedback`
--

CREATE TABLE `deli_feedback` (
  `id` int(11) NOT NULL,
  `staff_id` int(22) NOT NULL,
  `user_id` int(22) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deli_feedback`
--

INSERT INTO `deli_feedback` (`id`, `staff_id`, `user_id`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 22, 'hiiiiiiiiiiiiiiiiiiiiiiiiiiiiii', 1, '2019-06-18 06:55:18', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `deli_login`
--

CREATE TABLE `deli_login` (
  `id` int(11) NOT NULL,
  `uid` bigint(22) NOT NULL,
  `pwd` varchar(233) NOT NULL,
  `name` varchar(222) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deli_login`
--

INSERT INTO `deli_login` (`id`, `uid`, `pwd`, `name`, `created`, `modified`) VALUES
(1, 8745894567, 'ddd', 'test', '0000-00-00 00:00:00', '2019-06-06 09:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `deli_product_order`
--

CREATE TABLE `deli_product_order` (
  `id` int(15) NOT NULL,
  `order_id` varchar(150) NOT NULL,
  `user_id` int(15) NOT NULL,
  `preorder_date` varchar(150) NOT NULL,
  `preorder_time` varchar(150) NOT NULL,
  `total_price` varchar(250) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `location_lat` float NOT NULL,
  `location_long` float NOT NULL,
  `delivered_status` varchar(150) NOT NULL,
  `order_status` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deli_staff_id` int(11) NOT NULL,
  `amount_paid` bigint(200) NOT NULL,
  `pending_amount` bigint(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deli_product_order`
--

INSERT INTO `deli_product_order` (`id`, `order_id`, `user_id`, `preorder_date`, `preorder_time`, `total_price`, `address`, `location_lat`, `location_long`, `delivered_status`, `order_status`, `created_at`, `updated_at`, `deli_staff_id`, `amount_paid`, `pending_amount`) VALUES
(85, '100085', 22, '', '', '100', '', 0, 0, 'completed', 'Order', '2018-08-14 13:11:33', '2019-06-19 06:33:17', 0, 20, 80),
(86, '100086', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Preorder', '2018-08-14 13:13:40', '2019-06-18 05:50:05', 0, 0, 0),
(87, '100087', 53, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:26:21', '2019-06-18 05:50:05', 0, 0, 0),
(88, '100088', 40, '', '', '250', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:43:17', '2019-06-18 05:50:05', 0, 0, 0),
(89, '100089', 53, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:46:49', '2019-06-18 05:50:05', 0, 0, 0),
(90, '100090', 53, '', '', '250', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:48:43', '2019-06-18 05:50:05', 0, 0, 0),
(91, '100091', 53, '', '', '90', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:49:07', '2019-06-18 05:50:05', 0, 0, 0),
(92, '100092', 22, '', '', '5', '', 0, 0, 'Ordered', 'Order', '2018-08-14 14:11:12', '2019-06-18 05:50:05', 0, 0, 0),
(93, '100093', 22, '', '', '15', '', 0, 0, 'Ordered', 'Order', '2018-08-14 14:21:41', '2019-06-18 05:50:05', 0, 0, 0),
(94, '100094', 41, '', '', '120', '', 0, 0, 'Ordered', 'Order', '2018-08-14 15:00:03', '2019-06-18 05:50:05', 0, 0, 0),
(95, '100095', 53, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-08-21 15:22:08', '2019-06-18 05:50:05', 0, 0, 0),
(102, '100096', 41, 'Select Date', 'Select Time', '50', '', 0, 0, 'Ordered', 'Preorder', '2018-08-23 15:06:29', '2019-06-18 05:50:05', 0, 0, 0),
(104, '100097', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-08-23 15:06:51', '2019-06-18 05:50:05', 0, 0, 0),
(106, '100098', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-08-23 15:31:40', '2019-06-18 05:50:05', 0, 0, 0),
(107, '100099', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-08-23 15:34:58', '2019-06-18 05:50:05', 0, 0, 0),
(108, '100100', 41, '23-8-2018', '8:35 PM', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-08-23 15:35:20', '2019-06-18 05:50:05', 0, 0, 0),
(109, '100101', 41, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-27 13:56:34', '2019-06-18 05:50:05', 0, 0, 0),
(113, '100102', 41, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-28 14:35:12', '2019-06-18 05:50:05', 0, 0, 0),
(114, '100103', 41, '', '', '75', '', 0, 0, 'Ordered', 'Order', '2018-08-28 14:40:18', '2019-06-18 05:50:05', 0, 0, 0),
(115, '100104', 41, 'Select Date', 'Select Time', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-08-28 14:51:28', '2019-06-18 05:50:05', 0, 0, 0),
(116, '100105', 41, '', '', '150', '', 0, 0, 'Ordered', 'Order', '2018-08-28 16:07:47', '2019-06-18 05:50:05', 0, 0, 0),
(117, '100106', 41, '8-9-2018', '12:02 PM', '50', '', 0, 0, 'Ordered', 'Preorder', '2018-09-01 12:02:22', '2019-06-18 05:50:05', 0, 0, 0),
(118, '100107', 26, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-09-01 12:06:55', '2019-06-18 05:50:05', 0, 0, 0),
(119, '100108', 26, '', '', '1050', '', 0, 0, 'Ordered', 'Order', '2018-09-01 12:09:40', '2019-06-18 05:50:05', 0, 0, 0),
(120, '100109', 41, '', '', '300', '', 0, 0, 'Ordered', 'Order', '2018-09-03 11:54:39', '2019-06-18 05:50:05', 0, 0, 0),
(121, '100110', 62, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-09-03 12:04:20', '2019-06-18 05:50:05', 0, 0, 0),
(122, '100111', 62, '3-9-2018', '4:05 PM', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-09-03 12:05:21', '2019-06-18 05:50:05', 0, 0, 0),
(123, '100112', 41, '', '', '120', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:31:40', '2019-06-18 05:50:05', 0, 0, 0),
(124, '100113', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:31:56', '2019-06-18 05:50:05', 0, 0, 0),
(125, '100114', 41, '', '', '5', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:32:02', '2019-06-18 05:50:05', 0, 0, 0),
(126, '100115', 41, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:32:07', '2019-06-18 05:50:05', 0, 0, 0),
(127, '100116', 41, 'Select Date', 'Select Time', '50', '', 0, 0, 'Ordered', 'Preorder', '2018-09-03 14:32:25', '2019-06-18 05:50:05', 0, 0, 0),
(128, '100117', 41, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:32:49', '2019-06-18 05:50:05', 0, 0, 0),
(129, '100118', 41, '7-9-2018', '7:13 PM', '30', '', 0, 0, 'Ordered', 'Preorder', '2018-09-05 17:14:02', '2019-06-18 05:50:05', 0, 0, 0),
(130, '100119', 41, '7-9-2018', '6:47 PM', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-09-05 17:47:05', '2019-06-18 05:50:05', 0, 0, 0),
(131, '100120', 22, '', '', '100', '', 1.23545, 1.23545, 'completed', 'Order', '2018-09-06 11:11:46', '2019-06-19 09:22:48', 1, 20, 0),
(132, '100121', 22, '22/07/2018', '10:00 AM', '260', '', 1.2563, 2.2354, 'completed', 'Preorder', '2018-09-06 11:14:31', '2019-06-19 07:50:34', 1, 0, 0),
(133, '100122', 41, '', '', '30', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-06 11:37:10', '2019-06-18 05:50:05', 1, 0, 0),
(134, '100123', 41, '', '', '75', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-07 10:47:44', '2019-06-18 05:50:05', 1, 0, 0),
(135, '100124', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:45:45', '2019-06-18 05:50:05', 1, 0, 0),
(136, '100125', 41, '', '', '25', '', 8.51935, 76.96, 'cancelled', 'Order', '2018-09-11 11:45:55', '2019-06-20 10:23:38', 1, 0, 0),
(137, '100126', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:50:19', '2019-06-18 05:50:05', 7, 0, 0),
(138, '100127', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:51:59', '2019-06-18 05:50:05', 0, 0, 0),
(139, '100128', 41, '', '', '120', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-11 12:16:08', '2019-06-18 05:50:05', 1, 0, 0),
(140, '100129', 41, '11-9-2018', '12:16 PM', '25', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-11 12:16:18', '2019-06-18 05:50:05', 1, 0, 0),
(141, '100130', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 16:26:28', '2019-06-18 05:50:05', 0, 0, 0),
(142, '100131', 26, 'Ø§Ø®ØªØ± Ø§Ù„ØªØ§Ø±ÙŠØ®', 'Ø§Ø®ØªØ± Ø§Ù„ÙˆÙ‚Øª', '25', '', 8.57005, 76.9719, 'Ordered', 'Preorder', '2018-09-11 23:52:50', '2019-06-18 05:50:05', 0, 0, 0),
(143, '100132', 26, 'Ø§Ø®ØªØ± Ø§Ù„ØªØ§Ø±ÙŠØ®', 'Ø§Ø®ØªØ± Ø§Ù„ÙˆÙ‚Øª', '30', '', 8.57005, 76.9719, 'Ordered', 'Preorder', '2018-09-11 23:53:00', '2019-06-18 05:50:05', 0, 0, 0),
(144, '100133', 26, '', '', '150', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-11 23:53:07', '2019-06-18 05:50:05', 1, 0, 0),
(145, '100134', 26, '', '', '75', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-11 23:53:14', '2019-06-18 05:50:05', 0, 0, 0),
(146, '100135', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-12 10:30:48', '2019-06-18 05:50:05', 0, 0, 0),
(147, '100136', 41, '12-9-2018', '10:30 AM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-12 10:30:59', '2019-06-18 05:50:05', 0, 0, 0),
(148, '100137', 41, '12-9-2018', '10:31 AM', '25', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-12 10:31:12', '2019-06-18 05:50:05', 0, 0, 0),
(149, '100138', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:16:20', '2019-06-18 05:50:05', 0, 0, 0),
(150, '100139', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:16:24', '2019-06-18 05:50:05', 0, 0, 0),
(151, '100140', 41, '', '', '75', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:16:32', '2019-06-18 05:50:05', 0, 0, 0),
(152, '100141', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:17:43', '2019-06-18 05:50:05', 0, 0, 0),
(153, '100142', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:40:12', '2019-06-18 05:50:05', 0, 0, 0),
(154, '100143', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:43:13', '2019-06-18 05:50:05', 0, 0, 0),
(155, '100144', 41, '', '', '300', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 12:03:29', '2019-06-18 05:50:05', 0, 0, 0),
(156, '100145', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-17 13:26:03', '2019-06-18 05:50:05', 0, 0, 0),
(157, '100146', 41, '', '', '15', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-17 13:26:03', '2019-06-18 05:50:05', 0, 0, 0),
(158, '100147', 26, '', '', '200', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 10:06:35', '2019-06-18 05:50:05', 0, 0, 0),
(159, '100148', 26, '', '', '210', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 10:06:35', '2019-06-18 05:50:05', 0, 0, 0),
(160, '100149', 41, '', '', '100', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:14:58', '2019-06-18 05:50:05', 0, 0, 0),
(161, '100150', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:14:58', '2019-06-18 05:50:05', 0, 0, 0),
(162, '100151', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:14:58', '2019-06-18 05:50:05', 0, 0, 0),
(163, '100152', 41, '', '', '75', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:19:24', '2019-06-18 05:50:05', 0, 0, 0),
(164, '100153', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-18 10:57:49', '2019-06-18 05:50:05', 0, 0, 0),
(165, '100154', 41, '', '', '50', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-18 10:57:49', '2019-06-18 05:50:05', 0, 0, 0),
(166, '100155', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:58:35', '2019-06-18 05:50:05', 0, 0, 0),
(167, '100156', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:58:35', '2019-06-18 05:50:05', 0, 0, 0),
(168, '100157', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:58:35', '2019-06-18 05:50:05', 0, 0, 0),
(169, '100158', 41, '', '', '150', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:59:27', '2019-06-18 05:50:05', 0, 0, 0),
(170, '100159', 41, '', '', '60', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:59:27', '2019-06-18 05:50:05', 0, 0, 0),
(171, '100160', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:01:24', '2019-06-18 05:50:05', 0, 0, 0),
(172, '100161', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:01:24', '2019-06-18 05:50:05', 0, 0, 0),
(173, '100162', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:02:13', '2019-06-18 05:50:05', 0, 0, 0),
(174, '100163', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:02:13', '2019-06-18 05:50:05', 0, 0, 0),
(175, '100164', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:02:13', '2019-06-18 05:50:05', 0, 0, 0),
(176, '100165', 26, '', '', '200', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 12:39:11', '2019-06-18 05:50:05', 0, 0, 0),
(177, '100166', 26, '', '', '210', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 12:39:11', '2019-06-18 05:50:05', 0, 0, 0),
(178, '100167', 26, '', '', '250', '', 8.49568, 76.9316, 'Ordered', 'Order', '2018-09-18 12:40:51', '2019-06-18 05:50:05', 0, 0, 0),
(179, '100168', 26, '', '', '200', '', 8.49524, 76.9324, 'Ordered', 'Order', '2018-09-18 12:40:51', '2019-06-18 05:50:05', 0, 0, 0),
(180, '100169', 27, '', '', '80', '', 2.21122, 2.36555, 'Ordered', 'Order', '2018-09-18 14:06:55', '2019-06-18 05:50:05', 0, 0, 0),
(181, '100170', 41, '', '', '2060', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 14:22:09', '2019-06-18 05:50:05', 0, 0, 0),
(182, '100171', 22, '', '', '205', '', 0, 0, 'Ordered', 'Order', '2018-09-18 17:11:11', '2019-06-18 05:50:05', 0, 0, 0),
(183, '100172', 41, '', '', '100', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 17:31:07', '2019-06-18 05:50:05', 0, 0, 0),
(184, '100173', 41, '', '', '105', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 17:35:15', '2019-06-18 05:50:05', 0, 0, 0),
(185, '100174', 26, '', '', '300', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 17:45:46', '2019-06-18 05:50:05', 0, 0, 0),
(186, '100175', 41, '', '', '470', '', 8.51935, 76.9599, 'Ordered', 'Order', '2018-09-19 13:57:51', '2019-06-18 05:50:05', 0, 0, 0),
(187, '', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 14:21:13', '2019-06-18 05:50:05', 0, 0, 0),
(188, '', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 14:21:13', '2019-06-18 05:50:05', 0, 0, 0),
(189, '', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 14:22:36', '2019-06-18 05:50:05', 0, 0, 0),
(190, '', 41, '', '', '105', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-19 14:23:07', '2019-06-18 05:50:05', 0, 0, 0),
(191, '', 41, '', '', '105', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-19 14:23:25', '2019-06-18 05:50:05', 0, 0, 0),
(192, '100176', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 15:31:36', '2019-06-18 05:50:05', 0, 0, 0),
(193, '100177', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-19 15:32:31', '2019-06-18 05:50:05', 0, 0, 0),
(194, '100178', 41, '', '', '470', '', 8.51935, 76.9599, 'Ordered', 'Order', '2018-09-19 15:32:51', '2019-06-18 05:50:05', 0, 0, 0),
(195, '100179', 63, '', '', '130', '', 25.7785, 55.9697, 'Ordered', 'Order', '2018-09-20 10:50:15', '2019-06-18 05:50:05', 0, 0, 0),
(196, '100180', 41, '19-9-2018', '11:28 AM', '50', '', 8.51935, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:28:37', '2019-06-18 05:50:05', 0, 0, 0),
(197, '100181', 41, '21-9-2018', '11:30 AM', '25', '', 8.51935, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:30:13', '2019-06-18 05:50:05', 0, 0, 0),
(198, '100182', 41, '27-9-2018', '11:33 AM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:33:12', '2019-06-18 05:50:05', 0, 0, 0),
(199, '100183', 41, '27-9-2018', '11:34 AM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:34:58', '2019-06-18 05:50:05', 0, 0, 0),
(200, '100184', 41, '26-9-2018', '4:35 PM', '25', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:35:52', '2019-06-18 05:50:05', 0, 0, 0),
(201, '100185', 41, '21-9-2018', '5:36 PM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:36:22', '2019-06-18 05:50:05', 0, 0, 0),
(202, '100186', 41, '', '', '275', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-21 11:37:46', '2019-06-18 05:50:05', 0, 0, 0),
(203, '100187', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-09-21 11:56:36', '2019-06-18 05:50:05', 0, 0, 0),
(204, '100188', 26, '', '', '350', '', 8.55243, 76.877, 'Ordered', 'Order', '2018-09-24 12:52:39', '2019-06-18 05:50:05', 0, 0, 0),
(205, '100189', 26, '26-9-2018', '5:52 AM', '50', '', 8.55243, 76.877, 'Ordered', 'Preorder', '2018-09-24 12:52:53', '2019-06-18 05:50:05', 0, 0, 0),
(206, '100190', 41, '', '', '12175', '', 8.51934, 76.96, 'Ordered', 'Order', '2018-09-28 16:14:52', '2019-06-18 05:50:05', 0, 0, 0),
(207, '100191', 41, '', '', '25', '', 8.51934, 76.96, 'Ordered', 'Order', '2018-09-28 16:20:37', '2019-06-18 05:50:05', 0, 0, 0),
(208, '100192', 26, '', '', '420', '', 8.52465, 76.9423, 'Ordered', 'Order', '2018-09-30 17:31:14', '2019-06-18 05:50:05', 0, 0, 0),
(209, '100193', 26, '', '', '110', '', 8.56051, 76.8802, 'Ordered', 'Order', '2018-10-04 10:44:17', '2019-06-18 05:50:05', 0, 0, 0),
(210, '100194', 26, '', '', '200', '', 8.5605, 76.8803, 'Ordered', 'Order', '2018-10-12 14:05:52', '2019-06-18 05:50:05', 0, 0, 0),
(211, '100195', 26, '', '', '200', '', 8.56052, 76.8803, 'Ordered', 'Order', '2018-10-23 10:01:24', '2019-06-18 05:50:05', 0, 0, 0),
(212, '100196', 26, '', '', '130', '', 8.56356, 76.8589, 'Ordered', 'Order', '2018-10-26 17:38:42', '2019-06-18 05:50:05', 0, 0, 0),
(213, '100197', 26, '', '', '100', '', 8.56356, 76.8589, 'Ordered', 'Order', '2018-10-26 17:38:59', '2019-06-18 05:50:05', 0, 0, 0),
(214, '100198', 26, '', '', '300', '', 8.53943, 76.9457, 'Ordered', 'Order', '2018-11-06 20:53:05', '2019-06-18 05:50:05', 0, 0, 0),
(215, '100199', 41, '14-11-2018', '6:06 PM', '25', '', 8.51934, 76.9599, 'Ordered', 'Preorder', '2018-11-14 14:06:43', '2019-06-18 05:50:05', 0, 0, 0),
(216, '100200', 41, '', '', '75', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-11-14 14:07:01', '2019-06-18 05:50:05', 0, 0, 0),
(217, '100201', 26, '', '', '250', '', 0, 0, 'Ordered', 'Order', '2018-11-15 10:20:01', '2019-06-18 05:50:05', 0, 0, 0),
(218, '100202', 26, '', '', '350', '', 0, 0, 'Ordered', 'Order', '2018-11-15 12:02:24', '2019-06-18 05:50:05', 0, 0, 0),
(219, '100203', 26, '', '', '190', '', 0, 0, 'Ordered', 'Order', '2018-11-15 14:47:11', '2019-06-18 05:50:05', 0, 0, 0),
(220, '100204', 103, '', '', '170', '', 25.7915, 55.9419, 'Ordered', 'Order', '2018-11-23 21:23:29', '2019-06-18 05:50:05', 0, 0, 0),
(221, '100205', 104, '23-11-2018', '12:15 AM', '50', '', 25.7913, 55.9416, 'Ordered', 'Preorder', '2018-11-23 22:46:00', '2019-06-18 05:50:05', 0, 0, 0),
(222, '100206', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-11-26 16:29:21', '2019-06-18 05:50:05', 0, 0, 0),
(223, '100207', 41, '1-12-2018', '9:16 PM', '11.25', '', 8.51934, 76.9599, 'Ordered', 'Preorder', '2018-11-27 14:16:40', '2019-06-18 05:50:05', 0, 0, 0),
(224, '100208', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-11-27 14:17:13', '2019-06-18 05:50:05', 0, 0, 0),
(225, '100209', 26, '', '', '6750', '', 8.52121, 76.9065, 'Ordered', 'Order', '2018-11-29 15:00:39', '2019-06-18 05:50:05', 0, 0, 0),
(226, '100210', 26, '6-12-2018', '3:16 PM', '12', '', 8.54638, 76.9603, 'Ordered', 'Preorder', '2018-11-29 15:16:55', '2019-06-18 05:50:05', 0, 0, 0),
(227, '100211', 26, '', '', '12', '', 8.51864, 76.9, 'Ordered', 'Order', '2018-11-29 15:49:18', '2019-06-18 05:50:05', 0, 0, 0),
(228, '100212', 41, '1-12-2018', '6:43 PM', '11.25', '', 0, 0, 'Ordered', 'Preorder', '2018-11-30 17:43:58', '2019-06-18 05:50:05', 0, 0, 0),
(229, '100213', 41, '', '', '77', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 15:31:57', '2019-06-18 05:50:05', 0, 0, 0),
(230, '100214', 41, '', '', '81', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 15:43:54', '2019-06-18 05:50:05', 0, 0, 0),
(231, '100215', 41, '', '', '57.5', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 15:46:48', '2019-06-18 05:50:05', 0, 0, 0),
(232, '100216', 41, '', '', '117.5', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 17:12:27', '2019-06-18 05:50:05', 0, 0, 0),
(233, '100217', 26, '', '', '1150', '', 8.56051, 76.8803, 'Ordered', 'Order', '2018-12-06 10:08:48', '2019-06-18 05:50:05', 0, 0, 0),
(234, '100218', 41, '', '', '246', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:11:02', '2019-06-18 05:50:05', 0, 0, 0),
(235, '100219', 41, '', '', '72', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:18:06', '2019-06-18 05:50:05', 0, 0, 0),
(236, '100220', 41, '', '', '45', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:21:20', '2019-06-18 05:50:05', 0, 0, 0),
(237, '100221', 41, '', '', '21', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:25:06', '2019-06-18 05:50:05', 0, 0, 0),
(238, '100222', 41, '', '', '36', 'aravind,test,test,test', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:35:17', '2019-06-18 05:50:05', 0, 0, 0),
(239, '100223', 26, '', '', '56.25', '', 8.56035, 76.88, 'Ordered', 'Order', '2018-12-06 16:26:33', '2019-06-18 05:50:05', 0, 0, 0),
(240, '100224', 26, '', '', '67.5', '', 8.56052, 76.8803, 'Ordered', 'Order', '2018-12-06 17:31:03', '2019-06-18 05:50:05', 0, 0, 0),
(241, '100225', 26, '', '', '1825', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56052, 76.8803, 'Ordered', 'Order', '2018-12-07 11:17:27', '2019-06-18 05:50:05', 0, 0, 0),
(242, '100226', 41, '', '', '191.25', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:26:59', '2019-06-18 05:50:05', 0, 0, 0),
(243, '100227', 41, '', '', '11.25', 'fhjk,fhjj,gjjk,vhji', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:30:02', '2019-06-18 05:50:05', 0, 0, 0),
(244, '100228', 41, '', '', '11.25', 'rrd,add,hjj,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:45:56', '2019-06-18 05:50:05', 0, 0, 0),
(245, '100229', 41, '', '', '48', 'fga,sggs,shbs,ahba', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:53:45', '2019-06-18 05:50:05', 0, 0, 0),
(246, '100230', 41, '', '', '48', 'ahbs,zvvs,svvd,vzvd', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:54:11', '2019-06-18 05:50:05', 0, 0, 0),
(247, '100231', 41, '', '', '46', 'rf,hhj,hjiu,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:54:47', '2019-06-18 05:50:05', 0, 0, 0),
(248, '100232', 41, '', '', '11.25', 'fgh,huk,yio,hjo', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:55:12', '2019-06-18 05:50:05', 0, 0, 0),
(249, '100233', 26, '', '', '3187.5', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56032, 76.8801, 'Ordered', 'Order', '2018-12-07 16:22:57', '2019-06-18 05:50:05', 0, 0, 0),
(250, '100234', 26, '', '', '12', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56032, 76.8801, 'Ordered', 'Order', '2018-12-07 16:23:15', '2019-06-18 05:50:05', 0, 0, 0),
(251, '100235', 26, '', '', '33.75', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56054, 76.8803, 'Ordered', 'Order', '2018-12-07 17:25:18', '2019-06-18 05:50:05', 0, 0, 0),
(252, '100236', 105, '', '', '90', 'shihab,dfff,dfgg,fffg', 8.75146, 76.9353, 'Ordered', 'Order', '2018-12-08 13:51:22', '2019-06-18 05:50:05', 0, 0, 0),
(253, '100237', 105, '', '', '58.5', 'shihab,dfff,dfgg,fffg', 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-11 22:53:55', '2019-06-18 05:50:05', 0, 0, 0),
(254, '100238', 26, '', '', '74.25', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.52686, 76.9913, 'Ordered', 'Order', '2018-12-13 20:24:06', '2019-06-18 05:50:05', 0, 0, 0),
(255, '100239', 41, '', '', '33.75', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:40:43', '2019-06-18 05:50:05', 0, 0, 0),
(256, '100240', 41, '', '', '34.5', 'dfyfh,guu,fyu,fhjk', 0, 0, 'Ordered', 'Order', '2018-12-14 10:42:16', '2019-06-18 05:50:05', 0, 0, 0),
(257, '100241', 41, '', '', '54', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:45:50', '2019-06-18 05:50:05', 0, 0, 0),
(258, '100242', 41, '', '', '33.75', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:46:46', '2019-06-18 05:50:05', 0, 0, 0),
(259, '100243', 41, '', '', '24', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:49:02', '2019-06-18 05:50:05', 0, 0, 0),
(260, '100244', 41, '', '', '24', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 10:50:45', '2019-06-18 05:50:05', 0, 0, 0),
(261, '100245', 41, '', '', '12', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 10:52:49', '2019-06-18 05:50:05', 0, 0, 0),
(262, '100246', 41, '', '', '12', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 12:22:17', '2019-06-18 05:50:05', 0, 0, 0),
(263, '100247', 41, '', '', '11.25', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 16:58:41', '2019-06-18 05:50:05', 0, 0, 0),
(264, '100248', 41, '', '', '24', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 16:59:16', '2019-06-18 05:50:05', 0, 0, 0),
(265, '100249', 41, '', '', '22.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 16:59:41', '2019-06-18 05:50:05', 0, 0, 0),
(266, '100250', 41, '', '', '11.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 17:00:12', '2019-06-18 05:50:05', 0, 0, 0),
(267, '100251', 41, '', '', '36', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 17:01:09', '2019-06-18 05:50:05', 0, 0, 0),
(268, '100252', 41, '', '', '6', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 17:01:51', '2019-06-18 05:50:05', 0, 0, 0),
(269, '100253', 41, '', '', '42', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 17:20:16', '2019-06-18 05:50:05', 0, 0, 0),
(270, '100254', 26, '', '', '1350', 'Rajesh,Sree Chithira Lane,Muttada,Kerala', 8.54016, 76.9504, 'Ordered', 'Order', '2018-12-16 01:25:05', '2019-06-18 05:50:05', 0, 0, 0),
(271, '100255', 41, '', '', '22.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:10:18', '2019-06-18 05:50:05', 0, 0, 0),
(272, '100256', 41, '', '', '11.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:11:37', '2019-06-18 05:50:05', 0, 0, 0),
(273, '100257', 41, '', '', '22.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:21:01', '2019-06-18 05:50:05', 0, 0, 0),
(274, '100258', 41, '', '', '46', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:21:19', '2019-06-18 05:50:05', 0, 0, 0),
(275, '100259', 146, '', '', '81.75', 'gxjck,Ali Plaza,Thiruvananthapuram,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-19 15:01:56', '2019-06-18 05:50:05', 0, 0, 0),
(276, '100260', 146, '', '', '82.5', 'Main Test,Ali Plaza,Thiruvananthapuram,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-20 14:43:41', '2019-06-18 05:50:05', 0, 0, 0),
(277, '100261', 164, '', '', '67.5', 'ajmal,gggg,ggfu,yuuu', 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-22 12:34:09', '2019-06-18 05:50:05', 0, 0, 0),
(278, '100262', 164, '', '', '6', 'ajmal,gggg,ggfu,yuuu', 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-22 12:39:11', '2019-06-18 05:50:05', 0, 0, 0),
(279, '100263', 146, '', '', '22.5', 'Main Test,Ali Plaza,Thiruvananthapuram,Kerala', 8.42617, 77.0337, 'Ordered', 'Order', '2018-12-22 18:34:06', '2019-06-18 05:50:05', 0, 0, 0),
(280, '100264', 170, '', '', '200.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.2852, 55.4463, 'Delivered', 'Order', '2018-12-23 17:06:39', '2019-06-18 05:50:05', 0, 0, 0),
(281, '100265', 160, '27-12-2018', '4:15 AM', '11.25', NULL, 8.51926, 76.9599, 'Ordered', 'Preorder', '2018-12-26 17:03:05', '2019-06-18 05:50:05', 0, 0, 0),
(282, '100266', 160, '27-12-2018', '4:15 AM', '11.25', NULL, 8.51926, 76.9599, 'Ordered', 'Preorder', '2018-12-26 17:23:09', '2019-06-18 05:50:05', 0, 0, 0),
(283, '100267', 52, '28-12-2018', '6:00 AM', '11.25', NULL, 8.56905, 76.8723, 'Ordered', 'Preorder', '2018-12-26 17:29:15', '2019-06-18 05:50:05', 0, 0, 0),
(284, '100268', 52, '27-12-2018', '12:28 PM', '11.25', NULL, 8.56975, 76.8733, 'Ordered', 'Preorder', '2018-12-26 17:29:57', '2019-06-18 05:50:05', 0, 0, 0),
(285, '100269', 160, '', '', '69.75', 'Anish,Sreekanteswara Temple Rd,Thiruvananthapuram,Kerala', 8.51939, 76.9599, 'Ordered', 'Order', '2018-12-26 17:31:02', '2019-06-18 05:50:05', 0, 0, 0),
(286, '100270', 52, '', '', '67.5', 'Baiskah,Kazhakootam Bus Stop,Kazhakkoottam,695582', 8.56792, 76.8736, 'Ordered', 'Order', '2018-12-26 17:31:09', '2019-06-18 05:50:05', 0, 0, 0),
(287, '100271', 160, '', '', '69.75', NULL, 8.51939, 76.9599, 'Ordered', 'Order', '2018-12-26 17:31:21', '2019-06-18 05:50:05', 0, 0, 0),
(288, '100272', 52, '', '', '67.5', NULL, 8.56792, 76.8736, 'Ordered', 'Order', '2018-12-26 17:31:35', '2019-06-18 05:50:05', 0, 0, 0),
(289, '100273', 160, '', '', '69.75', NULL, 8.51939, 76.9599, 'Ordered', 'Order', '2018-12-26 17:31:35', '2019-06-18 05:50:05', 0, 0, 0),
(290, '100274', 52, '', '', '67.5', NULL, 8.56792, 76.8736, 'Ordered', 'Order', '2018-12-26 17:31:40', '2019-06-18 05:50:05', 0, 0, 0),
(291, '100275', 52, '', '', '33.75', 'Rajesh,Pazhaya Mudipura Road,Nalanchira,Thiruvananthapuram', 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:33:06', '2019-06-18 05:50:05', 0, 0, 0),
(292, '100276', 52, '', '', '33.75', NULL, 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:33:38', '2019-06-18 05:50:05', 0, 0, 0),
(293, '100277', 52, '', '', '33.75', NULL, 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:34:31', '2019-06-18 05:50:05', 0, 0, 0),
(294, '100278', 52, '', '', '33.75', NULL, 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:34:45', '2019-06-18 05:50:05', 0, 0, 0),
(295, '100279', 160, '', '', '36', 'Amish,Ali Plaza,Thiruvananthapuram,695003', 8.51941, 76.9599, 'Ordered', 'Order', '2018-12-26 17:51:03', '2019-06-18 05:50:05', 0, 0, 0),
(296, '100280', 170, '28-12-2018', '6:46 PM', '11.25', NULL, 0, 0, 'Delivered', 'Preorder', '2018-12-26 18:16:31', '2019-06-18 05:50:05', 0, 0, 0),
(297, '100281', 170, '', '', '200.5', NULL, 25.2852, 55.4463, 'Ordered', 'Order', '2018-12-26 18:16:56', '2019-06-18 05:50:05', 0, 0, 0),
(298, '100282', 173, '', '', '307.5', 'ZEENATH,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,sdffru', 25.7912, 55.9436, 'Delivered', 'Order', '2018-12-27 20:53:25', '2019-06-18 05:50:05', 0, 0, 0),
(299, '100283', 173, '28-12-2018', '5:00 PM', '11.25', NULL, 25.7912, 55.9435, 'Ordered', 'Preorder', '2018-12-27 21:00:10', '2019-06-18 05:50:05', 0, 0, 0),
(300, '100284', 173, '', '', '307.5', NULL, 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-27 21:01:48', '2019-06-18 05:50:05', 0, 0, 0),
(301, '100285', 174, '', '', '11', 'sultan afzal,royal breeze 2,flat 202,rak', 25.7005, 55.7836, 'Ordered', 'Order', '2018-12-29 08:19:04', '2019-06-18 05:50:05', 0, 0, 0),
(302, '100286', 146, '', '', '82.5', NULL, 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-31 14:18:08', '2019-06-18 05:50:05', 0, 0, 0),
(303, '100287', 165, '', '', '1162.5', 'ajmal,Al Manama - Ras Al Khaimah Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7794, 55.9692, 'Ordered', 'Order', '2019-01-01 11:21:06', '2019-06-18 05:50:05', 0, 0, 0),
(304, '100288', 146, '5-1-2019', '9:06 PM', '11.25', NULL, 8.51935, 76.9599, 'Ordered', 'Preorder', '2019-01-01 16:06:46', '2019-06-18 05:50:05', 0, 0, 0),
(305, '100289', 146, '', '', '150', 'Main Test,Ali Plaza,Thiruvananthapuram,Kerala', 8.51935, 76.9599, 'Delivered', 'Order', '2019-01-01 16:07:05', '2019-06-18 05:50:05', 0, 0, 0),
(306, '100290', 146, '11-1-2019', '6:08 AM', '7', NULL, 8.51935, 76.9599, 'Ordered', 'Preorder', '2019-01-01 16:08:28', '2019-06-18 05:50:05', 0, 0, 0),
(307, '100291', 170, '', '', '693.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7907, 55.9416, 'Ordered', 'Order', '2019-01-01 17:22:55', '2019-06-18 05:50:05', 0, 0, 0),
(308, '100292', 170, '', '', '192.75', 'ra,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7895, 55.9419, 'Ordered', 'Order', '2019-01-01 23:06:39', '2019-06-18 05:50:05', 0, 0, 0),
(309, '100293', 170, '', '', '1730.25', 'ra,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7895, 55.9419, 'Delivered', 'Order', '2019-01-01 23:08:18', '2019-06-18 05:50:05', 0, 0, 0),
(310, '100294', 165, '', '', '677.5', 'nijam,Al Manama - Ras Al Khaimah Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7782, 55.9694, 'Ordered', 'Order', '2019-01-02 11:05:51', '2019-06-18 05:50:05', 0, 0, 0),
(311, '100295', 165, '', '', '677.5', NULL, 25.7782, 55.9694, 'Delivered', 'Order', '2019-01-02 11:06:50', '2019-06-18 05:50:05', 0, 0, 0),
(312, '100296', 170, '', '', '210', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7897, 55.9416, 'Cancelled', 'Order', '2019-01-03 15:30:44', '2019-06-18 05:50:05', 0, 0, 0),
(313, '100297', 170, '', '', '1400', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7908, 55.9419, 'Delivered', 'Order', '2019-01-03 16:19:45', '2019-06-18 05:50:05', 0, 0, 0),
(314, '100298', 170, '', '', '2109.75', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7778, 55.9677, 'Ordered', 'Order', '2019-01-03 18:26:00', '2019-06-18 05:50:05', 0, 0, 0),
(315, '100299', 170, '', '', '1279.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:12:01', '2019-06-18 05:50:05', 0, 0, 0),
(316, '100300', 170, '', '', '1279.5', NULL, 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:12:30', '2019-06-18 05:50:05', 0, 0, 0),
(317, '100301', 170, '10-1-2019', '12:40 PM', '11.25', NULL, 8.53939, 76.9455, 'Ordered', 'Preorder', '2019-01-03 23:29:34', '2019-06-18 05:50:05', 0, 0, 0),
(318, '100302', 170, '', '', '135', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:31:17', '2019-06-18 05:50:05', 0, 0, 0),
(319, '100303', 170, '', '', '135', NULL, 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:41:12', '2019-06-18 05:50:05', 0, 0, 0),
(320, '100304', 170, '', '', '135', NULL, 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:46:27', '2019-06-18 05:50:05', 0, 0, 0),
(321, '100305', 170, '', '', '11.25', 'Rajesh,110,Thiruvananthapuram,Kerala', 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-04 00:10:22', '2019-06-20 08:22:14', 1, 0, 0),
(322, '100306', 170, '', '', '11.25', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.55919, 76.8778, 'Ordered', 'Order', '2019-01-04 13:58:20', '2019-06-18 05:50:05', 0, 0, 0),
(323, '100307', 153, '4-1-2019', '7:45 PM', '12', NULL, 8.51937, 76.9599, 'Ordered', 'Preorder', '2019-01-04 15:46:03', '2019-06-18 05:50:05', 0, 0, 0),
(324, '100308', 153, '11-1-2019', '10:49 PM', '12', NULL, 8.51937, 76.9599, 'Ordered', 'Preorder', '2019-01-04 15:50:03', '2019-06-18 05:50:05', 0, 0, 0),
(325, '100309', 153, '', '', '36', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-04 16:07:12', '2019-06-18 05:50:05', 0, 0, 0),
(326, '100310', 153, '', '', '100', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-04 16:48:31', '2019-06-20 08:22:28', 1, 0, 0),
(327, '100311', 153, '', '', '36', NULL, 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-04 17:03:02', '2019-06-18 05:50:05', 0, 0, 0),
(328, '100312', 170, '', '', '39.25', 'Rajesh,techno park Kazhakoottam,Thiruvananthapuram,Kerala', 8.55655, 76.8766, 'Ordered', 'Order', '2019-01-04 20:22:32', '2019-06-18 05:50:05', 0, 0, 0),
(329, '100313', 165, '', '', '225', 'nijam,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7784, 55.9677, 'Ordered', 'Order', '2019-01-05 11:04:34', '2019-06-18 05:50:05', 0, 0, 0),
(330, '100314', 165, '', '', '225', NULL, 25.7784, 55.9677, 'Ordered', 'Order', '2019-01-05 11:17:15', '2019-06-18 05:50:05', 0, 0, 0),
(331, '100315', 170, '', '', '56.25', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.33198, 77.0523, 'Ordered', 'Order', '2019-01-05 17:49:08', '2019-06-18 05:50:05', 0, 0, 0),
(332, '100316', 170, '', '', '67.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.56053, 76.8804, 'Ordered', 'Order', '2019-01-07 17:22:34', '2019-06-18 05:50:05', 0, 0, 0),
(333, '100317', 170, '', '', '67.5', NULL, 8.56053, 76.8804, 'Ordered', 'Order', '2019-01-07 17:23:39', '2019-06-18 05:50:05', 0, 0, 0),
(334, '100318', 170, '24-1-2019', '1:45 PM', '11.25', NULL, 25.791, 55.9416, 'Ordered', 'Preorder', '2019-01-12 15:15:43', '2019-06-18 05:50:05', 0, 0, 0),
(335, '100319', 153, '', '', '666.5', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-17 17:30:30', '2019-06-18 05:50:05', 0, 0, 0),
(336, '100320', 180, '', '', '232.5', 'fggh,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7897, 55.9419, 'Ordered', 'Order', '2019-01-19 15:04:47', '2019-06-18 05:50:05', 0, 0, 0),
(337, '100321', 180, '', '', '232.5', NULL, 25.7897, 55.9419, 'Ordered', 'Order', '2019-01-19 15:06:00', '2019-06-18 05:50:05', 0, 0, 0),
(338, '100322', 181, '', '', '342.75', 'dgghf,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7912, 55.9434, 'Ordered', 'Order', '2019-01-21 00:35:03', '2019-06-18 05:50:05', 0, 0, 0),
(339, '100323', 181, '', '', '342.75', NULL, 25.7912, 55.9434, 'Ordered', 'Order', '2019-01-21 00:37:16', '2019-06-18 05:50:05', 0, 0, 0),
(340, '100324', 180, '', '', '700', 'Ajmal,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7908, 55.9409, 'Delivered', 'Order', '2019-01-26 14:58:13', '2019-06-18 05:50:05', 0, 0, 0),
(341, '100325', 197, '', '', '7', 'najim,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7912, 55.9435, 'Ordered', 'Order', '2019-01-26 15:39:13', '2019-06-18 05:50:05', 0, 0, 0),
(342, '100326', 153, '', '', '27', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.56058, 76.8804, 'Ordered', 'Order', '2019-01-29 14:53:47', '2019-06-18 05:50:05', 0, 0, 0),
(343, '100327', 153, '', '', '14', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-01 17:02:13', '2019-06-18 05:50:05', 0, 0, 0),
(344, '100328', 153, '', '', '22.5', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-01 17:16:42', '2019-06-18 05:50:05', 0, 0, 0),
(345, '100329', 153, '', '', '33.75', 'aravindAli PlazaKowdiarKerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-02-06 16:39:03', '2019-06-18 05:50:05', 0, 0, 0),
(346, '100330', 153, '', '', '43.50', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51938, 76.9599, 'Ordered', 'Order', '2019-02-07 10:53:22', '2019-06-18 05:50:05', 0, 0, 0),
(347, '100331', 153, '', '', '21.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-07 11:28:35', '2019-06-18 05:50:05', 0, 0, 0),
(348, '100332', 205, '', '', '45.00', 'Al yamuna densons fze,Unnamed Road - Ras al Khaimah - United Arab Emirates,Al Riffa,Ras al Khaimah', 25.7123, 55.845, 'Delivered', 'Order', '2019-02-11 08:35:43', '2019-06-18 05:50:05', 0, 0, 0),
(349, '100333', 153, '', '', '15.70', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Delivered', 'Order', '2019-02-11 14:47:35', '2019-06-18 05:50:05', 0, 0, 0),
(350, '100334', 153, '2019-02-15 00:00:00.000', '0001-01-01 16:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 11:56:37', '2019-06-18 05:50:05', 0, 0, 0),
(351, '100335', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 11:58:19', '2019-06-18 05:50:05', 0, 0, 0),
(352, '100336', 153, '2019-02-21 00:00:00.000', '0001-01-01 19:00:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 11:59:14', '2019-06-18 05:50:05', 0, 0, 0),
(353, '100337', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:01:35', '2019-06-18 05:50:05', 0, 0, 0),
(354, '100338', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:04:24', '2019-06-18 05:50:05', 0, 0, 0),
(355, '100339', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:07:20', '2019-06-18 05:50:05', 0, 0, 0),
(356, '100340', 153, '2019-02-27 00:00:00.000', '0001-01-01 18:00:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:12:59', '2019-06-18 05:50:05', 0, 0, 0),
(357, '100341', 153, '2019-02-28 00:00:00.000', '0001-01-01 17:39:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:39:49', '2019-06-18 05:50:05', 0, 0, 0),
(358, '100342', 153, '2019-02-27 00:00:00.000', '0001-01-01 18:40:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:40:27', '2019-06-18 05:50:05', 0, 0, 0),
(359, '100343', 153, '2019-02-20 00:00:00.000', '0001-01-01 21:42:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:42:25', '2019-06-18 05:50:05', 0, 0, 0),
(360, '100344', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:45:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:45:17', '2019-06-18 05:50:05', 0, 0, 0),
(361, '100345', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:46:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:46:20', '2019-06-18 05:50:05', 0, 0, 0),
(362, '100346', 153, '2019-02-21', '05:47PM', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:47:08', '2019-06-18 05:50:05', 0, 0, 0),
(363, '100347', 153, '2019-02-14', '11:55PM', '36', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:55:33', '2019-06-18 05:50:05', 0, 0, 0),
(364, '100348', 153, '', '', '174.25', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-14 15:11:46', '2019-06-18 05:50:05', 0, 0, 0),
(365, '100349', 153, '', '', '60.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-14 15:19:35', '2019-06-18 05:50:05', 0, 0, 0),
(366, '100350', 196, '', '', '108.00', 'shihabudeen,Al Muntasir Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7795, 55.9681, 'Ordered', 'Order', '2019-02-20 20:43:48', '2019-06-18 05:50:05', 0, 0, 0),
(367, '100351', 208, '???? ???????', '???? ?????', '9', NULL, 25.3868, 55.4538, 'Ordered', 'Preorder', '2019-02-24 14:32:39', '2019-06-18 05:50:05', 0, 0, 0),
(368, '100352', 209, '25-2-2019', '6:50 PM', '11.5', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-25 00:20:21', '2019-06-18 05:50:05', 0, 0, 0),
(369, '100353', 205, '', '', '45.00', 'Al yamuna densons,Unnamed Road - Ras al Khaimah - United Arab Emirates,Al Riffa,Ras al Khaimah', 25.7123, 55.845, 'Ordered', 'Order', '2019-02-25 21:09:29', '2019-06-18 05:50:05', 0, 0, 0),
(370, '100354', 187, '', '', '54.00', 'vijay,Unnamed Road - Ras al Khaimah - United Arab Emirates,Al Jazirah Al Hamra,Ras al Khaimah', 25.6862, 55.7999, 'Ordered', 'Order', '2019-03-05 10:09:08', '2019-06-18 05:50:05', 0, 0, 0),
(371, '100355', 153, '', '', '35.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-03-11 16:44:41', '2019-06-18 05:50:05', 0, 0, 0),
(372, '100356', 153, '', '', '107.50', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.56035, 76.8803, 'Ordered', 'Order', '2019-03-19 12:17:44', '2019-06-18 05:50:05', 0, 0, 0),
(373, '100357', 153, '', '', '42.00', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 11.1105, 77.3418, 'Ordered', 'Order', '2019-03-21 11:59:44', '2019-06-18 05:50:05', 0, 0, 0),
(374, '100358', 153, '', '', '14.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-03-21 12:41:55', '2019-06-18 05:50:05', 0, 0, 0),
(375, '100359', 153, '2019-03-21', '04:46PM', '34.5', NULL, 0, 0, 'Ordered', 'Preorder', '2019-03-21 12:46:50', '2019-06-18 05:50:05', 0, 0, 0),
(376, '100360', 217, '', '', '234.50', 'hassan,ajman,ajmam,ajman', 25.387, 55.4534, 'Ordered', 'Order', '2019-03-24 20:53:57', '2019-06-18 05:50:05', 0, 0, 0),
(377, '100361', 153, '', '', '7.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-03-27 18:04:43', '2019-06-18 05:50:05', 0, 0, 0),
(378, '100362', 153, '', '', '7.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-01 13:33:00', '2019-06-18 05:50:05', 0, 0, 0),
(379, '100363', 153, '', '', '25.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-02 17:20:29', '2019-06-18 05:50:05', 0, 0, 0),
(380, '100364', 179, '', '', '28.00', 'ramshiAl Jazeerah Industrial1Al Jazeerah Industrial1Ras al-Khaimah', 25.6923, 55.7995, 'Delivered', 'Order', '2019-04-02 18:34:09', '2019-06-18 05:50:05', 0, 0, 0),
(381, '100365', 179, '', '', '262.25', 'ramshirakrakras all khaimah', 0, 0, 'Delivered', 'Order', '2019-04-02 18:35:08', '2019-06-18 05:50:05', 0, 0, 0),
(382, '100366', 179, '', '', '54.00', 'ramshirakrakras all khaimah', 0, 0, 'Delivered', 'Order', '2019-04-13 18:49:14', '2019-06-18 05:50:05', 0, 0, 0),
(383, '100367', 153, '', '', '83.50', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-25 16:34:52', '2019-06-18 05:50:05', 0, 0, 0),
(384, '100368', 177, '', '', '290.00', 'AjmalUnnamed Road - Ras al Khaimah - United Arab EmiratesRas Al-KhaimahRas al Khaimah', 25.7911, 55.9437, 'Ordered', 'Order', '2019-04-25 18:35:48', '2019-06-18 05:50:05', 0, 0, 0),
(385, '100369', 153, '2019-04-27', '11:44AM', '45', NULL, 0, 0, 'Ordered', 'Preorder', '2019-04-26 11:44:12', '2019-06-18 05:50:05', 0, 0, 0),
(386, '100370', 153, '', '', '18.25', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-29 10:58:09', '2019-06-18 05:50:05', 0, 0, 0),
(387, '100371', 153, '', '', '122.25', 'Rajesh695581KazhakkottamKerala', 8.56039, 76.8805, 'Ordered', 'Order', '2019-04-29 15:57:42', '2019-06-18 05:50:05', 0, 0, 0),
(388, '100372', 153, '2019-05-15', '10:58PM', '230', NULL, 0, 0, 'Ordered', 'Preorder', '2019-04-29 15:59:18', '2019-06-18 05:50:05', 0, 0, 0),
(389, '100373', 153, '', '', '126.00', 'Vivek695581KazhakkottamKerala', 8.56039, 76.8805, 'Ordered', 'Order', '2019-04-29 15:59:59', '2019-06-18 05:50:05', 0, 0, 0),
(390, '100374', 153, '', '', '1375.00', 'johnKazhakuttomKazhakuttomKerala', 8.56042, 76.8806, 'Ordered', 'Order', '2019-05-02 11:16:42', '2019-06-18 05:50:05', 0, 0, 0),
(391, '100375', 153, '2019-05-16', '11:20AM', '105', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:20:31', '2019-06-18 05:50:05', 0, 0, 0),
(392, '100376', 153, '2019-05-18', '09:20AM', '156', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:20:58', '2019-06-26 05:56:17', 1, 0, 0),
(393, '100377', 153, '2019-05-17', '10:21AM', '98', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:21:38', '2019-06-26 05:56:13', 1, 0, 0),
(394, '100378', 153, '2019-05-27', '02:29PM', '84', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:29:25', '2019-06-26 05:56:10', 1, 0, 0),
(395, '100379', 177, '', '', '35.00', 'AjmalUnnamed Road - Ras al Khaimah - United Arab EmiratesRas Al-KhaimahRas al Khaimah', 0, 0, 'Ordered', 'Order', '2019-05-27 13:56:47', '2019-06-18 05:50:05', 0, 0, 0),
(400, '100380', 107, '', '', '21', 'MARIS,MARIS,MARIS,0000', 8.56035, 76.8803, 'Ordered', 'Order', '0000-00-00 00:00:00', '2019-07-03 07:34:20', 330, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `deli_staff_stocks`
--

CREATE TABLE `deli_staff_stocks` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(15) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deli_stocks`
--

CREATE TABLE `deli_stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `batch_no` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `returned_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `damaged_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sample_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `available_quantity` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `packing_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deli_staff_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `deli_stocks`
--

INSERT INTO `deli_stocks` (`id`, `batch_no`, `category_id`, `quantity`, `returned_quantity`, `damaged_quantity`, `sample_quantity`, `available_quantity`, `value`, `packing_date`, `expiry_date`, `created_at`, `updated_at`, `deleted_at`, `deli_staff_id`) VALUES
(1, '500-001', 2, 1500, '', '', '', 1500, 10125, '2017-03-05 00:00:00', '2018-03-05 00:00:00', '2017-03-05 17:03:25', '2017-03-05 17:14:18', '2017-03-05 17:14:18', 0),
(2, '1.5-002', 1, 500, '', '', '', 500, 3375, '2017-03-05 00:00:00', '2018-03-05 00:00:00', '2017-03-05 17:04:47', '2017-03-05 17:14:27', '2017-03-05 17:14:27', 0),
(3, '500-003', 2, 500, '', '', '', 500, 3375, '2017-03-01 00:00:00', '2018-03-01 00:00:00', '2017-03-05 17:10:36', '2017-03-05 17:14:31', '2017-03-05 17:14:31', 0),
(4, '1.5-004', 1, 500, '', '', '', 100, 3725, '2017-03-05 00:00:00', '2018-03-04 00:00:00', '2017-03-10 11:27:53', '2017-04-08 18:38:20', '2017-04-08 18:38:20', 0),
(5, '500-005', 2, 1000, '', '', '', 1000, 6750, '2017-03-05 00:00:00', '2018-03-04 00:00:00', '2017-03-10 11:29:21', '2017-04-08 18:38:24', '2017-04-08 18:38:24', 0),
(6, '1.5-006', 1, 1000, '', '', '', 0, 6750, '2017-03-08 00:00:00', '2018-03-07 00:00:00', '2017-03-10 11:32:57', '2017-04-08 18:38:32', '2017-04-08 18:38:32', 0),
(7, '1.5-007', 1, 500, '', '', '', 500, 3725, '2017-03-05 00:00:00', '2018-03-04 00:00:00', '2017-03-10 11:38:05', '2017-04-08 18:38:36', '2017-04-08 18:38:36', 0),
(8, '1.5-008', 1, 1000, '', '', '', 775, 100, '2017-03-23 00:00:00', '2018-03-22 00:00:00', '2017-03-10 13:46:00', '2017-04-08 18:38:40', '2017-04-08 18:38:40', 0),
(9, '500-009', 2, 2304, '', '', '', 88, 15552, '2017-04-03 00:00:00', '2018-04-02 00:00:00', '2017-04-14 18:59:57', '2017-09-07 15:31:44', '2017-09-07 15:31:44', 0),
(10, '330-010', 3, 1008, '', '', '', 704, 6804, '2017-04-19 00:00:00', '2018-04-18 00:00:00', '2017-04-24 18:20:07', '2017-09-07 15:31:48', '2017-09-07 15:31:48', 0),
(11, '1.5-011', 1, 1584, '', '', '', 1034, 10692, '2017-04-22 00:00:00', '2018-04-21 00:00:00', '2017-04-24 18:22:18', '2017-09-07 15:31:54', '2017-09-07 15:31:54', 0),
(12, '1.5-012', 1, 1584, '', '', '', 1584, 10692, '2017-04-27 00:00:00', '2018-04-26 00:00:00', '2017-04-28 19:06:02', '2017-09-07 15:32:05', '2017-09-07 15:32:05', 0),
(13, '500-013', 2, 2304, '', '', '', 2304, 15552, '2017-04-30 00:00:00', '2018-04-29 00:00:00', '2017-05-10 14:33:02', '2017-09-07 15:32:13', '2017-09-07 15:32:13', 0),
(14, '200-014', 5, 780, '', '', '', 780, 3315, '2017-05-03 00:00:00', '2018-05-02 00:00:00', '2017-05-10 14:41:34', '2017-09-07 15:32:18', '2017-09-07 15:32:18', 0),
(15, '200-015', 5, 780, '', '', '', 780, 3315, '2017-05-09 00:00:00', '2018-05-08 00:00:00', '2017-05-10 14:42:51', '2017-09-06 18:04:10', '2017-09-06 18:04:10', 0),
(16, '200-016', 5, 780, '', '', '', 780, 3315, '2017-05-11 00:00:00', '2018-05-10 00:00:00', '2017-05-12 09:24:37', '2017-09-06 18:03:58', '2017-09-06 18:03:58', 0),
(17, '1.5-017', 1, 100, '2', '', '2', 75, 980, '2017-09-07 00:00:00', '2018-09-07 00:00:00', '2017-09-07 15:36:27', '2017-11-05 08:37:55', '2017-11-05 08:37:55', 0),
(18, '500-018', 2, 250, '', '', '', 200, 1688, '2017-09-13 00:00:00', '2018-09-12 00:00:00', '2017-09-12 19:39:14', '2017-11-05 08:37:45', '2017-11-05 08:37:45', 0),
(19, '1.5-019', 1, 250, '', '', '', 250, 1688, '2017-09-13 00:00:00', '2018-08-13 00:00:00', '2017-09-12 19:39:17', '2017-09-12 19:41:33', '2017-09-12 19:41:33', 0),
(20, '500-020', 2, 500, '13', '', '', 500, 5000, '2017-10-11 00:00:00', '2018-10-30 00:00:00', '2017-10-11 15:33:54', '2017-11-05 08:37:27', '2017-11-05 08:37:27', 0),
(21, '500-021', 2, 350, '11', '11', '15', 324, 3500, '2017-10-11 00:00:00', '2018-10-23 00:00:00', '2017-10-11 15:37:02', '2017-11-05 08:37:09', '2017-11-05 08:37:09', 0),
(22, '500-022', 2, 640, '', '', '', 640, 6400, '2017-10-16 00:00:00', '2018-10-16 00:00:00', '2017-10-16 14:22:40', '2017-11-05 08:37:00', '2017-11-05 08:37:00', 0),
(23, '500-023', 2, 340, '', '8', '7', 340, 3400, '2017-10-22 00:00:00', '2018-10-22 00:00:00', '2017-10-22 04:13:45', '2017-11-05 08:36:44', '2017-11-05 08:36:44', 0),
(24, '1.5-024', 1, 550, '4', '9', '11', 430, 5000, '2017-10-22 00:00:00', '2018-10-22 00:00:00', '2017-10-22 04:17:09', '2017-11-05 08:36:36', '2017-11-05 08:36:36', 0),
(25, '1.5-025', 1, 700, '', '', '14', 700, 7000, '2017-10-25 00:00:00', '2018-10-25 00:00:00', '2017-10-25 05:23:04', '2017-11-05 08:36:19', '2017-11-05 08:36:19', 0),
(26, '1.5-026', 1, 100, '', '', '5', 67, 1000, '2017-11-05 00:00:00', '2018-11-05 00:00:00', '2017-11-05 08:39:38', '2017-11-05 09:14:23', NULL, 0),
(27, '500-027', 2, 50, '', '7', '', 50, 500, '2017-11-05 00:00:00', '2018-11-05 00:00:00', '2017-11-05 08:40:18', '2017-11-05 08:47:52', NULL, 0),
(28, '1.5-028', 1, 80, '', '', '', 25, 800, '2017-11-01 00:00:00', '2018-11-01 00:00:00', '2017-11-05 08:41:26', '2017-11-05 09:04:51', NULL, 0),
(29, '1.5-029', 1, 150, '', '', '', 98, 1500, '2017-11-03 00:00:00', '2018-11-03 00:00:00', '2017-11-05 09:15:47', '2017-11-05 09:21:39', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `deli_user`
--

CREATE TABLE `deli_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(222) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` int(15) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `passport` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `iqama` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `joined` datetime NOT NULL,
  `employeetype` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `address`, `phone`, `email`, `passport`, `iqama`, `joined`, `employeetype`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'AJMAL SHIHABUDEEN', 'al yanabea', '0522256788', 'ajmalshihab3@gmail.com', 'j9201332', '243434343', '2017-03-01 00:00:00', 1, '2017-03-10 11:43:06', '2017-03-10 11:43:06', NULL),
(2, 'temp_helper', 'asdfghklouyggdggdgg', '1234567898', '', '13455', '123654', '2017-09-01 00:00:00', 2, '2017-09-07 15:34:28', '2017-09-07 15:34:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employeetypes`
--

CREATE TABLE `employeetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeetype` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employeetypes`
--

INSERT INTO `employeetypes` (`id`, `employeetype`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Driver', '2017-03-10 11:41:30', '2017-03-10 11:41:30', NULL),
(2, 'Helper', '2017-09-07 15:33:35', '2017-09-07 15:33:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `category` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `vehicle` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `remarks` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expensetypes`
--

CREATE TABLE `expensetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `expensetype` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login_api`
--

CREATE TABLE `login_api` (
  `user_id` int(15) NOT NULL,
  `phone_number` varchar(250) NOT NULL,
  `alternate_phonenumber` varchar(250) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `govt_employee` varchar(15) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL,
  `address_one` varchar(250) NOT NULL,
  `address_two` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `status` int(15) NOT NULL,
  `profile_image` varchar(250) NOT NULL,
  `otp` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `fcm_id` longtext,
  `refcode` varchar(250) NOT NULL,
  `refered_by` varchar(250) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_api`
--

INSERT INTO `login_api` (`user_id`, `phone_number`, `alternate_phonenumber`, `user_name`, `password`, `govt_employee`, `latitude`, `longitude`, `address_one`, `address_two`, `state`, `pincode`, `status`, `profile_image`, `otp`, `email`, `fcm_id`, `refcode`, `refered_by`, `created_at`, `updated_at`) VALUES
(26, '9947817003', '8289954409', 'Anish', '1234', 'yes', '8.5603878', '76.8799732', 'rajesh villa', 'muttada.p.o', 'Thiruvananthapuram', 'India', 0, '5c18dd317fbca', '1234', 'anishkris@gmail.com', 'cWYwbtDBPRI:APA91bGFVLfQN65lmePLb8zEnfPuB8Xup6-vH_AnIBpJslrH6matvcbg1uRgCizxDhR_jDtLDqaDQx93UsBio4uAFBnFB2gqBuDmk4hMKwABc7XrU-fjV1M9FywvLWGFMVQq9QNpvp_9', '', NULL, '2018-07-12 13:03:19', '2018-12-18 17:12:41'),
(31, '8848280696', '', 'paul', '898956', 'no', '8.5566328', '76.8820492', 'Thejaswini', 'Technopark Road', 'Thiruvananthapuram', '695581', 0, '', '', '', '', '', NULL, '2018-07-13 04:14:13', '0000-00-00 00:00:00'),
(50, '7907750306', '8289954409', 'Paul', 'tXCTbE6H', 'no', '8.5604696', '76.8802609', 'Nila Building', 'Technopark Road', 'Thiruvananthapuram', '695581', 0, '5b682975df734', '', 'rajeshmra18@gmail.com', '', '', NULL, '2018-08-02 14:17:09', '2018-08-06 16:26:53'),
(51, '9274566215', '', 'nithin', '123456', 'yes', '0.0', '0.0', 'address 1', 'address 2', 'state', '698754', 0, '', '', '', '', '', NULL, '2018-08-02 15:30:45', '2018-08-02 15:30:45'),
(95, '9526633399', '', 'aby', '123', 'no', '8.5605504', '76.8803343', 'Nila Building', 'Technopark Road', 'Thiruvananthapuram', 'India', 0, '', '980825', '', '', '', NULL, '2018-11-15 17:47:17', '2018-11-15 17:47:17'),
(116, '7907330422', '', 'Vaisakh V S', '862607', 'no', '8.557702', '76.8788822', 'Trivandrum', 'TVM', 'Kerala', '695006', 0, '', '862607', '', NULL, '', NULL, '2018-12-17 16:28:58', '2018-12-17 16:28:58'),
(117, '8089000673', '', 'Vishnu V S', '805729', 'no', '8.5603749', '76.8787982', 'Trivandrum', 'TVM', 'Kerala', '695006', 0, '', '805729', '', NULL, '', 'MR962480', '2018-12-17 17:49:54', '2018-12-17 17:49:54'),
(153, '1234567891', '5528258235', 'aravind', '370740', 'yes', '0.0', '0.0', '695581', 'Kazhakkottam', 'Kerala', '695581', 0, '5cca881edca6b', '951318', 'aravind@bgi', '', 'MR270962', '', '2018-12-19 14:06:25', '2019-05-02 11:33:10'),
(159, '9961311636', '', 'Vaisakh V S', '807809', 'no', '8.5603547', '76.880056', 'Nila Building', 'Thiruvananthapuram', 'Kerala', '695581', 0, '', '807809', '', '', 'MR998029', '', '2018-12-19 17:37:17', '2018-12-19 17:37:17'),
(167, '7907374911', '', 'Anish', '744231', 'yes', '8.50777654', '76.94994778', 'Pmg Hanuman Temple', 'Thiruvananthapuram', 'Kerala', '695033', 0, '', '744231', '', 'ccQRSmJikeg:APA91bHbl-crE8akKhNW33QOaNvNG6HuOeHDgLwSwCDLkPa8mOY0S1Wptno1gEHVhUpn3_hPtU8PF0lCib4rwORXMQp8ibRQgMWcBRrk2FmCWIJn6PQVsJSUdxb4NTH4UfkFPn1AcOMo', 'MR393383', '', '2018-12-22 19:02:43', '2018-12-22 19:02:43'),
(171, '9947817009', '', 'anish', '433054', 'yes', '8.56997393', '76.97184663', 'Eanikkara Kallayam Vattappara Rd', 'Thiruvananthapuram', 'Kerala', '695043', 0, '', '433054', '', 'ccQRSmJikeg:APA91bHbl-crE8akKhNW33QOaNvNG6HuOeHDgLwSwCDLkPa8mOY0S1Wptno1gEHVhUpn3_hPtU8PF0lCib4rwORXMQp8ibRQgMWcBRrk2FmCWIJn6PQVsJSUdxb4NTH4UfkFPn1AcOMo', 'MR464669', 'MR614999', '2018-12-24 11:01:08', '2018-12-24 11:01:08'),
(176, '8300123349', '', 'Nithin', '945005', 'no', '8.4954514', '76.932672', 'TC 30/1415(3)', 'Thiruvananthapuram', 'Kerala', '695024', 0, '', '945005', '', 'd7jPRyU_gMY:APA91bGdP6mCFjxhS7x0dezrU7577a89c37qdN3ZyUAkSmWGLzQvm6AB-y489IFB07kgjq5DvIeRgNlP3zjbMyz5QX9DhrFbiGXyAc07BNhI7lBaYHyXMIYjKroTWck0E9ksziGGyMWQ', 'MR614999', '', '2019-01-04 11:29:37', '2019-01-04 11:29:37'),
(177, '0553260078', '', 'Ajmal', '898355', 'no', '25.7913122', '55.9416008', 'Unnamed Road - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '905484', '', 'null', 'MR655181', '', '2019-01-15 14:08:16', '2019-01-15 14:08:16'),
(180, '0522256788', '', 'Ajmal', '850223', 'no', '25.7896968', '55.9418928', 'Unnamed Road - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '850223', '', 'c5KxHxscAxQ:APA91bFL-VrzcEy9K_OUol-mWAQQCkKrpPuPzFi9V1UXY85dJI7wKSf_dOnp7UgWAt8tiCXc5jN6xW59Tx1dRuD-x3WT0ufYxNRiACGdWGGF6R32YVO2leWh-HnnFptBwL6AnQy3cdLQ', 'MR285123', 'MR655181', '2019-01-19 14:54:20', '2019-01-19 14:54:20'),
(181, '0529733695', '', 'shafi', '673255', 'no', '25.790945000000004', '55.943405', 'Unnamed Road - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '673255', '', 'cgL0jwzUZeU:APA91bH4lX4Ilg-w9XiA5N28ivyFvpnri9-CV4DjJbsp1w1UvLjkJxaoC0wc7IHdDqa24GrKvL8Cb3BchCBBF_mVvwF6gZsO90EbL1yoNYrh9HnjylIy2grx-7w5bUiebXO_SoG33uhM', 'MR562694', '', '2019-01-21 00:30:09', '2019-01-21 00:30:09'),
(186, '0567749277', '', 'nijam', '148379', 'no', '25.7173588', '55.8608878', 'E11 - Ras al Khaimah - United Arab Emirates', 'Al Riffa', 'Ras al Khaimah', '', 0, '', '148379', '', 'f-vkxObW5uY:APA91bFIC1DxPu6uHe8Mr7LqBAaHzQYKB1Oz2VY9pTKCIU_PRceFtYN0U38iL6J8rvpqf3m06TRGvDvF1VxdoWpIHIoQ-hiXfqJpJIZCtyhyK7wfDTTO5zj3kBaF4bzu2MxWHwID4ikV', 'MR447104', '', '2019-01-23 12:22:43', '2019-01-23 12:22:43'),
(187, '0588983214', '', 'vijay', '310846', 'no', '25.6863091', '55.7998242', 'Unnamed Road - Ras al Khaimah - United Arab Emirates', 'Al Jazirah Al Hamra', 'Ras al Khaimah', '', 0, '', '310846', '', 'c1LHJR9G7rE:APA91bEqc4F9qZFGw0QcP50Q2zuSLwX97lyEeFuPDOt3AFaILlNi5DClhAuDsOEynjhISM1Z7QQb2UJjZcNMKyjQ8NqQ4Ez-GdXgBbY5v9pAPYjOrjOdH_0wlmicr1-cSzf_Dl88o-mG', 'MR203491', '', '2019-01-23 12:52:41', '2019-01-23 12:52:41'),
(196, '0505401766', '', 'shihabudeen', '150426', 'no', '25.7760558', '55.9673605', 'Al Muntasir Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '150426', '', 'ddiTN-aOO-U:APA91bGwN3kmsSbfmiYXrlDlDczEu3tL4OEtK8WGx79C2ClWPywV2k5AoKV2mAyFlvs5DlOMfp1eKrgQaC_2uhLVPYAieRFysoD2nhOmAMoWfR5c6D0tbf4BG6RtH5NiZuZ9jOVc9fIW', 'MR356148', '', '2019-01-26 11:26:29', '2019-01-26 11:26:29'),
(197, '0559218040', '', 'najim', '444444', 'no', '25.7912379', '55.9435066', 'Unnamed Road - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '444444', '', 'c0_feR4j_sQ:APA91bEhZqi1_WU0zWVxxbQnUxdspIfLuUpx_A_ZU7Bk1zrB7xUGgtEAAZft8sAT9ui258fdkjG3x5D35tZoRORd1oAauUZtYmJJmiz1DeoB2dlZQO5CuB4PdTkBzIShTAZ6rCaztWpV', 'MR879201', '', '2019-01-26 15:37:03', '2019-01-26 15:37:03'),
(198, '0557106929', '', 'aboobakker koya', '268679', 'no', '25.845002038978954', '56.004646460671374', 'julan', 'Julan Al sura laundry', 'ras al khaima', '0971', 0, '', '268679', '', '', 'MR425010', '', '2019-01-31 13:24:36', '2019-01-31 13:24:36'),
(199, '0529140808', '', 'aboobakker koya', '735690', 'no', '25.844935903832113', '56.00478479104881', 'Al Mataf Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '735690', '', '', 'MR801396', '', '2019-01-31 13:28:33', '2019-01-31 13:28:33'),
(200, '0558531715', '', 'Aliton das', '611323', 'no', '25.8399171', '55.9993127', 'Al Mataf Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '611323', '', 'fMxKDRwP0Y4:APA91bH_MBtl9te-S8Iyd_pjnYc7CIAAZIMWPn4RrCEovfcd5FHRZsU9Wcu7zthAQFngdv6XE7D1-OH7Sz3-tR6kOFX-djgSjX4_yU3jxB2FvNr4lWHHy4bgsNL7buF-tzN9xyb8Ys9H', 'MR601689', '', '2019-02-05 19:28:44', '2019-02-05 19:28:44'),
(201, '0544750864', '', 'suman kumar', '355925', 'no', '25.8401991', '55.9995474', 'Al Mataf Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '355925', '', 'fLoiHLJT7r8:APA91bHyssTkyUeIjk9C1IFb9XngDDW7RQNQgumrESV6cfO1xGCb995ySF6LY42b0sAXazNXpImHeBasPa7sLaWHlJU5vjlkb1TLVwCw2mRgYSHkH1E3V-qqNZZuEVoQ3SG1to45owOG', 'MR245021', '', '2019-02-05 19:40:31', '2019-02-05 19:40:31'),
(202, '0558014952', '', 'Mohammed Abdur Razzak', '144149', 'no', '25.8400493', '55.9994516', 'Al Mataf Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '144149', '', 'd16ntmWTwbo:APA91bFhFmfgNc4a1mc82ikpCd3CtyCckcfzbFpT-RM46aRQI7U_2wMh5t3-s9Y4dzXAx1iiACfedi4A-9L6IEAQgFBBVdxhhE3qvV1dlWiEmVlvMKRHbM4xQmJZsyq7RBOBlwd-MwvC', 'MR582742', '', '2019-02-05 19:58:29', '2019-02-05 19:58:29'),
(203, '0561716056', '', 'farook mohammed', '862861', 'no', '25.8146584', '55.9822515', 'Al Mataf Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '862861', '', 'fygws9kKwq8:APA91bEFj-MBGGocLFG3qJVEtod21v8pKYfvyeAJtbdQYmNxFaPNd0EIaOkh3YYBY9A9sRtWqao3ycv1eoZQLFtK1bPMk7vOQboApSEa6R6TJ9M-4lPE9ZM3EPzM1TSHcHndcZelIXaz', 'MR902573', '', '2019-02-05 21:06:15', '2019-02-05 21:06:15'),
(204, '0524467560', '', 'farook mohammed', '260977', 'no', '25.8129351', '55.9794323', 'Unnamed Road - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '260977', '', 'fw5FUGDlSss:APA91bGH2PQomBOVo1Vg9hNRCIsyxd1TuUq40cjuQCoDmnsISj5TRjN6kyZV795Gp7QiNQ9Cs4Ih60PiLJoelxQYI2cxmD680B91KKxB7pu_-efTS2OUX661dXfq6v5wDkZZT0S7sF5G', 'MR359733', '', '2019-02-05 21:16:33', '2019-02-05 21:16:33'),
(205, '0508347705', '', 'Al yamuna ', '539672', 'no', '25.6853927', '55.799512', 'Rak Technological Park Freezone Main Road - ????? ??? ?????? - United Arab Emirates', 'technology park', '????? ??? ??????', '14577', 0, '', '539672', '', 'e88hf0-xAGY:APA91bFrb1OAp22DARy6svwYGZD_E6pAdTOu0o9r0F0IrH-aIfqQqeEKke7lvFozTbvK85wl56W_CN1c8myE6zbNw0HTtFEVUocrjfXrW_i70sxMSlKyry0inA3jKZdrQFFafQBVsRKO', 'MR652052', '', '2019-02-07 14:06:17', '2019-02-07 14:06:17'),
(206, '9564897523', '', 'gdjd', '587159', 'yes', '8.5193684', '76.9599185', 'Ali Plaza', 'Thiruvananthapuram', 'Kerala', '695003', 0, '', '587159', '', 'dXdkCKJmPvk:APA91bG_kvAXcqD-Lo8j55TDZmKZCD83VSk4609EW0Vi3mGOfzr8yHL1sWdz_YBiMEgMZfEiSXDt2DEQ7ar4MB-6xiZ2KQqR3i9vCzfV4cTRbjOeT8JMaRHOshAIkvuK9257Kx80ZpJE', 'MR801081', '', '2019-02-13 16:24:34', '2019-02-13 16:24:34'),
(207, '0554172652', '', 'mohammed salim', '313113', 'no', '25.8305461', '55.9917475', 'Al Mataf Rd - Ras al Khaimah - United Arab Emirates', 'Ras Al-Khaimah', 'Ras al Khaimah', '', 0, '', '313113', '', 'dm_ZMJ_LLuk:APA91bG3j92Glyqo5RtmQocRVY2LqW1VxpiIsiEuqorKz3sRxC7vm_INFvnpgyM1N7kH_hPzfP9_SWJul-XDz1cYxaH5yPNUS2VjHndQp9ocZUZ1tItSYd_B59lvJfDEopf8YIRu4DFD', 'MR827078', '', '2019-02-14 13:42:33', '2019-02-14 13:42:33'),
(208, '0555740148', '', '??????? ???????', '632659', 'no', '25.3867951', '55.4537679', '36 Sheikh Jaber Al-Sabah St - Ajman - United Arab Emirates', 'Ajman', '??????', '', 0, '', '632659', '', 'fA1YA0fh2i8:APA91bHW2ZPEYsguaZB4i4esCRTgGU4jAq6apxuKBn8KqWnbQ7_FeTwD69-1uSjqn820sz8i-Gd8OJgNwdvKhAHemczTvJaSxXm_kR_8miZRstzOfjRodKS6We6E5UGyyI-DqwCSvIBh', 'MR863271', '', '2019-02-24 14:31:08', '2019-02-24 14:31:08'),
(209, '0503213572', '', 'hafedh Ghribi ', '159835', 'no', '25.408596675843', '55.510892579331994', 'AL IMAM AL SHAFEE St - Ajman - Émirats arabes unis', 'Ajman', 'Ajman', '', 0, '', '159835', '', 'ekdjwZwhQK8:APA91bFgk4LBkOW742QT311RVQil_4aQB5h7_4J6my5TYBwptinxGDAiY3v-81PebNpyh9xlJIdbaNNVK5v3aKL6QQ2dSpiDpglK2YL2wPQaXS-ohI7ZZyrCw9OIWAI3UQlYqbSrdj4u', 'MR559531', '', '2019-02-24 16:32:29', '2019-02-24 16:32:29'),
(210, '0528056520', '', 'Dangana Mohamed', '514308', 'no', '25.386763', '55.4536981', '36 Sheikh Jaber Al-Sabah St - Ajman - Émirats arabes unis', 'Ajman', '???????? ', '', 0, '', '514308', '', 'dnLrgxergP0:APA91bHDE2EacXt4cx2pWA2e21M1xp6JKan_VOwdCmyQNzTJP8SFxdRJ47usOTY_GXL0QfmnpOUu15VqrsCeg7mmscaEcfD7ZQ0TZTlO8nQS2gZEgmkvG7Qr62kABHpkXi8DfEtZld_P', 'MR454282', '', '2019-02-26 00:19:24', '2019-02-26 00:19:24'),
(211, '0528106805', '', 'Dangana Mohamed', '557100', 'no', '25.3867699', '55.4537344', '36 Sheikh Jaber Al-Sabah St - Ajman - Émirats arabes unis', 'Ajman', '?????? ', '', 0, '', '557100', '', 'dnLrgxergP0:APA91bHDE2EacXt4cx2pWA2e21M1xp6JKan_VOwdCmyQNzTJP8SFxdRJ47usOTY_GXL0QfmnpOUu15VqrsCeg7mmscaEcfD7ZQ0TZTlO8nQS2gZEgmkvG7Qr62kABHpkXi8DfEtZld_P', 'MR899676', '', '2019-02-26 00:26:33', '2019-02-26 00:26:33'),
(212, '0528106805', '', 'Dangana Mohamed', '141155', 'no', '25.3867699', '55.4537344', '36 Sheikh Jaber Al-Sabah St - Ajman - Émirats arabes unis', 'Ajman', '?????? ', '', 0, '', '141155', '', 'dnLrgxergP0:APA91bHDE2EacXt4cx2pWA2e21M1xp6JKan_VOwdCmyQNzTJP8SFxdRJ47usOTY_GXL0QfmnpOUu15VqrsCeg7mmscaEcfD7ZQ0TZTlO8nQS2gZEgmkvG7Qr62kABHpkXi8DfEtZld_P', 'MR440058', '', '2019-02-26 00:26:32', '2019-02-26 00:26:32'),
(213, '0552380768', '', 'rohit', '449640', 'no', '25.2539251', '55.2943972', 'rohit ', 'dubi', 'dubai', '1234', 0, '', '449640', '', 'eLm65QQhdNc:APA91bEkCW6Cz7AeAfylATnEJNSef8950THkKrW8Wgj93gdUP9xSmYLvaGJJc9uLwfIAJI4DytKxIONCnKQSlPm6vRi7oKvO4S4Rt58wdoJb0bSCQcQwcRwtyaCF_ejWaHqjrT_pa9Rd', 'MR590394', '', '2019-03-07 03:44:21', '2019-03-07 03:44:21'),
(214, '0501231231', '', 'Mohd ', '271174', 'yes', '25.5457034', '55.6662798', 'dubai', 'Dubai ', 'uae', '22', 0, '', '271174', '', 'dkVt10vUuZA:APA91bGV8u72Dy_2dINHZmfg0Sv92dxHOFgN2E5DXJIy_8lMdcELAHwe2JeRZycDM2gbHTq1Q1vQxwWj785UvFs49VahDPtO5BB4gkBJK21sVddesC6myfrj9fCTWN3jonwfEPhDJ7SP', 'MR231769', '', '2019-03-09 11:38:49', '2019-03-09 11:38:49'),
(215, '0504844454', '', '????', '551841', 'yes', '25.5457034', '55.6662798', '???', '??????', '????????', '39', 0, '', '551841', '', 'dkVt10vUuZA:APA91bGV8u72Dy_2dINHZmfg0Sv92dxHOFgN2E5DXJIy_8lMdcELAHwe2JeRZycDM2gbHTq1Q1vQxwWj785UvFs49VahDPtO5BB4gkBJK21sVddesC6myfrj9fCTWN3jonwfEPhDJ7SP', 'MR115026', '', '2019-03-09 11:40:11', '2019-03-09 11:40:11'),
(216, '0544462938', '', 'muuna nagaa', '176812', 'no', '25.09786383', '55.18898251', '123445', 'miiftahi', '0000', '', 0, '', '176812', '', 'fDcXSJd7Z2M:APA91bFGcZo8KALxB_K6Skz-1lr-0CmRAa26AbSqlbK2X_K0CKTtH4885qanP2-a3ahu24ti3cseVxhFbm7Qlk1DxhSGev4A51zzx0vt1ZLjX84RyTxOnynuaOK8G7qRJRAAn6DW9syz', 'MR778874', '', '2019-03-18 21:05:16', '2019-03-18 21:05:16'),
(217, '+555531471', '', 'hassan', '341053', 'no', '25.3873444', '55.4534312', 'ajman', 'ajmam', 'ajman', '', 0, '', '341053', '', 'fTci27KFJLA:APA91bF5Jc5a7RxrLHAwT4VcJvIsh049Tk88jCGLCgek68EIaxDewcrd7NSsLjY1KlNFL9zbdSqfS8_vVLA0IcVbt5V6sf-S0UsLwWalPMkvEoIHVNajra8ChDMBjHS3lhDgITJVgN8e', 'MR324040', '', '2019-03-24 20:51:46', '2019-03-24 20:51:46'),
(218, '9746582604', '', 'vivek', '192232', 'no', '8.5603965', '76.8803016', 'Nila Building', 'Technopark Road', 'Thiruvananthapuram', 'India', 0, '', '192232', '', '', 'MR155790', '', '2019-03-25 14:16:36', '2019-03-25 14:16:36'),
(219, '9746582684', '', 'Vivek', '663683', 'no', '8.560364', '76.8803103', 'Nila Building', 'Technopark Road', 'Thiruvananthapuram', 'India', 0, '', '663683', '', '', 'MR386214', '', '2019-03-26 09:40:36', '2019-03-26 09:40:36'),
(220, '8075915526', '', 'Vivi', '197493', 'no', '8.5603638', '76.8803102', 'Nila Building', 'Technopark Road', 'Thiruvananthapuram', 'India', 0, '', '197493', '', '', 'MR700114', '', '2019-03-26 09:41:22', '2019-03-26 09:41:22'),
(221, '8129579141', '', 'suraj krishna', '877201', 'no', '0.0', '0.0', '11115', '11  --5', '5644', '849315', 0, '', '877201', '', '', 'MR986212', '', '2019-03-29 13:59:07', '2019-03-29 13:59:07'),
(222, '9809622148', '', 'Sreejith', '783521', 'no', '0.0', '0.0', '123', '123', 'kerala', '679103', 0, '', '783521', '', '', 'MR664620', '', '2019-04-01 14:58:24', '2019-04-01 14:58:24'),
(223, '12345678900', '', 'test', '340622', 'no', '8.560362566351582', '76.88030164898905', 'Nila Building', 'Kazhakkottam', 'Kerala', '695581', 0, '', '340622', '', '', 'MR700069', '', '2019-04-02 17:17:05', '2019-04-02 17:17:05'),
(224, '0552919108', '', 'muneer', '315758', 'no', '25.790908813476562', '55.94126622346077', 'Defan Rak', 'Defan Rak', 'Ras al-Khaimah', 'null', 0, '', '315758', '', '', 'MR433131', '', '2019-04-13 19:06:39', '2019-04-13 19:06:39'),
(225, '8594087830', '', 'byju', '644115', 'no', '8.333335876464844', '77.0499301516305', 'Poovar Vizhinjam Road', 'Neyyattinkara', 'Kerala', '695526', 0, '', '644115', '', '', 'MR691556', '', '2019-05-01 12:47:49', '2019-05-01 12:47:49'),
(226, '0557694381', '', 'heba', '616013', 'yes', '25.798904418945312', '55.98668769057489', 'Al Hudeeba', 'Al Hudeeba', 'Ras al-Khaimah', 'null', 0, '', '616013', '', '', 'MR355095', '', '2019-05-10 11:54:22', '2019-05-10 11:54:22'),
(227, '+971555270389', '', 'khaled arbi', '867032', 'yes', '25.3988334', '55.4503336', '????????', '??????', 'ajman', '', 0, '', '867032', '', 'cUm5__WoR2M:APA91bHIQ2e8BST5mX7KmAXaKqMMt8YaYH6QP0pkbgsEl6S75TEgji5xjL6tLg3s0oH00LHnw9ophqYErS3qJFEyVsa3eDBQYPmzhvvqWVWbn4wGLk3ewVBOSu16vmh5k2XicBPJflRu', 'MR237195', '', '2019-05-21 06:32:03', '2019-05-21 06:32:03'),
(228, '+971555270389', '', 'khaled arbi', '500550', 'yes', '25.3988334', '55.4503336', '????????', '??????', 'ajman', '', 0, '', '500550', '', 'cUm5__WoR2M:APA91bHIQ2e8BST5mX7KmAXaKqMMt8YaYH6QP0pkbgsEl6S75TEgji5xjL6tLg3s0oH00LHnw9ophqYErS3qJFEyVsa3eDBQYPmzhvvqWVWbn4wGLk3ewVBOSu16vmh5k2XicBPJflRu', 'MR285921', '', '2019-05-21 06:32:05', '2019-05-21 06:32:05'),
(229, '0501911215', '', 'khaseeba', '284104', 'no', '25.143157958984375', '56.3463332497534', 'Al Faisal', 'Al Faisal', 'Fujairah', 'null', 0, '', '284104', '', '', 'MR680533', '', '2019-05-24 16:05:46', '2019-05-24 16:05:46'),
(230, '0503133643', '', 'shaikha salen', '181230', 'no', '25.84844970703125', '56.00100256959292', 'Julfar', 'Julfar', 'Ras al-Khaimah', 'null', 0, '', '181230', '', '', 'MR854699', '', '2019-05-31 00:58:56', '2019-05-31 00:58:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_12_28_171741_create_social_logins_table', 1),
('2015_12_29_015055_setup_access_tables', 1),
('2016_10_14_202046_create_customer_table', 1),
('2016_10_14_205723_add_secondary_phone_in_customer_table', 1),
('2016_10_18_191022_remove_phone2_from_customers', 1),
('2016_10_18_193932_create_customer_phone_table', 1),
('2016_10_28_212521_add_usual_order_amount_in_customer_table', 1),
('2016_11_03_191233_addImageExtensionInCustomer', 1),
('2017_02_05_064205_cretae_employeetype_table', 1),
('2017_02_05_064239_cretae_employees_table', 1),
('2017_02_05_080522_remove_usual_quantity_amount_from_customers_table', 1),
('2017_02_09_172416_create_stock_table', 1),
('2017_02_09_181338_create_categories_table', 1),
('2017_02_11_024647_add_category_in_stock', 1),
('2017_02_11_154830_add_available_quantity_in_stocks', 1),
('2017_02_11_163141_create_out_stocks_table', 1),
('2017_02_11_163153_create_out_stock_details_table', 1),
('2017_02_11_173048_update_out_stocks_table', 1),
('2017_02_12_023559_create_vehicles_table', 1),
('2017_02_12_054846_create_sales_table', 1),
('2017_02_12_055436_create_agents_table', 1),
('2017_02_13_164933_update_sales_table', 1),
('2017_02_14_171816_create_sales_details_table', 1),
('2017_03_18_095136_add_vehicle_to_out_stock', 2),
('2017_03_18_121358_remove_column_employee_from_outstock', 2),
('2017_03_19_175113_add_agent_and_type_to_customers', 2),
('2017_04_10_162930_create_stakeholders_table', 3),
('2017_04_13_075814_add_api_toen_to_users', 3),
('2017_04_14_045023_create_customer_groups_table', 3),
('2017_04_14_045047_create_customer_categories_table', 3),
('2017_04_14_064958_create_prices_table', 3),
('2017_04_14_184858_make_customer_category_and_group_in_customers', 3),
('2017_04_15_060010_add_amount_free_quantity_to_sale_details', 3),
('2017_04_15_062343_add_driver_helper_in_sales', 3),
('2017_04_15_062814_add_driver_commission_helper_commission_in_sales', 3),
('2017_04_15_063259_add_agent_type_in_sales', 3),
('2017_04_27_170650_add_sale_status_insales', 3),
('2017_04_27_191011_add_stakeholder_commission_in_sales_table', 3),
('2017_06_12_182534_update_sales_for_discount_and_voucher', 3),
('2017_07_11_190643_add_return_stock', 3),
('2017_08_04_174546_add_damaged_stock_and_sample_stock', 3),
('2017_08_14_181934_create_expenses_table', 3),
('2017_08_14_184808_create_expensetypes_table', 3),
('2017_08_15_113026_add_ootstockid_to_sales_table', 3),
('2017_08_15_114332_add_sold_to_outstockdetails_table', 3),
('2017_09_05_183640_create_sales_payments_table', 4),
('2017_10_25_183155_create_sale_detail_outstock_details_table', 5),
('2017_11_18_182822_add_payment_collected_and_credit_in_sales', 6),
('2017_11_19_062245_create_credit_payments_table', 6),
('2017_11_19_103340_create_credit_payment_details_table', 6),
('2017_11_19_103747_add_amount_aplit_completed_field_in_credit_payment', 6),
('2017_11_19_105226_add_amount_aplit_completed_field_in_credit_payment_again', 6),
('2017_12_10_071541_add_credi_payment_id_field_to_sale_payments', 7);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(15) NOT NULL,
  `heading` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `arabic_heading` text CHARACTER SET utf8 NOT NULL,
  `arabic_description` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(250) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `product_id` int(15) DEFAULT NULL,
  `quantity` varchar(250) NOT NULL,
  `special_price` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `heading`, `description`, `arabic_heading`, `arabic_description`, `image`, `from_date`, `to_date`, `product_id`, `quantity`, `special_price`) VALUES
(7, 'GRAB YOUR SAMPLE''S', 'Get the chance to experience our quality and service free of cost.', 'الاستيلاء علي العينة الخاصة بك', 'الحصول علي فرصه لتجربه الجودة والخدمة مجانا من التكلفة.', '1548623065OFFR_IMG.jpg', '2019-01-26', '2019-07-15', 15, '1', '-');

-- --------------------------------------------------------

--
-- Table structure for table `offer_order`
--

CREATE TABLE `offer_order` (
  `id` int(15) NOT NULL,
  `order_id` varchar(250) NOT NULL,
  `user_id` int(15) NOT NULL,
  `offer_id` int(15) NOT NULL,
  `delivered_status` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `order_place` varchar(250) NOT NULL,
  `latitude` varchar(250) NOT NULL,
  `longitude` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offer_order`
--

INSERT INTO `offer_order` (`id`, `order_id`, `user_id`, `offer_id`, `delivered_status`, `created_at`, `order_place`, `latitude`, `longitude`) VALUES
(1, '100001', 22, 1, 'Ordered', '2018-08-21 15:09:35', 'Tvm', '1.023', '1.823'),
(2, '100001', 22, 1, 'Ordered', '2018-08-21 15:13:38', 'Tvm', '1.023', '1.823'),
(3, '100001', 22, 1, 'Ordered', '2018-08-21 15:27:06', 'Tvm', '1.023', '1.823'),
(4, '100002', 40, 1, 'Ordered', '2018-08-23 11:57:08', 'Tvm', '0.00', '0.00'),
(5, '100003', 40, 2, 'Ordered', '2018-08-23 11:57:16', 'Tvm', '0.00', '0.00'),
(6, '100004', 40, 2, 'Ordered', '2018-08-23 11:57:17', 'Tvm', '0.00', '0.00'),
(7, '100005', 40, 2, 'Ordered', '2018-08-23 11:57:19', 'Tvm', '0.00', '0.00'),
(8, '100006', 40, 2, 'Ordered', '2018-08-23 11:57:20', 'Tvm', '0.00', '0.00'),
(9, '100007', 41, 1, 'Ordered', '2018-08-23 12:12:47', 'gghfh', '0.000', '0.000'),
(10, '100008', 41, 1, 'Ordered', '2018-08-23 12:39:34', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(11, '100009', 41, 1, 'Ordered', '2018-08-23 12:39:34', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(12, '100010', 41, 1, 'Ordered', '2018-08-23 12:39:54', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(13, '100011', 41, 2, 'Ordered', '2018-08-23 12:40:24', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(14, '100012', 41, 2, 'Ordered', '2018-08-23 12:40:24', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(15, '100013', 41, 2, 'Ordered', '2018-08-23 12:40:27', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(16, '100014', 41, 2, 'Ordered', '2018-08-23 12:40:27', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(17, '100015', 41, 2, 'Ordered', '2018-08-23 12:40:28', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(18, '100016', 41, 2, 'Ordered', '2018-08-23 12:40:28', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(19, '100017', 41, 2, 'Ordered', '2018-08-23 12:40:28', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(20, '100018', 41, 2, 'Ordered', '2018-08-23 12:40:28', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(21, '100019', 41, 2, 'Ordered', '2018-08-23 12:40:28', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(22, '100020', 41, 2, 'Ordered', '2018-08-23 12:40:28', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(23, '100021', 41, 2, 'Ordered', '2018-08-23 12:41:29', 'Thiruvananthapuram', '8.5193527', '76.9599628'),
(24, '100022', 41, 2, 'Ordered', '2018-08-23 12:43:04', 'Thiruvananthapuram', '8.5193527', '76.959963'),
(25, '100023', 41, 1, 'Ordered', '2018-08-23 12:43:17', 'Thiruvananthapuram', '8.5193527', '76.9599627'),
(26, '100024', 41, 1, 'Ordered', '2018-08-23 12:45:52', 'Thiruvananthapuram', '8.5193527', '76.9599628'),
(27, '100025', 41, 2, 'Ordered', '2018-08-23 12:46:09', 'Thiruvananthapuram', '8.5193527', '76.9599627'),
(28, '100026', 41, 2, 'Ordered', '2018-08-23 13:37:45', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(29, '100009', 41, 1, 'Ordered', '2018-08-23 14:22:43', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(30, '100027', 41, 2, 'Ordered', '2018-08-23 14:32:46', 'Thiruvananthapuram', '8.5193526', '76.959963'),
(31, '100007', 41, 1, 'Ordered', '2018-08-23 14:33:27', 'gghfh', '0.000', '0.000'),
(32, '100028', 41, 1, 'Ordered', '2018-08-23 14:36:42', 'Thiruvananthapuram', '8.5193527', '76.9599627'),
(33, '100029', 41, 2, 'Ordered', '2018-08-23 15:18:25', 'Thiruvananthapuram', '8.5193526', '76.9599627'),
(34, '100030', 41, 2, 'Ordered', '2018-08-23 15:18:30', 'Thiruvananthapuram', '8.5193675', '76.95998'),
(35, '100031', 26, 2, 'Ordered', '2018-08-23 15:26:15', 'Thiruvananthapuram', '8.4954669', '76.9326933'),
(36, '100032', 41, 1, 'Ordered', '2018-08-23 15:26:22', 'Thiruvananthapuram', '8.5193511', '76.9599612'),
(37, '100033', 41, 2, 'Ordered', '2018-08-23 15:46:00', '', '0.0', '0.0'),
(38, '100034', 41, 2, 'Ordered', '2018-08-23 15:46:48', '', '0.0', '0.0'),
(39, '100007', 41, 1, 'Ordered', '2018-08-27 13:56:17', 'gghfh', '0.000', '0.000'),
(40, '100008', 41, 1, 'Ordered', '2018-08-27 15:23:56', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(41, '100035', 41, 2, 'Ordered', '2018-08-27 15:24:22', '', '0.0', '0.0'),
(42, '100009', 41, 1, 'Ordered', '2018-08-27 15:24:28', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(43, '100036', 41, 2, 'Ordered', '2018-08-27 15:57:44', '', '0.0', '0.0'),
(44, '100009', 41, 1, 'Ordered', '2018-08-27 15:57:50', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(45, '100037', 41, 1, 'Ordered', '2018-08-27 17:36:24', 'Thiruvananthapuram', '8.5604641', '76.8802838'),
(46, '100038', 41, 1, 'Ordered', '2018-08-27 20:20:23', 'Thiruvananthapuram', '8.5448741', '76.9441831'),
(47, '100039', 41, 1, 'Ordered', '2018-08-27 20:20:33', 'Thiruvananthapuram', '8.5448741', '76.9441831'),
(48, '100040', 41, 1, 'Ordered', '2018-08-28 12:07:22', '', '0.0', '0.0'),
(49, '100041', 41, 1, 'Ordered', '2018-08-28 12:07:22', '', '0.0', '0.0'),
(50, '100042', 41, 1, 'Ordered', '2018-08-28 12:07:46', 'Thiruvananthapuram', '8.5193606', '76.9599718'),
(51, '100043', 41, 2, 'Ordered', '2018-08-28 12:07:48', 'Thiruvananthapuram', '8.5193606', '76.9599718'),
(52, '100008', 41, 1, 'Ordered', '2018-08-28 12:13:24', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(53, '100044', 41, 1, 'Ordered', '2018-08-28 14:41:00', 'Thiruvananthapuram', '8.5193606', '76.9599713'),
(54, '100045', 41, 2, 'Ordered', '2018-08-28 14:41:54', 'Thiruvananthapuram', '8.5193674', '76.9599642'),
(55, '100046', 41, 2, 'Ordered', '2018-08-28 14:45:21', 'Thiruvananthapuram', '8.5193599', '76.9599712'),
(56, '100047', 41, 1, 'Ordered', '2018-08-28 14:51:18', 'Thiruvananthapuram', '8.5193606', '76.9599719'),
(57, '100048', 41, 1, 'Ordered', '2018-08-29 12:27:46', 'Thiruvananthapuram', '8.5517412', '76.8772087'),
(58, '100049', 41, 2, 'Ordered', '2018-08-29 12:27:53', 'Thiruvananthapuram', '8.5517412', '76.8772087'),
(59, '100050', 26, 2, 'Ordered', '2018-08-29 12:27:57', 'Thiruvananthapuram', '8.5517444', '76.8771944'),
(60, '100051', 41, 2, 'Ordered', '2018-08-31 13:19:34', 'Thiruvananthapuram', '8.5193682', '76.9599839'),
(61, '100052', 41, 2, 'Ordered', '2018-08-31 13:19:34', 'Thiruvananthapuram', '8.5193605', '76.9599724'),
(62, '100007', 41, 1, 'Ordered', '2018-08-31 13:47:52', 'gghfh', '0.000', '0.000'),
(63, '100053', 62, 1, 'Ordered', '2018-09-03 12:06:04', 'Thiruvananthapuram', '8.519931', '76.9576447'),
(64, '100054', 41, 1, 'Ordered', '2018-09-03 16:13:21', 'Thiruvananthapuram', '8.519931', '76.9576447'),
(65, '100055', 41, 2, 'Ordered', '2018-09-03 16:13:23', 'Thiruvananthapuram', '8.519931', '76.9576447'),
(66, '100056', 26, 2, 'Ordered', '2018-09-11 23:53:24', 'Thiruvananthapuram', '8.5700529', '76.9719261'),
(67, '100057', 26, 2, 'Ordered', '2018-09-11 23:53:24', 'Thiruvananthapuram', '8.5700529', '76.9719261'),
(68, '100058', 41, 2, 'Ordered', '2018-09-12 10:23:17', 'Thiruvananthapuram', '8.5193593', '76.9599708'),
(69, '100059', 41, 2, 'Ordered', '2018-09-12 10:30:39', 'Thiruvananthapuram', '8.5193592', '76.9599709'),
(70, '100060', 41, 2, 'Ordered', '2018-09-12 10:30:42', 'Thiruvananthapuram', '8.5193592', '76.9599709'),
(71, '100010', 41, 1, 'Ordered', '2018-09-12 10:31:32', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(72, '100010', 41, 1, 'Ordered', '2018-09-13 12:16:41', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(73, '100061', 41, 2, 'Ordered', '2018-09-13 12:33:01', 'Thiruvananthapuram', '8.5193595', '76.9599709'),
(74, '100009', 41, 1, 'Ordered', '2018-09-17 16:53:41', 'Thiruvananthapuram', '8.5193525', '76.9599632'),
(75, '100062', 26, 2, 'Ordered', '2018-09-18 10:07:29', 'Thiruvananthapuram', '8.5700529', '76.9719261'),
(76, '100063', 26, 2, 'Ordered', '2018-09-18 12:40:16', 'Thiruvananthapuram', '8.4951714', '76.9321834'),
(77, '100064', 41, 2, 'Ordered', '2018-09-20 17:34:05', 'Thiruvananthapuram', '8.5193584', '76.9599723'),
(78, '100065', 41, 1, 'Ordered', '2018-09-21 11:38:12', 'Thiruvananthapuram', '8.5193581', '76.959972'),
(79, '100066', 41, 2, 'Ordered', '2018-09-21 11:38:20', 'Thiruvananthapuram', '8.5193581', '76.959972'),
(80, '100026', 41, 2, 'Ordered', '2018-09-21 11:50:18', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(81, '100067', 41, 1, 'Ordered', '2018-09-28 16:06:15', 'Thiruvananthapuram', '8.5193408', '76.9600172'),
(82, '100008', 41, 1, 'Ordered', '2018-11-26 16:29:28', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(83, '100008', 41, 1, 'Ordered', '2018-11-26 16:29:31', 'Thiruvananthapuram', '8.5193527', '76.9599626'),
(84, '100032', 41, 1, 'Ordered', '2018-11-27 14:17:26', 'Thiruvananthapuram', '8.5193511', '76.9599612'),
(85, '100068', 160, 1, 'Ordered', '2019-01-03 12:27:35', 'Thiruvananthapuram', '8.5700317', '76.9719205'),
(86, '100069', 170, 1, 'Ordered', '2019-01-03 15:24:17', 'Ras Al-Khaimah', '25.7896894', '55.9416008'),
(87, '100070', 170, 1, 'Ordered', '2019-01-03 15:24:45', 'Ras Al-Khaimah', '25.7896894', '55.9416008'),
(88, '100071', 170, 1, 'Ordered', '2019-01-03 15:30:14', 'Ras Al-Khaimah', '25.7896894', '55.9416008'),
(89, '100072', 170, 6, 'Ordered', '2019-01-03 16:17:19', 'Ras Al-Khaimah', '25.7907553', '55.9419488'),
(90, '100073', 170, 6, 'Delivered', '2019-01-03 16:17:52', 'Ras Al-Khaimah', '25.7907553', '55.9419488'),
(91, '100074', 170, 6, 'Cancelled', '2019-01-03 17:14:09', 'Ras Al-Khaimah', '25.7898657', '55.9471698'),
(92, '100075', 170, 1, 'Delivered', '2019-01-03 17:40:43', 'Ras Al-Khaimah', '25.7867253', '55.9659679'),
(93, '100076', 170, 1, 'Delivered', '2019-01-03 17:40:43', 'Ras Al-Khaimah', '25.7867253', '55.9659679'),
(94, '100077', 170, 1, 'Delivered', '2019-01-03 17:40:43', 'Ras Al-Khaimah', '25.7867253', '55.9659679'),
(95, '100078', 170, 6, 'Delivered', '2019-01-03 23:12:20', 'Thiruvananthapuram', '8.5393925', '76.9454739'),
(96, '100079', 170, 1, 'Delivered', '2019-01-03 23:45:52', 'Thiruvananthapuram', '8.5393925', '76.9454739'),
(97, '100080', 153, 1, 'Delivered', '2019-01-04 10:09:13', 'Thiruvananthapuram', '8.5193505', '76.9599158'),
(98, '100081', 153, 1, 'Delivered', '2019-01-04 12:09:14', 'sfgsb,Ali Plaza,Thiruvananthapuram,Kerala', '8.5193504', '76.959916'),
(99, '100082', 153, 1, 'Delivered', '2019-01-04 12:09:19', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', '8.5193504', '76.959916'),
(100, '100083', 153, 1, 'Delivered', '2019-01-04 15:46:46', 'test,Ali Plaza,Thiruvananthapuram,Kerala', '8.5193659', '76.9599174'),
(101, '100084', 153, 6, 'Delivered', '2019-01-04 15:47:45', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', '8.5193664', '76.9599175'),
(102, '100085', 165, 6, 'Delivered', '2019-01-05 11:02:43', 'ghhj\nhshs,Al Manar Mall - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.7832879', '55.96574'),
(103, '100086', 165, 6, 'Delivered', '2019-01-05 11:09:59', 'nijam,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.7777226', '55.9687531'),
(104, '100087', 170, 1, 'Delivered', '2019-01-07 17:23:09', 'mohan,Nila Building,Thiruvananthapuram,Kerala', '8.5605282', '76.8803573'),
(105, '100088', 153, 1, 'Delivered', '2019-01-23 16:43:28', 'tru', '0.0', '0.0'),
(106, '100089', 180, 7, 'Delivered', '2019-01-26 14:54:06', 'RN\nrm,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.7908033', '55.9409047'),
(107, '100090', 180, 7, 'Delivered', '2019-01-26 14:55:07', 'Ajmal,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.7908033', '55.9409047'),
(108, '100091', 199, 7, 'Delivered', '2019-01-31 13:32:04', 'aboobakker koya,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.845008906018396', '56.004885424302216'),
(109, '100092', 199, 7, 'Delivered', '2019-01-31 13:32:36', 'aboobakker koya,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.845042696654904', '56.00473184135505'),
(110, '100093', 176, 7, 'Ordered', '2019-02-01 10:26:01', 'Thiruvananthapuram', '8.4954366', '76.9326615'),
(111, '100094', 24, 7, 'Ordered', '2019-02-01 11:09:03', 'Tvm', '1.023', '1.823'),
(112, '100095', 153, 7, 'Ordered', '2019-02-01 11:22:41', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', '8.5193676', '76.9599185'),
(113, '100096', 153, 7, 'Delivered', '2019-02-04 16:33:28', 'tru', '0.0', '0.0'),
(114, '100097', 200, 7, 'Delivered', '2019-02-05 19:29:30', 'Aliton das,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.8395772', '55.9991481'),
(115, '100098', 201, 7, 'Delivered', '2019-02-05 19:45:11', 'suman kumar,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.8401922', '55.9995184'),
(116, '100099', 201, 7, 'Delivered', '2019-02-05 19:45:20', 'suman kumar,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.8401922', '55.9995184'),
(117, '100100', 202, 7, 'Delivered', '2019-02-05 19:59:08', 'Mohammed Abdur Razzak,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.83994', '55.9993753'),
(118, '100101', 204, 7, 'Delivered', '2019-02-05 21:21:37', 'farook mohammed,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.8153609', '55.9826286'),
(119, '100102', 153, 7, 'Delivered', '2019-02-07 14:51:46', 'tru', '0.0', '0.0'),
(120, '100103', 207, 7, 'Ordered', '2019-02-14 13:43:05', 'mohammed salim,Al Mataf Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', '25.8305811', '55.9919835'),
(121, '100104', 153, 7, 'Ordered', '2019-02-14 15:19:51', 'tru', '0.0', '0.0'),
(122, '100105', 217, 7, 'Ordered', '2019-03-24 20:53:39', 'hassan,ajman,ajmam,ajman', '25.3869596', '55.4533872'),
(123, '100106', 179, 7, 'Ordered', '2019-04-13 18:53:43', 'tru', '0.0', '0.0'),
(124, '100107', 179, 7, 'Ordered', '2019-04-13 18:53:44', 'tru', '0.0', '0.0'),
(125, '100108', 179, 7, 'Ordered', '2019-04-13 18:53:46', 'tru', '0.0', '0.0'),
(126, '100109', 179, 7, 'Ordered', '2019-04-13 18:53:47', 'tru', '0.0', '0.0'),
(127, '100110', 179, 7, 'Ordered', '2019-04-13 18:54:13', 'tru', '0.0', '0.0'),
(128, '100111', 179, 7, 'Ordered', '2019-04-13 18:54:33', 'tru', '0.0', '0.0'),
(129, '100112', 179, 7, 'Ordered', '2019-04-13 18:54:36', 'tru', '0.0', '0.0'),
(130, '100113', 179, 7, 'Ordered', '2019-04-13 18:54:37', 'tru', '0.0', '0.0'),
(131, '100114', 179, 7, 'Ordered', '2019-04-13 18:54:37', 'tru', '0.0', '0.0'),
(132, '100115', 179, 7, 'Ordered', '2019-04-13 18:54:37', 'tru', '0.0', '0.0'),
(133, '100116', 179, 7, 'Ordered', '2019-04-13 18:54:38', 'tru', '0.0', '0.0'),
(134, '100117', 179, 7, 'Ordered', '2019-04-13 18:54:38', 'tru', '0.0', '0.0'),
(135, '100118', 179, 7, 'Ordered', '2019-04-13 18:54:38', 'tru', '0.0', '0.0'),
(136, '100119', 179, 7, 'Ordered', '2019-04-13 18:54:39', 'tru', '0.0', '0.0'),
(137, '100120', 179, 7, 'Ordered', '2019-04-13 18:54:39', 'tru', '0.0', '0.0'),
(138, '100121', 179, 7, 'Ordered', '2019-04-13 18:54:39', 'tru', '0.0', '0.0'),
(139, '100122', 153, 7, 'Ordered', '2019-04-17 11:27:40', 'tru', '0.0', '0.0'),
(140, '100123', 153, 7, 'Ordered', '2019-05-02 11:28:56', 'tru', '0.0', '0.0'),
(141, '100124', 153, 7, 'Ordered', '2019-06-04 12:17:13', 'tru', '0.0', '0.0'),
(142, '100125', 153, 7, 'Ordered', '2019-06-04 12:17:15', 'tru', '0.0', '0.0');

-- --------------------------------------------------------

--
-- Table structure for table `out_stocks`
--

CREATE TABLE `out_stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `out_stocks`
--

INSERT INTO `out_stocks` (`id`, `created_at`, `updated_at`, `deleted_at`, `category_id`, `quantity`, `vehicle_id`) VALUES
(1, '2017-03-10 11:44:13', '2017-03-10 11:44:13', NULL, 1, 1400, 0),
(2, '2017-03-10 13:47:47', '2017-03-10 13:47:47', NULL, 1, 225, 0),
(3, '2017-04-14 19:03:34', '2017-04-14 19:03:34', NULL, 2, 190, 1),
(4, '2017-04-14 19:03:58', '2017-04-14 19:03:58', NULL, 2, 193, 2),
(5, '2017-04-14 19:04:30', '2017-04-14 19:04:30', NULL, 2, 30, 3),
(6, '2017-04-15 18:48:18', '2017-04-15 18:48:18', NULL, 2, 130, 1),
(7, '2017-04-16 19:27:45', '2017-04-16 19:27:45', NULL, 2, 55, 2),
(8, '2017-04-17 18:01:52', '2017-04-17 18:01:52', NULL, 2, 68, 2),
(9, '2017-04-21 19:10:07', '2017-04-21 19:10:07', NULL, 2, 41, 1),
(10, '2017-04-21 19:10:49', '2017-04-21 19:10:49', NULL, 2, 55, 2),
(11, '2017-04-24 18:24:18', '2017-04-24 18:24:18', NULL, 1, 66, 1),
(12, '2017-04-24 18:24:47', '2017-04-24 18:24:47', NULL, 1, 132, 2),
(13, '2017-04-24 18:26:33', '2017-04-24 18:26:33', NULL, 3, 80, 1),
(14, '2017-04-24 18:26:58', '2017-04-24 18:26:58', NULL, 3, 84, 2),
(15, '2017-04-26 05:37:08', '2017-04-26 05:37:08', NULL, 1, 22, 1),
(16, '2017-04-26 05:38:09', '2017-04-26 05:38:09', NULL, 2, 192, 1),
(17, '2017-04-26 05:38:46', '2017-04-26 05:38:46', NULL, 3, 80, 1),
(18, '2017-04-26 05:39:54', '2017-04-26 05:39:54', NULL, 1, 66, 2),
(19, '2017-04-26 05:40:25', '2017-04-26 05:40:25', NULL, 2, 96, 2),
(20, '2017-04-28 18:59:44', '2017-04-28 18:59:44', NULL, 2, 384, 2),
(21, '2017-04-28 19:03:35', '2017-04-28 19:03:35', NULL, 1, 132, 1),
(22, '2017-04-28 19:04:00', '2017-04-28 19:04:00', NULL, 1, 44, 2),
(23, '2017-04-28 19:04:42', '2017-04-28 19:04:42', NULL, 3, 20, 2),
(24, '2017-04-30 19:22:02', '2017-04-30 19:22:02', NULL, 1, 66, 1),
(25, '2017-04-30 19:22:57', '2017-04-30 19:22:57', NULL, 1, 22, 2),
(26, '2017-04-30 19:25:53', '2017-04-30 19:25:53', NULL, 2, 36, 2),
(27, '2017-04-30 19:27:11', '2017-04-30 19:27:11', NULL, 2, 127, 1),
(28, '2017-04-30 19:29:13', '2017-04-30 19:29:13', NULL, 3, 40, 1),
(29, '2017-05-10 14:46:16', '2017-05-10 14:46:16', NULL, 2, 435, 1),
(30, '2017-05-10 14:49:28', '2017-05-10 14:49:28', NULL, 2, 184, 2),
(31, '2017-09-07 15:44:37', '2017-09-07 15:44:37', NULL, 1, 10, 2),
(32, '2017-09-07 15:45:29', '2017-09-07 15:45:29', NULL, 1, 15, 1),
(33, '2017-09-12 19:44:12', '2017-09-12 19:44:12', NULL, 2, 50, 1),
(34, '2017-10-11 15:42:54', '2017-10-11 15:42:54', NULL, 2, 26, 1),
(35, '2017-10-22 04:32:14', '2017-10-22 04:32:14', NULL, 1, 120, 1),
(36, '2017-11-05 08:43:22', '2017-11-05 08:43:22', NULL, 1, 25, 1),
(37, '2017-11-05 08:45:13', '2017-11-05 08:45:13', NULL, 1, 13, 1),
(38, '2017-11-05 09:04:51', '2017-11-05 09:04:51', NULL, 1, 30, 1),
(39, '2017-11-05 09:14:23', '2017-11-05 09:14:23', NULL, 1, 20, 1),
(40, '2017-11-05 09:20:17', '2017-11-05 09:20:17', NULL, 1, 26, 1),
(41, '2017-11-05 09:21:39', '2017-11-05 09:21:39', NULL, 1, 26, 1);

-- --------------------------------------------------------

--
-- Table structure for table `out_stock_details`
--

CREATE TABLE `out_stock_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `out_stock_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sold_quantity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `out_stock_details`
--

INSERT INTO `out_stock_details` (`id`, `out_stock_id`, `stock_id`, `quantity`, `sold_quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 6, 1000, 0, '2017-03-10 11:44:13', '2017-03-10 11:44:13', NULL),
(2, 1, 4, 400, 0, '2017-03-10 11:44:13', '2017-03-10 11:44:13', NULL),
(3, 2, 8, 225, 0, '2017-03-10 13:47:47', '2017-03-10 13:47:47', NULL),
(4, 3, 9, 190, 0, '2017-04-14 19:03:34', '2017-04-14 19:03:34', NULL),
(5, 4, 9, 193, 0, '2017-04-14 19:03:58', '2017-04-14 19:03:58', NULL),
(6, 5, 9, 30, 0, '2017-04-14 19:04:30', '2017-04-14 19:04:30', NULL),
(7, 6, 9, 130, 0, '2017-04-15 18:48:18', '2017-04-15 18:48:18', NULL),
(8, 7, 9, 55, 0, '2017-04-16 19:27:45', '2017-04-16 19:27:45', NULL),
(9, 8, 9, 68, 0, '2017-04-17 18:01:52', '2017-04-17 18:01:52', NULL),
(10, 9, 9, 41, 0, '2017-04-21 19:10:07', '2017-04-21 19:10:07', NULL),
(11, 10, 9, 55, 0, '2017-04-21 19:10:49', '2017-04-21 19:10:49', NULL),
(12, 11, 11, 66, 0, '2017-04-24 18:24:18', '2017-04-24 18:24:18', NULL),
(13, 12, 11, 132, 0, '2017-04-24 18:24:47', '2017-04-24 18:24:47', NULL),
(14, 13, 10, 80, 0, '2017-04-24 18:26:33', '2017-04-24 18:26:33', NULL),
(15, 14, 10, 84, 0, '2017-04-24 18:26:58', '2017-04-24 18:26:58', NULL),
(16, 15, 11, 22, 0, '2017-04-26 05:37:08', '2017-04-26 05:37:08', NULL),
(17, 16, 9, 192, 0, '2017-04-26 05:38:09', '2017-04-26 05:38:09', NULL),
(18, 17, 10, 80, 0, '2017-04-26 05:38:46', '2017-04-26 05:38:46', NULL),
(19, 18, 11, 66, 0, '2017-04-26 05:39:54', '2017-04-26 05:39:54', NULL),
(20, 19, 9, 96, 0, '2017-04-26 05:40:25', '2017-04-26 05:40:25', NULL),
(21, 20, 9, 384, 0, '2017-04-28 18:59:44', '2017-04-28 18:59:44', NULL),
(22, 21, 11, 132, 0, '2017-04-28 19:03:35', '2017-04-28 19:03:35', NULL),
(23, 22, 11, 44, 0, '2017-04-28 19:04:00', '2017-04-28 19:04:00', NULL),
(24, 23, 10, 20, 0, '2017-04-28 19:04:42', '2017-04-28 19:04:42', NULL),
(25, 24, 11, 66, 0, '2017-04-30 19:22:02', '2017-04-30 19:22:02', NULL),
(26, 25, 11, 22, 0, '2017-04-30 19:22:57', '2017-04-30 19:22:57', NULL),
(27, 26, 9, 36, 0, '2017-04-30 19:25:53', '2017-04-30 19:25:53', NULL),
(28, 27, 9, 127, 0, '2017-04-30 19:27:11', '2017-04-30 19:27:11', NULL),
(29, 28, 10, 40, 0, '2017-04-30 19:29:13', '2017-04-30 19:29:13', NULL),
(30, 29, 9, 435, 0, '2017-05-10 14:46:16', '2017-05-10 14:46:16', NULL),
(31, 30, 9, 184, 0, '2017-05-10 14:49:28', '2017-05-10 14:49:28', NULL),
(32, 31, 17, 10, 0, '2017-09-07 15:44:37', '2017-09-07 15:44:37', NULL),
(33, 32, 17, 15, 0, '2017-09-07 15:45:29', '2017-09-07 15:45:29', NULL),
(34, 33, 18, 50, 0, '2017-09-12 19:44:12', '2017-09-12 19:44:12', NULL),
(35, 34, 21, 26, 0, '2017-10-11 15:42:54', '2017-10-11 15:42:54', NULL),
(36, 35, 24, 120, 0, '2017-10-22 04:32:14', '2017-10-22 04:32:14', NULL),
(37, 36, 28, 25, 0, '2017-11-05 08:43:22', '2017-11-05 08:43:22', NULL),
(38, 37, 26, 13, 0, '2017-11-05 08:45:13', '2017-11-05 08:45:13', NULL),
(39, 38, 28, 30, 0, '2017-11-05 09:04:51', '2017-11-05 09:04:51', NULL),
(40, 39, 26, 20, 0, '2017-11-05 09:14:23', '2017-11-05 09:14:23', NULL),
(41, 40, 29, 26, 0, '2017-11-05 09:20:17', '2017-11-05 09:20:17', NULL),
(42, 41, 29, 26, 0, '2017-11-05 09:21:39', '2017-11-05 09:21:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `system` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `group_id`, `name`, `display_name`, `system`, `sort`, `created_at`, `updated_at`) VALUES
(1, 1, 'view-backend', 'View Backend', 1, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(2, 1, 'view-access-management', 'View Access Management', 1, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(3, 2, 'create-users', 'Create Users', 1, 5, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(4, 2, 'edit-users', 'Edit Users', 1, 6, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(5, 2, 'delete-users', 'Delete Users', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(6, 2, 'change-user-password', 'Change User Password', 1, 8, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(7, 2, 'deactivate-users', 'Deactivate Users', 1, 9, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(8, 2, 'reactivate-users', 'Re-Activate Users', 1, 11, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(9, 2, 'undelete-users', 'Restore Users', 1, 13, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(10, 2, 'permanently-delete-users', 'Permanently Delete Users', 1, 14, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(11, 2, 'resend-user-confirmation-email', 'Resend Confirmation E-mail', 1, 15, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(12, 3, 'create-roles', 'Create Roles', 1, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(13, 3, 'edit-roles', 'Edit Roles', 1, 3, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(14, 3, 'delete-roles', 'Delete Roles', 1, 4, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(15, 4, 'create-permission-groups', 'Create Permission Groups', 1, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(16, 4, 'edit-permission-groups', 'Edit Permission Groups', 1, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(17, 4, 'delete-permission-groups', 'Delete Permission Groups', 1, 3, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(18, 4, 'sort-permission-groups', 'Sort Permission Groups', 1, 4, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(19, 4, 'create-permissions', 'Create Permissions', 1, 5, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(20, 4, 'edit-permissions', 'Edit Permissions', 1, 6, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(21, 4, 'delete-permissions', 'Delete Permissions', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(22, 4, 'manage-order', 'Manage Order', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(23, 4, 'manage-customer', 'Manage Customer', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(24, 4, 'manage-employee', 'Manage Employee', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(25, 4, 'manage-remuneration', 'Manage Remuneration', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(26, 4, 'manage-infrastructure', 'Manage Infrastructure', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(27, 4, 'manage-expense', 'Manage Expense', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(28, 4, 'manage-report', 'Manage Report', 1, 7, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(29, 4, 'add-sales', 'Add Sale', 0, 0, '2017-11-07 18:20:44', '2017-11-07 18:20:44'),
(30, 4, 'list-sales', 'List Sales', 0, 0, '2017-11-07 18:21:05', '2017-11-07 18:21:05');

-- --------------------------------------------------------

--
-- Table structure for table `permission_dependencies`
--

CREATE TABLE `permission_dependencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `dependency_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_dependencies`
--

INSERT INTO `permission_dependencies` (`id`, `permission_id`, `dependency_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(2, 3, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(3, 3, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(4, 4, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(5, 4, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(6, 5, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(7, 5, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(8, 6, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(9, 6, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(10, 7, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(11, 7, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(12, 8, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(13, 8, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(14, 9, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(15, 9, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(16, 10, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(17, 10, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(18, 11, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(19, 11, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(20, 12, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(21, 12, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(22, 13, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(23, 13, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(24, 14, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(25, 14, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(26, 15, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(27, 15, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(28, 16, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(29, 16, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(30, 17, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(31, 17, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(32, 18, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(33, 18, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(34, 19, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(35, 19, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(36, 20, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(37, 20, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(38, 21, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(39, 21, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(40, 22, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(41, 22, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(42, 23, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(43, 23, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(44, 24, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(45, 24, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(46, 25, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(47, 25, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(48, 26, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(49, 26, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(50, 27, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(51, 27, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(52, 28, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(53, 28, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45');

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_groups`
--

INSERT INTO `permission_groups` (`id`, `parent_id`, `name`, `sort`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Access', 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(2, 1, 'User', 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(3, 1, 'Role', 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(4, 1, 'Permission', 3, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(5, 1, 'Employees', 4, '2017-11-07 18:15:59', '2017-11-07 18:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 3),
(2, 29, 3),
(3, 30, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `customer_category` int(11) NOT NULL,
  `customer_group` int(11) NOT NULL,
  `no_of_cartons` int(11) NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `no_of_free_cartons` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(15) NOT NULL,
  `product_name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `arabic_product_name` text CHARACTER SET utf8 NOT NULL,
  `arabic_description` text CHARACTER SET utf8 NOT NULL,
  `price` varchar(250) NOT NULL,
  `special_price` varchar(250) NOT NULL,
  `discount_price` varchar(250) NOT NULL,
  `images` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_name`, `description`, `arabic_product_name`, `arabic_description`, `price`, `special_price`, `discount_price`, `images`, `created_at`) VALUES
(5, '200 ml (cup)', 'This conveniently sealed cups are perfect for meetings and occasions. It is ideal for charitable distribution in mosques throughout the year and best for distribution in schools and institutions.', '200 مللي (كوب)', 'هذه الأكواب المختومة مريحه مثاليه للاجتماعات والمناسبات. وهي مثاليه للتوزيع الخيري في المساجد علي مدار العام وأفضل للتوزيع في المدارس والمؤسسات.', '7', '-', '-', '7046061_thumb.jpeg', '2019-01-26 13:30:23'),
(7, '200 ml', 'This snug little container is perfect in lunch boxes. It encourages a healthy alternative for your kids.\r\nThis is also preferred in Govt. Offices and private companies to provide their customers a refreshing feel as a part of customer service.', '200 مللي', 'يعد ذلك الوعاء الصغير المحكم مثالياً للاستخدام في صناديق الغذاء. كما يشجع الأطفال على استخدامه كمشروب بديل صحي .\r\nوتعد هذه العبوة أيضاً من العبوات المفضل استخدامها في المكاتب الحكومية وشركات القطاع الخاص ، حيث يتم تقديمها إلى العملاء لإضفاء الشعور بالنشاط لهم كجزء من عملية خدمة\r\n', '11.25', '', '', '4067005_thumb.jpeg', '2019-04-23 00:33:12'),
(8, '330 ml', '24Bottles\r\nThis is the most preferred one in the conference rooms. place them around the conference table, and better decisions will be made.', '330 مللي', 'يمثل ذلك الحجم الأمثل للاستخدام في غرف الاجتماعات ، حضع هذه الزجاجات حول طاولة الاجتماعات ، وسيتم اتخاذ أفضل القرارات', '12', '-', '-', '6570379_thumb.jpeg', '2019-01-27 03:12:37'),
(13, '500 ml', '(500ml X 24)\r\nOur half litre is the most preferred bottle size, and provides more water with less waste. Make family hydration when you go fishing, to the playground, or on that long car ride .', '500 مللي', 'تعد زجاجاتنا النصف لتر هي الحجم الأكثر تفضيلاً على الإطلاق ، حيث تقوم بتوفير مياه أكثر في مقابل مخلفات أقل . \r\nأروي عائلتك عندما تذهب للصيد ، أو للملعب ، أو عندما تقوم برحلة طويلة بالسيارة .\r\n', '11.50', '-', '-', '5427157_thumb.jpeg', '2019-04-23 00:29:50'),
(14, '1500 ml', '(1500ml X 12)\r\nHere''s a longer-lasting source of great tasting water to take along wherever you go. Whether you slam it or sip it, its always refreshingly good stuff.', '1500 مللي', 'وهنا مصدر أطول أمدا من المياه تذوق كبيره لتاخذ علي طول أينما ذهبت. سواء كنت الضربة القاضية أو رشفه ، ولها دائما الأشياء الجيدة منعش.', '11.50', '-', '-', '5227594_thumb.jpeg', '2019-04-23 00:52:39'),
(15, '5 gallon', 'We take pride in offering fast and friendly water delivery services right in front of your door, its convenient dependable and hassle free. Our scheduled water deliveries ensure your drinking water never runs out.', '5 جالون', 'نفتخر بعرض خدمة توصيل سريعة وودودة للمياه حتى عتبة بابك، في خدمة مريحة ويمكن الاعتماد عليها وخالية من المتاعب .\r\nيضمن جدول توزيع المياه الخاص بنا عدم انتهاء مياه الشرب المتوفرة لديك في أي وقت', '9', '-', '-', '8066202_thumb.jpeg', '2019-04-13 18:34:48');

-- --------------------------------------------------------

--
-- Table structure for table `product_buy`
--

CREATE TABLE `product_buy` (
  `id` int(15) NOT NULL,
  `product_id` int(15) NOT NULL,
  `quantity` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `delivered_status` varchar(150) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_buy`
--

INSERT INTO `product_buy` (`id`, `product_id`, `quantity`, `user_id`, `delivered_status`, `date_time`) VALUES
(1, 1, 2, 1, '0', '2018-07-12 09:50:58'),
(2, 125, 2, 1235, '0', '2018-07-12 13:10:27'),
(4, 1, 2, 2, '0', '2018-07-12 14:20:33'),
(5, 2, 1, 1, '0', '2018-07-12 18:15:17'),
(6, 3, 1, 1, '0', '2018-07-12 18:23:50'),
(7, 1, 1, 27, '0', '2018-07-12 18:42:39'),
(8, 2, 5, 2, '0', '2018-07-13 04:53:16'),
(9, 4, 1, 27, '0', '2018-07-13 06:02:35'),
(10, 3, 1, 27, '0', '2018-07-13 06:25:21'),
(12, 2, 5, 6, '0', '2018-07-13 07:09:31'),
(13, 1, 1, 40, '0', '2018-07-13 07:20:36'),
(14, 2, 1, 40, '0', '2018-07-13 08:02:05'),
(15, 1, 1, 30, '0', '2018-07-13 09:09:11'),
(16, 2, 1, 30, '0', '2018-07-13 09:09:59'),
(17, 1, 1, 43, '0', '2018-07-13 10:33:46'),
(18, 3, 1, 43, '0', '2018-07-13 10:39:36'),
(19, 4, 1, 43, '0', '2018-07-13 11:02:41'),
(20, 3, 1, 44, '0', '2018-07-13 11:10:07'),
(21, 1, 1, 31, '0', '2018-07-13 12:05:34'),
(22, 1, 1, 31, '0', '2018-07-14 03:24:25'),
(23, 1, 1, 31, '0', '2018-07-14 03:24:26'),
(24, 2, 1, 31, '0', '2018-07-14 03:24:39'),
(25, 2, 1, 31, '0', '2018-07-14 03:24:40'),
(26, 1, 1, 31, '0', '2018-07-16 09:17:31'),
(27, 1, 1, 31, '0', '2018-07-16 09:17:32'),
(28, 2, 1, 31, '0', '2018-07-16 09:17:44'),
(29, 1, 1, 31, '0', '2018-07-16 09:17:49'),
(30, 1, 1, 31, '0', '2018-07-16 09:17:57'),
(31, 4, 1, 31, '0', '2018-07-16 09:18:06'),
(32, 4, 1, 40, '0', '2018-07-17 04:46:45'),
(33, 3, 1, 40, '0', '2018-07-17 04:49:46'),
(34, 4, 1, 40, '0', '2018-07-17 04:49:59'),
(35, 4, 1, 40, '0', '2018-07-17 04:50:03'),
(36, 4, 1, 40, '0', '2018-07-17 04:50:04'),
(37, 4, 1, 31, '0', '2018-07-17 05:33:30'),
(38, 1, 1, 40, '0', '2018-07-19 07:39:58'),
(47, 1, 2, 22, 'ordered', '2018-07-26 08:47:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` int(15) NOT NULL,
  `order_id` varchar(150) NOT NULL,
  `user_id` int(15) NOT NULL,
  `preorder_date` varchar(150) NOT NULL,
  `preorder_time` varchar(150) NOT NULL,
  `total_price` varchar(250) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `location_lat` float NOT NULL,
  `location_long` float NOT NULL,
  `delivered_status` varchar(150) NOT NULL,
  `order_status` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `order_id`, `user_id`, `preorder_date`, `preorder_time`, `total_price`, `address`, `location_lat`, `location_long`, `delivered_status`, `order_status`, `created_at`) VALUES
(85, '100085', 22, '', '', '100', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:11:33'),
(86, '100086', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Preorder', '2018-08-14 13:13:40'),
(87, '100087', 53, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:26:21'),
(88, '100088', 40, '', '', '250', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:43:17'),
(89, '100089', 53, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:46:49'),
(90, '100090', 53, '', '', '250', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:48:43'),
(91, '100091', 53, '', '', '90', '', 0, 0, 'Ordered', 'Order', '2018-08-14 13:49:07'),
(92, '100092', 22, '', '', '5', '', 0, 0, 'Ordered', 'Order', '2018-08-14 14:11:12'),
(93, '100093', 22, '', '', '15', '', 0, 0, 'Ordered', 'Order', '2018-08-14 14:21:41'),
(94, '100094', 41, '', '', '120', '', 0, 0, 'Ordered', 'Order', '2018-08-14 15:00:03'),
(95, '100095', 53, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-08-21 15:22:08'),
(102, '100096', 41, 'Select Date', 'Select Time', '50', '', 0, 0, 'Ordered', 'Preorder', '2018-08-23 15:06:29'),
(104, '100097', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-08-23 15:06:51'),
(106, '100098', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-08-23 15:31:40'),
(107, '100099', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-08-23 15:34:58'),
(108, '100100', 41, '23-8-2018', '8:35 PM', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-08-23 15:35:20'),
(109, '100101', 41, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-27 13:56:34'),
(113, '100102', 41, '', '', '25', '', 0, 0, 'Ordered', 'Order', '2018-08-28 14:35:12'),
(114, '100103', 41, '', '', '75', '', 0, 0, 'Ordered', 'Order', '2018-08-28 14:40:18'),
(115, '100104', 41, 'Select Date', 'Select Time', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-08-28 14:51:28'),
(116, '100105', 41, '', '', '150', '', 0, 0, 'Ordered', 'Order', '2018-08-28 16:07:47'),
(117, '100106', 41, '8-9-2018', '12:02 PM', '50', '', 0, 0, 'Ordered', 'Preorder', '2018-09-01 12:02:22'),
(118, '100107', 26, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-09-01 12:06:55'),
(119, '100108', 26, '', '', '1050', '', 0, 0, 'Ordered', 'Order', '2018-09-01 12:09:40'),
(120, '100109', 41, '', '', '300', '', 0, 0, 'Ordered', 'Order', '2018-09-03 11:54:39'),
(121, '100110', 62, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-09-03 12:04:20'),
(122, '100111', 62, '3-9-2018', '4:05 PM', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-09-03 12:05:21'),
(123, '100112', 41, '', '', '120', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:31:40'),
(124, '100113', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:31:56'),
(125, '100114', 41, '', '', '5', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:32:02'),
(126, '100115', 41, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:32:07'),
(127, '100116', 41, 'Select Date', 'Select Time', '50', '', 0, 0, 'Ordered', 'Preorder', '2018-09-03 14:32:25'),
(128, '100117', 41, '', '', '30', '', 0, 0, 'Ordered', 'Order', '2018-09-03 14:32:49'),
(129, '100118', 41, '7-9-2018', '7:13 PM', '30', '', 0, 0, 'Ordered', 'Preorder', '2018-09-05 17:14:02'),
(130, '100119', 41, '7-9-2018', '6:47 PM', '25', '', 0, 0, 'Ordered', 'Preorder', '2018-09-05 17:47:05'),
(131, '100120', 22, '', '', '100', '', 1.23545, 1.23545, 'Ordered', 'Order', '2018-09-06 11:11:46'),
(132, '100121', 22, '22/07/2018', '10:00 AM', '260', '', 1.2563, 2.2354, 'Ordered', 'Preorder', '2018-09-06 11:14:31'),
(133, '100122', 41, '', '', '30', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-06 11:37:10'),
(134, '100123', 41, '', '', '75', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-07 10:47:44'),
(135, '100124', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:45:45'),
(136, '100125', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:45:55'),
(137, '100126', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:50:19'),
(138, '100127', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 11:51:59'),
(139, '100128', 41, '', '', '120', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-11 12:16:08'),
(140, '100129', 41, '11-9-2018', '12:16 PM', '25', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-11 12:16:18'),
(141, '100130', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-11 16:26:28'),
(142, '100131', 26, 'Ø§Ø®ØªØ± Ø§Ù„ØªØ§Ø±ÙŠØ®', 'Ø§Ø®ØªØ± Ø§Ù„ÙˆÙ‚Øª', '25', '', 8.57005, 76.9719, 'Ordered', 'Preorder', '2018-09-11 23:52:50'),
(143, '100132', 26, 'Ø§Ø®ØªØ± Ø§Ù„ØªØ§Ø±ÙŠØ®', 'Ø§Ø®ØªØ± Ø§Ù„ÙˆÙ‚Øª', '30', '', 8.57005, 76.9719, 'Ordered', 'Preorder', '2018-09-11 23:53:00'),
(144, '100133', 26, '', '', '150', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-11 23:53:07'),
(145, '100134', 26, '', '', '75', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-11 23:53:14'),
(146, '100135', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-12 10:30:48'),
(147, '100136', 41, '12-9-2018', '10:30 AM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-12 10:30:59'),
(148, '100137', 41, '12-9-2018', '10:31 AM', '25', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-12 10:31:12'),
(149, '100138', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:16:20'),
(150, '100139', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:16:24'),
(151, '100140', 41, '', '', '75', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:16:32'),
(152, '100141', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:17:43'),
(153, '100142', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:40:12'),
(154, '100143', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 11:43:13'),
(155, '100144', 41, '', '', '300', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-14 12:03:29'),
(156, '100145', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-17 13:26:03'),
(157, '100146', 41, '', '', '15', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-17 13:26:03'),
(158, '100147', 26, '', '', '200', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 10:06:35'),
(159, '100148', 26, '', '', '210', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 10:06:35'),
(160, '100149', 41, '', '', '100', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:14:58'),
(161, '100150', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:14:58'),
(162, '100151', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:14:58'),
(163, '100152', 41, '', '', '75', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:19:24'),
(164, '100153', 41, '', '', '25', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-18 10:57:49'),
(165, '100154', 41, '', '', '50', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-18 10:57:49'),
(166, '100155', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:58:35'),
(167, '100156', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:58:35'),
(168, '100157', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:58:35'),
(169, '100158', 41, '', '', '150', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:59:27'),
(170, '100159', 41, '', '', '60', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 10:59:27'),
(171, '100160', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:01:24'),
(172, '100161', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:01:24'),
(173, '100162', 41, '', '', '30', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:02:13'),
(174, '100163', 41, '', '', '50', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:02:13'),
(175, '100164', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 11:02:13'),
(176, '100165', 26, '', '', '200', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 12:39:11'),
(177, '100166', 26, '', '', '210', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 12:39:11'),
(178, '100167', 26, '', '', '250', '', 8.49568, 76.9316, 'Ordered', 'Order', '2018-09-18 12:40:51'),
(179, '100168', 26, '', '', '200', '', 8.49524, 76.9324, 'Ordered', 'Order', '2018-09-18 12:40:51'),
(180, '100169', 27, '', '', '80', '', 2.21122, 2.36555, 'Ordered', 'Order', '2018-09-18 14:06:55'),
(181, '100170', 41, '', '', '2060', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 14:22:09'),
(182, '100171', 22, '', '', '205', '', 0, 0, 'Ordered', 'Order', '2018-09-18 17:11:11'),
(183, '100172', 41, '', '', '100', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 17:31:07'),
(184, '100173', 41, '', '', '105', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-18 17:35:15'),
(185, '100174', 26, '', '', '300', '', 8.57005, 76.9719, 'Ordered', 'Order', '2018-09-18 17:45:46'),
(186, '100175', 41, '', '', '470', '', 8.51935, 76.9599, 'Ordered', 'Order', '2018-09-19 13:57:51'),
(187, '', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 14:21:13'),
(188, '', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 14:21:13'),
(189, '', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 14:22:36'),
(190, '', 41, '', '', '105', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-19 14:23:07'),
(191, '', 41, '', '', '105', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-19 14:23:25'),
(192, '100176', 22, '22/07/2018', '10:00 AM', '260', '', 0, 0, 'Ordered', 'Order', '2018-09-19 15:31:36'),
(193, '100177', 41, '', '', '25', '', 8.51936, 76.96, 'Ordered', 'Order', '2018-09-19 15:32:31'),
(194, '100178', 41, '', '', '470', '', 8.51935, 76.9599, 'Ordered', 'Order', '2018-09-19 15:32:51'),
(195, '100179', 63, '', '', '130', '', 25.7785, 55.9697, 'Ordered', 'Order', '2018-09-20 10:50:15'),
(196, '100180', 41, '19-9-2018', '11:28 AM', '50', '', 8.51935, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:28:37'),
(197, '100181', 41, '21-9-2018', '11:30 AM', '25', '', 8.51935, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:30:13'),
(198, '100182', 41, '27-9-2018', '11:33 AM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:33:12'),
(199, '100183', 41, '27-9-2018', '11:34 AM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:34:58'),
(200, '100184', 41, '26-9-2018', '4:35 PM', '25', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:35:52'),
(201, '100185', 41, '21-9-2018', '5:36 PM', '50', '', 8.51936, 76.96, 'Ordered', 'Preorder', '2018-09-21 11:36:22'),
(202, '100186', 41, '', '', '275', '', 8.51935, 76.96, 'Ordered', 'Order', '2018-09-21 11:37:46'),
(203, '100187', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-09-21 11:56:36'),
(204, '100188', 26, '', '', '350', '', 8.55243, 76.877, 'Ordered', 'Order', '2018-09-24 12:52:39'),
(205, '100189', 26, '26-9-2018', '5:52 AM', '50', '', 8.55243, 76.877, 'Ordered', 'Preorder', '2018-09-24 12:52:53'),
(206, '100190', 41, '', '', '12175', '', 8.51934, 76.96, 'Ordered', 'Order', '2018-09-28 16:14:52'),
(207, '100191', 41, '', '', '25', '', 8.51934, 76.96, 'Ordered', 'Order', '2018-09-28 16:20:37'),
(208, '100192', 26, '', '', '420', '', 8.52465, 76.9423, 'Ordered', 'Order', '2018-09-30 17:31:14'),
(209, '100193', 26, '', '', '110', '', 8.56051, 76.8802, 'Ordered', 'Order', '2018-10-04 10:44:17'),
(210, '100194', 26, '', '', '200', '', 8.5605, 76.8803, 'Ordered', 'Order', '2018-10-12 14:05:52'),
(211, '100195', 26, '', '', '200', '', 8.56052, 76.8803, 'Ordered', 'Order', '2018-10-23 10:01:24'),
(212, '100196', 26, '', '', '130', '', 8.56356, 76.8589, 'Ordered', 'Order', '2018-10-26 17:38:42'),
(213, '100197', 26, '', '', '100', '', 8.56356, 76.8589, 'Ordered', 'Order', '2018-10-26 17:38:59'),
(214, '100198', 26, '', '', '300', '', 8.53943, 76.9457, 'Ordered', 'Order', '2018-11-06 20:53:05'),
(215, '100199', 41, '14-11-2018', '6:06 PM', '25', '', 8.51934, 76.9599, 'Ordered', 'Preorder', '2018-11-14 14:06:43'),
(216, '100200', 41, '', '', '75', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-11-14 14:07:01'),
(217, '100201', 26, '', '', '250', '', 0, 0, 'Ordered', 'Order', '2018-11-15 10:20:01'),
(218, '100202', 26, '', '', '350', '', 0, 0, 'Ordered', 'Order', '2018-11-15 12:02:24'),
(219, '100203', 26, '', '', '190', '', 0, 0, 'Ordered', 'Order', '2018-11-15 14:47:11'),
(220, '100204', 103, '', '', '170', '', 25.7915, 55.9419, 'Ordered', 'Order', '2018-11-23 21:23:29'),
(221, '100205', 104, '23-11-2018', '12:15 AM', '50', '', 25.7913, 55.9416, 'Ordered', 'Preorder', '2018-11-23 22:46:00'),
(222, '100206', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-11-26 16:29:21'),
(223, '100207', 41, '1-12-2018', '9:16 PM', '11.25', '', 8.51934, 76.9599, 'Ordered', 'Preorder', '2018-11-27 14:16:40'),
(224, '100208', 41, '', '', '50', '', 0, 0, 'Ordered', 'Order', '2018-11-27 14:17:13'),
(225, '100209', 26, '', '', '6750', '', 8.52121, 76.9065, 'Ordered', 'Order', '2018-11-29 15:00:39'),
(226, '100210', 26, '6-12-2018', '3:16 PM', '12', '', 8.54638, 76.9603, 'Ordered', 'Preorder', '2018-11-29 15:16:55'),
(227, '100211', 26, '', '', '12', '', 8.51864, 76.9, 'Ordered', 'Order', '2018-11-29 15:49:18'),
(228, '100212', 41, '1-12-2018', '6:43 PM', '11.25', '', 0, 0, 'Ordered', 'Preorder', '2018-11-30 17:43:58'),
(229, '100213', 41, '', '', '77', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 15:31:57'),
(230, '100214', 41, '', '', '81', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 15:43:54'),
(231, '100215', 41, '', '', '57.5', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 15:46:48'),
(232, '100216', 41, '', '', '117.5', '', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-05 17:12:27'),
(233, '100217', 26, '', '', '1150', '', 8.56051, 76.8803, 'Ordered', 'Order', '2018-12-06 10:08:48'),
(234, '100218', 41, '', '', '246', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:11:02'),
(235, '100219', 41, '', '', '72', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:18:06'),
(236, '100220', 41, '', '', '45', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:21:20'),
(237, '100221', 41, '', '', '21', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:25:06'),
(238, '100222', 41, '', '', '36', 'aravind,test,test,test', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-06 14:35:17'),
(239, '100223', 26, '', '', '56.25', '', 8.56035, 76.88, 'Ordered', 'Order', '2018-12-06 16:26:33'),
(240, '100224', 26, '', '', '67.5', '', 8.56052, 76.8803, 'Ordered', 'Order', '2018-12-06 17:31:03'),
(241, '100225', 26, '', '', '1825', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56052, 76.8803, 'Ordered', 'Order', '2018-12-07 11:17:27'),
(242, '100226', 41, '', '', '191.25', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:26:59'),
(243, '100227', 41, '', '', '11.25', 'fhjk,fhjj,gjjk,vhji', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:30:02'),
(244, '100228', 41, '', '', '11.25', 'rrd,add,hjj,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:45:56'),
(245, '100229', 41, '', '', '48', 'fga,sggs,shbs,ahba', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:53:45'),
(246, '100230', 41, '', '', '48', 'ahbs,zvvs,svvd,vzvd', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:54:11'),
(247, '100231', 41, '', '', '46', 'rf,hhj,hjiu,Kerala', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:54:47'),
(248, '100232', 41, '', '', '11.25', 'fgh,huk,yio,hjo', 8.51934, 76.9599, 'Ordered', 'Order', '2018-12-07 11:55:12'),
(249, '100233', 26, '', '', '3187.5', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56032, 76.8801, 'Ordered', 'Order', '2018-12-07 16:22:57'),
(250, '100234', 26, '', '', '12', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56032, 76.8801, 'Ordered', 'Order', '2018-12-07 16:23:15'),
(251, '100235', 26, '', '', '33.75', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.56054, 76.8803, 'Ordered', 'Order', '2018-12-07 17:25:18'),
(252, '100236', 105, '', '', '90', 'shihab,dfff,dfgg,fffg', 8.75146, 76.9353, 'Ordered', 'Order', '2018-12-08 13:51:22'),
(253, '100237', 105, '', '', '58.5', 'shihab,dfff,dfgg,fffg', 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-11 22:53:55'),
(254, '100238', 26, '', '', '74.25', 'Anish,Nila Building,Technopark Road,Thiruvananthapuram', 8.52686, 76.9913, 'Ordered', 'Order', '2018-12-13 20:24:06'),
(255, '100239', 41, '', '', '33.75', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:40:43'),
(256, '100240', 41, '', '', '34.5', 'dfyfh,guu,fyu,fhjk', 0, 0, 'Ordered', 'Order', '2018-12-14 10:42:16'),
(257, '100241', 41, '', '', '54', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:45:50'),
(258, '100242', 41, '', '', '33.75', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:46:46'),
(259, '100243', 41, '', '', '24', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 10:49:02'),
(260, '100244', 41, '', '', '24', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 10:50:45'),
(261, '100245', 41, '', '', '12', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 10:52:49'),
(262, '100246', 41, '', '', '12', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 12:22:17'),
(263, '100247', 41, '', '', '11.25', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 16:58:41'),
(264, '100248', 41, '', '', '24', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 16:59:16'),
(265, '100249', 41, '', '', '22.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 16:59:41'),
(266, '100250', 41, '', '', '11.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 17:00:12'),
(267, '100251', 41, '', '', '36', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 17:01:09'),
(268, '100252', 41, '', '', '6', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 0, 0, 'Ordered', 'Order', '2018-12-14 17:01:51'),
(269, '100253', 41, '', '', '42', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.41738, 77.0192, 'Ordered', 'Order', '2018-12-14 17:20:16'),
(270, '100254', 26, '', '', '1350', 'Rajesh,Sree Chithira Lane,Muttada,Kerala', 8.54016, 76.9504, 'Ordered', 'Order', '2018-12-16 01:25:05'),
(271, '100255', 41, '', '', '22.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:10:18'),
(272, '100256', 41, '', '', '11.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:11:37'),
(273, '100257', 41, '', '', '22.5', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:21:01'),
(274, '100258', 41, '', '', '46', 'Aravind,PMG TTC Road,Next to ICICI bank,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-17 17:21:19'),
(275, '100259', 146, '', '', '81.75', 'gxjck,Ali Plaza,Thiruvananthapuram,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-19 15:01:56'),
(276, '100260', 146, '', '', '82.5', 'Main Test,Ali Plaza,Thiruvananthapuram,Kerala', 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-20 14:43:41'),
(277, '100261', 164, '', '', '67.5', 'ajmal,gggg,ggfu,yuuu', 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-22 12:34:09'),
(278, '100262', 164, '', '', '6', 'ajmal,gggg,ggfu,yuuu', 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-22 12:39:11'),
(279, '100263', 146, '', '', '22.5', 'Main Test,Ali Plaza,Thiruvananthapuram,Kerala', 8.42617, 77.0337, 'Ordered', 'Order', '2018-12-22 18:34:06'),
(280, '100264', 170, '', '', '200.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.2852, 55.4463, 'Delivered', 'Order', '2018-12-23 17:06:39'),
(281, '100265', 160, '27-12-2018', '4:15 AM', '11.25', NULL, 8.51926, 76.9599, 'Ordered', 'Preorder', '2018-12-26 17:03:05'),
(282, '100266', 160, '27-12-2018', '4:15 AM', '11.25', NULL, 8.51926, 76.9599, 'Ordered', 'Preorder', '2018-12-26 17:23:09'),
(283, '100267', 52, '28-12-2018', '6:00 AM', '11.25', NULL, 8.56905, 76.8723, 'Ordered', 'Preorder', '2018-12-26 17:29:15'),
(284, '100268', 52, '27-12-2018', '12:28 PM', '11.25', NULL, 8.56975, 76.8733, 'Ordered', 'Preorder', '2018-12-26 17:29:57'),
(285, '100269', 160, '', '', '69.75', 'Anish,Sreekanteswara Temple Rd,Thiruvananthapuram,Kerala', 8.51939, 76.9599, 'Ordered', 'Order', '2018-12-26 17:31:02'),
(286, '100270', 52, '', '', '67.5', 'Baiskah,Kazhakootam Bus Stop,Kazhakkoottam,695582', 8.56792, 76.8736, 'Ordered', 'Order', '2018-12-26 17:31:09'),
(287, '100271', 160, '', '', '69.75', NULL, 8.51939, 76.9599, 'Ordered', 'Order', '2018-12-26 17:31:21'),
(288, '100272', 52, '', '', '67.5', NULL, 8.56792, 76.8736, 'Ordered', 'Order', '2018-12-26 17:31:35'),
(289, '100273', 160, '', '', '69.75', NULL, 8.51939, 76.9599, 'Ordered', 'Order', '2018-12-26 17:31:35'),
(290, '100274', 52, '', '', '67.5', NULL, 8.56792, 76.8736, 'Ordered', 'Order', '2018-12-26 17:31:40'),
(291, '100275', 52, '', '', '33.75', 'Rajesh,Pazhaya Mudipura Road,Nalanchira,Thiruvananthapuram', 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:33:06'),
(292, '100276', 52, '', '', '33.75', NULL, 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:33:38'),
(293, '100277', 52, '', '', '33.75', NULL, 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:34:31'),
(294, '100278', 52, '', '', '33.75', NULL, 8.56905, 76.8723, 'Ordered', 'Order', '2018-12-26 17:34:45'),
(295, '100279', 160, '', '', '36', 'Amish,Ali Plaza,Thiruvananthapuram,695003', 8.51941, 76.9599, 'Ordered', 'Order', '2018-12-26 17:51:03'),
(296, '100280', 170, '28-12-2018', '6:46 PM', '11.25', NULL, 0, 0, 'Delivered', 'Preorder', '2018-12-26 18:16:31'),
(297, '100281', 170, '', '', '200.5', NULL, 25.2852, 55.4463, 'Ordered', 'Order', '2018-12-26 18:16:56'),
(298, '100282', 173, '', '', '307.5', 'ZEENATH,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,sdffru', 25.7912, 55.9436, 'Delivered', 'Order', '2018-12-27 20:53:25'),
(299, '100283', 173, '28-12-2018', '5:00 PM', '11.25', NULL, 25.7912, 55.9435, 'Ordered', 'Preorder', '2018-12-27 21:00:10'),
(300, '100284', 173, '', '', '307.5', NULL, 25.7912, 55.9436, 'Ordered', 'Order', '2018-12-27 21:01:48'),
(301, '100285', 174, '', '', '11', 'sultan afzal,royal breeze 2,flat 202,rak', 25.7005, 55.7836, 'Ordered', 'Order', '2018-12-29 08:19:04'),
(302, '100286', 146, '', '', '82.5', NULL, 8.51935, 76.9599, 'Ordered', 'Order', '2018-12-31 14:18:08'),
(303, '100287', 165, '', '', '1162.5', 'ajmal,Al Manama - Ras Al Khaimah Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7794, 55.9692, 'Ordered', 'Order', '2019-01-01 11:21:06'),
(304, '100288', 146, '5-1-2019', '9:06 PM', '11.25', NULL, 8.51935, 76.9599, 'Ordered', 'Preorder', '2019-01-01 16:06:46'),
(305, '100289', 146, '', '', '150', 'Main Test,Ali Plaza,Thiruvananthapuram,Kerala', 8.51935, 76.9599, 'Delivered', 'Order', '2019-01-01 16:07:05'),
(306, '100290', 146, '11-1-2019', '6:08 AM', '7', NULL, 8.51935, 76.9599, 'Ordered', 'Preorder', '2019-01-01 16:08:28'),
(307, '100291', 170, '', '', '693.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7907, 55.9416, 'Ordered', 'Order', '2019-01-01 17:22:55'),
(308, '100292', 170, '', '', '192.75', 'ra,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7895, 55.9419, 'Ordered', 'Order', '2019-01-01 23:06:39'),
(309, '100293', 170, '', '', '1730.25', 'ra,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7895, 55.9419, 'Delivered', 'Order', '2019-01-01 23:08:18'),
(310, '100294', 165, '', '', '677.5', 'nijam,Al Manama - Ras Al Khaimah Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7782, 55.9694, 'Ordered', 'Order', '2019-01-02 11:05:51'),
(311, '100295', 165, '', '', '677.5', NULL, 25.7782, 55.9694, 'Delivered', 'Order', '2019-01-02 11:06:50'),
(312, '100296', 170, '', '', '210', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7897, 55.9416, 'Cancelled', 'Order', '2019-01-03 15:30:44'),
(313, '100297', 170, '', '', '1400', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7908, 55.9419, 'Delivered', 'Order', '2019-01-03 16:19:45'),
(314, '100298', 170, '', '', '2109.75', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7778, 55.9677, 'Ordered', 'Order', '2019-01-03 18:26:00'),
(315, '100299', 170, '', '', '1279.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:12:01'),
(316, '100300', 170, '', '', '1279.5', NULL, 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:12:30'),
(317, '100301', 170, '10-1-2019', '12:40 PM', '11.25', NULL, 8.53939, 76.9455, 'Ordered', 'Preorder', '2019-01-03 23:29:34'),
(318, '100302', 170, '', '', '135', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:31:17'),
(319, '100303', 170, '', '', '135', NULL, 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:41:12'),
(320, '100304', 170, '', '', '135', NULL, 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-03 23:46:27'),
(321, '100305', 170, '', '', '11.25', 'Rajesh,110,Thiruvananthapuram,Kerala', 8.53939, 76.9455, 'Ordered', 'Order', '2019-01-04 00:10:22'),
(322, '100306', 170, '', '', '11.25', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.55919, 76.8778, 'Ordered', 'Order', '2019-01-04 13:58:20'),
(323, '100307', 153, '4-1-2019', '7:45 PM', '12', NULL, 8.51937, 76.9599, 'Ordered', 'Preorder', '2019-01-04 15:46:03'),
(324, '100308', 153, '11-1-2019', '10:49 PM', '12', NULL, 8.51937, 76.9599, 'Ordered', 'Preorder', '2019-01-04 15:50:03'),
(325, '100309', 153, '', '', '36', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-04 16:07:12'),
(326, '100310', 153, '', '', '100', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-04 16:48:31'),
(327, '100311', 153, '', '', '36', NULL, 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-04 17:03:02'),
(328, '100312', 170, '', '', '39.25', 'Rajesh,techno park Kazhakoottam,Thiruvananthapuram,Kerala', 8.55655, 76.8766, 'Ordered', 'Order', '2019-01-04 20:22:32'),
(329, '100313', 165, '', '', '225', 'nijam,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7784, 55.9677, 'Ordered', 'Order', '2019-01-05 11:04:34'),
(330, '100314', 165, '', '', '225', NULL, 25.7784, 55.9677, 'Ordered', 'Order', '2019-01-05 11:17:15'),
(331, '100315', 170, '', '', '56.25', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.33198, 77.0523, 'Ordered', 'Order', '2019-01-05 17:49:08'),
(332, '100316', 170, '', '', '67.5', 'rams,Khuzam Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 8.56053, 76.8804, 'Ordered', 'Order', '2019-01-07 17:22:34'),
(333, '100317', 170, '', '', '67.5', NULL, 8.56053, 76.8804, 'Ordered', 'Order', '2019-01-07 17:23:39'),
(334, '100318', 170, '24-1-2019', '1:45 PM', '11.25', NULL, 25.791, 55.9416, 'Ordered', 'Preorder', '2019-01-12 15:15:43'),
(335, '100319', 153, '', '', '666.5', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-01-17 17:30:30'),
(336, '100320', 180, '', '', '232.5', 'fggh,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7897, 55.9419, 'Ordered', 'Order', '2019-01-19 15:04:47'),
(337, '100321', 180, '', '', '232.5', NULL, 25.7897, 55.9419, 'Ordered', 'Order', '2019-01-19 15:06:00'),
(338, '100322', 181, '', '', '342.75', 'dgghf,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7912, 55.9434, 'Ordered', 'Order', '2019-01-21 00:35:03'),
(339, '100323', 181, '', '', '342.75', NULL, 25.7912, 55.9434, 'Ordered', 'Order', '2019-01-21 00:37:16'),
(340, '100324', 180, '', '', '700', 'Ajmal,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7908, 55.9409, 'Delivered', 'Order', '2019-01-26 14:58:13'),
(341, '100325', 197, '', '', '7', 'najim,Unnamed Road - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7912, 55.9435, 'Ordered', 'Order', '2019-01-26 15:39:13'),
(342, '100326', 153, '', '', '27', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.56058, 76.8804, 'Ordered', 'Order', '2019-01-29 14:53:47'),
(343, '100327', 153, '', '', '14', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-01 17:02:13'),
(344, '100328', 153, '', '', '22.5', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-01 17:16:42'),
(345, '100329', 153, '', '', '33.75', 'aravindAli PlazaKowdiarKerala', 8.51937, 76.9599, 'Ordered', 'Order', '2019-02-06 16:39:03'),
(346, '100330', 153, '', '', '43.50', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.51938, 76.9599, 'Ordered', 'Order', '2019-02-07 10:53:22'),
(347, '100331', 153, '', '', '21.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-07 11:28:35'),
(348, '100332', 205, '', '', '45.00', 'Al yamuna densons fze,Unnamed Road - Ras al Khaimah - United Arab Emirates,Al Riffa,Ras al Khaimah', 25.7123, 55.845, 'Delivered', 'Order', '2019-02-11 08:35:43'),
(349, '100333', 153, '', '', '15.70', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Delivered', 'Order', '2019-02-11 14:47:35'),
(350, '100334', 153, '2019-02-15 00:00:00.000', '0001-01-01 16:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 11:56:37'),
(351, '100335', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 11:58:19'),
(352, '100336', 153, '2019-02-21 00:00:00.000', '0001-01-01 19:00:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 11:59:14'),
(353, '100337', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:01:35'),
(354, '100338', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:04:24'),
(355, '100339', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:00:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:07:20'),
(356, '100340', 153, '2019-02-27 00:00:00.000', '0001-01-01 18:00:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:12:59'),
(357, '100341', 153, '2019-02-28 00:00:00.000', '0001-01-01 17:39:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:39:49'),
(358, '100342', 153, '2019-02-27 00:00:00.000', '0001-01-01 18:40:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:40:27'),
(359, '100343', 153, '2019-02-20 00:00:00.000', '0001-01-01 21:42:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:42:25'),
(360, '100344', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:45:00.000', '12', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:45:17'),
(361, '100345', 153, '2019-02-21 00:00:00.000', '0001-01-01 17:46:00.000', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:46:20'),
(362, '100346', 153, '2019-02-21', '05:47PM', '11.25', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:47:08'),
(363, '100347', 153, '2019-02-14', '11:55PM', '36', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-14 12:55:33'),
(364, '100348', 153, '', '', '174.25', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-14 15:11:46'),
(365, '100349', 153, '', '', '60.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-02-14 15:19:35'),
(366, '100350', 196, '', '', '108.00', 'shihabudeen,Al Muntasir Rd - Ras al Khaimah - United Arab Emirates,Ras Al-Khaimah,Ras al Khaimah', 25.7795, 55.9681, 'Ordered', 'Order', '2019-02-20 20:43:48'),
(367, '100351', 208, '???? ???????', '???? ?????', '9', NULL, 25.3868, 55.4538, 'Ordered', 'Preorder', '2019-02-24 14:32:39'),
(368, '100352', 209, '25-2-2019', '6:50 PM', '11.5', NULL, 0, 0, 'Ordered', 'Preorder', '2019-02-25 00:20:21'),
(369, '100353', 205, '', '', '45.00', 'Al yamuna densons,Unnamed Road - Ras al Khaimah - United Arab Emirates,Al Riffa,Ras al Khaimah', 25.7123, 55.845, 'Ordered', 'Order', '2019-02-25 21:09:29'),
(370, '100354', 187, '', '', '54.00', 'vijay,Unnamed Road - Ras al Khaimah - United Arab Emirates,Al Jazirah Al Hamra,Ras al Khaimah', 25.6862, 55.7999, 'Ordered', 'Order', '2019-03-05 10:09:08'),
(371, '100355', 153, '', '', '35.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-03-11 16:44:41'),
(372, '100356', 153, '', '', '107.50', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 8.56035, 76.8803, 'Ordered', 'Order', '2019-03-19 12:17:44'),
(373, '100357', 153, '', '', '42.00', 'aravind,Ali Plaza,Thiruvananthapuram,Kerala', 11.1105, 77.3418, 'Ordered', 'Order', '2019-03-21 11:59:44'),
(374, '100358', 153, '', '', '14.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-03-21 12:41:55'),
(375, '100359', 153, '2019-03-21', '04:46PM', '34.5', NULL, 0, 0, 'Ordered', 'Preorder', '2019-03-21 12:46:50'),
(376, '100360', 217, '', '', '234.50', 'hassan,ajman,ajmam,ajman', 25.387, 55.4534, 'Ordered', 'Order', '2019-03-24 20:53:57'),
(377, '100361', 153, '', '', '7.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-03-27 18:04:43'),
(378, '100362', 153, '', '', '7.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-01 13:33:00'),
(379, '100363', 153, '', '', '25.00', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-02 17:20:29'),
(380, '100364', 179, '', '', '28.00', 'ramshiAl Jazeerah Industrial1Al Jazeerah Industrial1Ras al-Khaimah', 25.6923, 55.7995, 'Delivered', 'Order', '2019-04-02 18:34:09'),
(381, '100365', 179, '', '', '262.25', 'ramshirakrakras all khaimah', 0, 0, 'Delivered', 'Order', '2019-04-02 18:35:08'),
(382, '100366', 179, '', '', '54.00', 'ramshirakrakras all khaimah', 0, 0, 'Delivered', 'Order', '2019-04-13 18:49:14'),
(383, '100367', 153, '', '', '83.50', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-25 16:34:52'),
(384, '100368', 177, '', '', '290.00', 'AjmalUnnamed Road - Ras al Khaimah - United Arab EmiratesRas Al-KhaimahRas al Khaimah', 25.7911, 55.9437, 'Ordered', 'Order', '2019-04-25 18:35:48'),
(385, '100369', 153, '2019-04-27', '11:44AM', '45', NULL, 0, 0, 'Ordered', 'Preorder', '2019-04-26 11:44:12'),
(386, '100370', 153, '', '', '18.25', 'aravindAli PlazaThiruvananthapuramKerala', 0, 0, 'Ordered', 'Order', '2019-04-29 10:58:09'),
(387, '100371', 153, '', '', '122.25', 'Rajesh695581KazhakkottamKerala', 8.56039, 76.8805, 'Ordered', 'Order', '2019-04-29 15:57:42'),
(388, '100372', 153, '2019-05-15', '10:58PM', '230', NULL, 0, 0, 'Ordered', 'Preorder', '2019-04-29 15:59:18'),
(389, '100373', 153, '', '', '126.00', 'Vivek695581KazhakkottamKerala', 8.56039, 76.8805, 'Ordered', 'Order', '2019-04-29 15:59:59'),
(390, '100374', 153, '', '', '1375.00', 'johnKazhakuttomKazhakuttomKerala', 8.56042, 76.8806, 'Ordered', 'Order', '2019-05-02 11:16:42'),
(391, '100375', 153, '2019-05-16', '11:20AM', '105', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:20:31'),
(392, '100376', 153, '2019-05-18', '09:20AM', '156', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:20:58'),
(393, '100377', 153, '2019-05-17', '10:21AM', '98', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:21:38'),
(394, '100378', 153, '2019-05-27', '02:29PM', '84', NULL, 0, 0, 'Ordered', 'Preorder', '2019-05-02 11:29:25'),
(395, '100379', 177, '', '', '35.00', 'AjmalUnnamed Road - Ras al Khaimah - United Arab EmiratesRas Al-KhaimahRas al Khaimah', 0, 0, 'Ordered', 'Order', '2019-05-27 13:56:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_order_detail`
--

CREATE TABLE `product_order_detail` (
  `id` int(15) NOT NULL,
  `product_order_id` int(15) NOT NULL,
  `product_id` int(15) NOT NULL,
  `quantity` int(20) NOT NULL,
  `total_price` varchar(250) NOT NULL,
  `total_special_price` varchar(250) NOT NULL,
  `total_discount_price` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_order_detail`
--

INSERT INTO `product_order_detail` (`id`, `product_order_id`, `product_id`, `quantity`, `total_price`, `total_special_price`, `total_discount_price`) VALUES
(107, 85, 1, 2, '100', '90', '40'),
(108, 86, 1, 2, '100', '90', '40'),
(109, 86, 2, 2, '50', '44', '24'),
(110, 86, 3, 3, '90', '75', '15'),
(111, 86, 4, 4, '20', '16', '4'),
(112, 87, 2, 1, '25', '22', '12'),
(113, 88, 1, 5, '250', '225', '100'),
(114, 89, 2, 1, '25', '22', '12'),
(115, 90, 1, 5, '250', '225', '100'),
(116, 91, 3, 3, '90', '75', '15'),
(117, 92, 4, 1, '5', '4', '1'),
(118, 93, 4, 3, '15', '12', '3'),
(119, 94, 3, 4, '120', '100', '20'),
(120, 95, 3, 1, '30', '25', '5'),
(139, 102, 1, 1, '50', '45', '20'),
(143, 104, 1, 1, '50', '45', '20'),
(147, 106, 1, 1, '50', '45', '20'),
(148, 107, 1, 1, '50', '45', '20'),
(149, 108, 2, 1, '25', '22', '12'),
(150, 109, 2, 1, '25', '22', '12'),
(160, 113, 2, 1, '25', '22', '12'),
(161, 114, 2, 3, '75', '66', '36'),
(162, 115, 2, 1, '25', '22', '12'),
(163, 116, 1, 3, '150', '135', '60'),
(164, 117, 1, 1, '50', '45', '20'),
(165, 118, 1, 1, '50', '45', '20'),
(166, 119, 1, 21, '1050', '945', '420'),
(167, 120, 1, 6, '300', '270', '120'),
(168, 121, 3, 1, '30', '25', '5'),
(169, 122, 2, 1, '25', '22', '12'),
(170, 123, 3, 4, '120', '100', '20'),
(171, 124, 1, 1, '50', '45', '20'),
(172, 125, 4, 1, '5', '4', '1'),
(173, 126, 3, 1, '30', '25', '5'),
(174, 127, 1, 1, '50', '45', '20'),
(175, 128, 3, 1, '30', '25', '5'),
(176, 129, 3, 1, '30', '25', '5'),
(177, 130, 2, 1, '25', '22', '12'),
(178, 131, 1, 2, '100', '90', '40'),
(179, 132, 1, 2, '100', '90', '40'),
(180, 132, 2, 2, '50', '44', '24'),
(181, 132, 3, 3, '90', '75', '15'),
(182, 132, 4, 4, '20', '16', '4'),
(183, 133, 5, 1, '7', '25', '5'),
(184, 134, 2, 3, '75', '66', '36'),
(185, 135, 2, 1, '25', '22', '12'),
(186, 136, 2, 1, '25', '22', '12'),
(187, 137, 2, 1, '25', '22', '12'),
(188, 138, 2, 1, '25', '22', '12'),
(189, 139, 3, 4, '120', '100', '20'),
(190, 140, 2, 1, '25', '22', '12'),
(191, 141, 2, 1, '25', '22', '12'),
(192, 142, 2, 1, '25', '22', '12'),
(193, 143, 3, 1, '30', '25', '5'),
(194, 144, 1, 3, '150', '135', '60'),
(195, 145, 2, 3, '75', '66', '36'),
(196, 146, 1, 1, '50', '45', '20'),
(197, 147, 1, 1, '50', '45', '20'),
(198, 148, 2, 1, '25', '22', '12'),
(199, 149, 2, 1, '25', '22', '12'),
(200, 150, 2, 1, '25', '22', '12'),
(201, 151, 2, 3, '75', '66', '36'),
(202, 152, 1, 1, '50', '45', '20'),
(203, 153, 3, 1, '30', '25', '5'),
(204, 154, 2, 1, '25', '22', '12'),
(205, 155, 1, 6, '300', '270', '120'),
(206, 156, 2, 2, '50', '44', '24'),
(207, 157, 4, 3, '15', '12', '3'),
(208, 158, 1, 4, '200', '180', '80'),
(209, 159, 3, 7, '210', '175', '35'),
(210, 160, 2, 4, '100', '88', '48'),
(211, 161, 2, 1, '25', '22', '12'),
(212, 162, 2, 1, '25', '22', '12'),
(213, 163, 2, 3, '75', '66', '36'),
(214, 164, 2, 1, '25', '22', '12'),
(215, 165, 1, 1, '50', '45', '20'),
(216, 166, 2, 1, '25', '22', '12'),
(217, 167, 1, 1, '50', '45', '20'),
(218, 168, 3, 1, '30', '25', '5'),
(219, 169, 1, 3, '150', '135', '60'),
(220, 170, 3, 2, '60', '50', '10'),
(221, 171, 2, 1, '25', '22', '12'),
(222, 172, 3, 1, '30', '25', '5'),
(223, 173, 3, 1, '30', '25', '5'),
(224, 174, 1, 1, '50', '45', '20'),
(225, 175, 2, 1, '25', '22', '12'),
(226, 176, 1, 4, '200', '180', '80'),
(227, 177, 3, 7, '210', '175', '35'),
(228, 178, 1, 5, '250', '225', '100'),
(229, 179, 1, 4, '200', '180', '80'),
(230, 180, 1, 4, '200', '180', '80'),
(231, 180, 1, 4, '200', '180', '80'),
(232, 181, 1, 1, '50', '45', '20'),
(233, 181, 1, 1, '50', '45', '20'),
(234, 181, 2, 4, '100', '88', '48'),
(235, 181, 2, 3, '75', '66', '36'),
(236, 182, 1, 2, '100', '90', '40'),
(237, 182, 2, 3, '75', '66', '36'),
(238, 182, 3, 1, '30', '25', '5'),
(239, 183, 2, 2, '50', '44', '24'),
(240, 183, 1, 1, '50', '45', '20'),
(241, 184, 2, 1, '25', '22', '12'),
(242, 184, 3, 1, '30', '25', '5'),
(243, 184, 1, 1, '50', '45', '20'),
(244, 185, 2, 12, '300', '264', '144'),
(245, 186, 1, 6, '300', '270', '120'),
(246, 186, 2, 2, '50', '44', '24'),
(247, 186, 3, 2, '60', '50', '10'),
(248, 186, 4, 12, '60', '48', '12'),
(249, 189, 1, 2, '100', '90', '40'),
(250, 189, 2, 2, '50', '44', '24'),
(251, 189, 3, 3, '90', '75', '15'),
(252, 189, 4, 4, '20', '16', '4'),
(253, 190, 2, 1, '25', '22', '12'),
(254, 190, 3, 1, '30', '25', '5'),
(255, 190, 1, 1, '50', '45', '20'),
(256, 191, 2, 1, '25', '22', '12'),
(257, 191, 3, 1, '30', '25', '5'),
(258, 191, 1, 1, '50', '45', '20'),
(259, 192, 1, 2, '100', '90', '40'),
(260, 192, 2, 2, '50', '44', '24'),
(261, 192, 3, 3, '90', '75', '15'),
(262, 192, 4, 4, '20', '16', '4'),
(263, 193, 2, 1, '25', '22', '12'),
(264, 194, 1, 6, '300', '270', '120'),
(265, 194, 2, 2, '50', '44', '24'),
(266, 194, 3, 2, '60', '50', '10'),
(267, 194, 4, 12, '60', '48', '12'),
(268, 195, 3, 1, '30', '25', '5'),
(269, 195, 1, 2, '100', '90', '40'),
(270, 196, 1, 1, '50', '45', '20'),
(271, 197, 2, 1, '25', '22', '12'),
(272, 198, 1, 1, '50', '45', '20'),
(273, 199, 1, 1, '50', '45', '20'),
(274, 200, 2, 1, '25', '22', '12'),
(275, 201, 1, 1, '50', '45', '20'),
(276, 202, 1, 3, '150', '135', '60'),
(277, 202, 2, 5, '125', '110', '60'),
(278, 203, 1, 1, '50', '45', '20'),
(279, 204, 1, 7, '350', '315', '140'),
(280, 205, 1, 1, '50', '45', '20'),
(281, 206, 2, 1, '25', '22', '12'),
(282, 206, 1, 243, '12150', '10935', '4860'),
(283, 207, 2, 1, '25', '22', '12'),
(284, 208, 4, 4, '20', '16', '4'),
(285, 208, 1, 8, '400', '360', '160'),
(286, 209, 3, 2, '60', '50', '10'),
(287, 209, 1, 1, '50', '45', '20'),
(288, 210, 1, 4, '200', '180', '80'),
(289, 211, 1, 4, '200', '180', '80'),
(290, 212, 3, 1, '30', '25', '5'),
(291, 212, 1, 2, '100', '90', '40'),
(292, 213, 1, 2, '100', '90', '40'),
(293, 214, 1, 6, '300', '270', '120'),
(294, 215, 2, 1, '25', '22', '12'),
(295, 216, 2, 3, '75', '66', '36'),
(296, 217, 1, 5, '250', '225', '100'),
(297, 218, 2, 2, '50', '44', '24'),
(298, 218, 3, 10, '300', '250', '50'),
(299, 219, 2, 4, '100', '88', '48'),
(300, 219, 3, 3, '90', '75', '15'),
(301, 220, 4, 9, '45', '36', '9'),
(302, 220, 2, 5, '125', '110', '60'),
(303, 221, 1, 1, '50', '45', '20'),
(304, 222, 1, 1, '50', '45', '20'),
(305, 223, 1, 1, '11.25', '45', '20'),
(306, 224, 1, 1, '50', '45', '20'),
(307, 225, 1, 6, '67.5', '270', '120'),
(308, 226, 2, 1, '12', '22', '12'),
(309, 227, 2, 1, '12', '22', '12'),
(310, 228, 1, 1, '11.25', '45', '20'),
(311, 229, 4, 3, '18', '12', '3'),
(312, 229, 3, 2, '23', '50', '10'),
(313, 229, 2, 3, '36', '66', '36'),
(314, 230, 4, 4, '24', '16', '4'),
(315, 230, 1, 4, '45', '180', '80'),
(316, 230, 2, 1, '12', '22', '12'),
(317, 231, 3, 5, '57.5', '125', '25'),
(318, 232, 2, 5, '60', '110', '60'),
(319, 232, 3, 5, '57.5', '125', '25'),
(320, 233, 3, 100, '1150', '2500', '500'),
(321, 234, 5, 1, '7', '7', '7'),
(322, 234, 3, 11, '126.5', '275', '55'),
(323, 234, 1, 10, '112.5', '450', '200'),
(324, 235, 2, 6, '72', '132', '72'),
(325, 236, 1, 4, '45', '180', '80'),
(326, 237, 5, 3, '21', '21', '21'),
(327, 238, 2, 3, '36', '66', '36'),
(328, 239, 1, 5, '56.25', '225', '100'),
(329, 240, 1, 6, '67.5', '270', '120'),
(330, 241, 1, 100, '1125', '4500', '2000'),
(331, 241, 5, 100, '700', '700', '700'),
(332, 242, 1, 17, '191.25', '765', '340'),
(333, 243, 1, 1, '11.25', '45', '20'),
(334, 244, 1, 1, '11.25', '45', '20'),
(335, 245, 2, 4, '48', '88', '48'),
(336, 246, 2, 4, '48', '88', '48'),
(337, 247, 3, 4, '46', '100', '20'),
(338, 248, 1, 1, '11.25', '45', '20'),
(339, 249, 2, 125, '1500', '2750', '1500'),
(340, 249, 1, 150, '1687.5', '6750', '3000'),
(341, 250, 2, 1, '12', '22', '12'),
(342, 251, 1, 3, '33.75', '135', '60'),
(343, 252, 1, 8, '90', '360', '160'),
(344, 253, 1, 2, '22.5', '90', '40'),
(345, 253, 2, 3, '36', '66', '36'),
(346, 254, 4, 3, '18', '12', '3'),
(347, 254, 1, 5, '56.25', '225', '100'),
(348, 255, 1, 3, '33.75', '135', '60'),
(349, 256, 3, 3, '34.5', '75', '15'),
(350, 257, 2, 3, '36', '66', '36'),
(351, 257, 4, 3, '18', '12', '3'),
(352, 258, 1, 3, '33.75', '135', '60'),
(353, 259, 2, 2, '24', '44', '24'),
(354, 260, 4, 4, '24', '16', '4'),
(355, 261, 4, 2, '12', '8', '2'),
(356, 262, 2, 1, '12', '22', '12'),
(357, 263, 1, 1, '11.25', '45', '20'),
(358, 264, 2, 2, '24', '44', '24'),
(359, 265, 1, 2, '22.5', '90', '40'),
(360, 266, 3, 1, '11.5', '25', '5'),
(361, 267, 2, 3, '36', '66', '36'),
(362, 268, 4, 1, '6', '4', '1'),
(363, 269, 3, 2, '23', '50', '10'),
(364, 269, 5, 1, '7', '7', '7'),
(365, 269, 2, 1, '12', '22', '12'),
(366, 270, 1, 120, '1350', '5400', '2400'),
(367, 271, 1, 2, '22.5', '90', '40'),
(368, 272, 3, 1, '11.5', '25', '5'),
(369, 273, 1, 2, '22.5', '90', '40'),
(370, 274, 3, 4, '46', '100', '20'),
(371, 275, 1, 3, '33.75', '135', '60'),
(372, 275, 2, 4, '48', '88', '48'),
(373, 276, 2, 4, '48', '88', '48'),
(374, 276, 3, 3, '34.5', '75', '15'),
(375, 277, 1, 6, '67.5', '270', '120'),
(376, 278, 4, 1, '6', '4', '1'),
(377, 279, 1, 2, '22.5', '90', '40'),
(378, 280, 2, 10, '120', '220', '120'),
(379, 280, 3, 7, '80.5', '175', '35'),
(380, 282, 1, 1, '11.25', '45', '20'),
(381, 283, 1, 1, '11.25', '45', '20'),
(382, 284, 1, 1, '11.25', '45', '20'),
(383, 285, 1, 3, '33.75', '135', '60'),
(384, 285, 2, 3, '36', '66', '36'),
(385, 286, 1, 6, '67.5', '270', '120'),
(386, 287, 1, 3, '33.75', '135', '60'),
(387, 287, 2, 3, '36', '66', '36'),
(388, 288, 1, 6, '67.5', '270', '120'),
(389, 289, 1, 3, '33.75', '135', '60'),
(390, 289, 2, 3, '36', '66', '36'),
(391, 290, 1, 6, '67.5', '270', '120'),
(392, 291, 1, 3, '33.75', '135', '60'),
(393, 292, 1, 3, '33.75', '135', '60'),
(394, 293, 1, 3, '33.75', '135', '60'),
(395, 294, 1, 3, '33.75', '135', '60'),
(396, 295, 2, 3, '36', '66', '36'),
(397, 296, 1, 1, '11.25', '45', '20'),
(398, 297, 2, 10, '120', '220', '120'),
(399, 297, 3, 7, '80.5', '175', '35'),
(400, 298, 1, 6, '67.5', '270', '120'),
(401, 298, 2, 20, '240', '440', '240'),
(402, 299, 1, 1, '11.25', '45', '20'),
(403, 300, 1, 6, '67.5', '270', '120'),
(404, 300, 2, 20, '240', '440', '240'),
(405, 301, 4, 1, '11', '6', '1'),
(406, 302, 2, 4, '48', '88', '48'),
(407, 302, 3, 3, '34.5', '75', '15'),
(408, 303, 2, 50, '600', '1100', '600'),
(409, 303, 1, 50, '562.5', '2250', '1000'),
(410, 304, 1, 1, '11.25', '45', '20'),
(411, 305, 2, 5, '60', '110', '60'),
(412, 305, 1, 8, '90', '360', '160'),
(413, 306, 5, 1, '7', '7', '7'),
(414, 307, 3, 14, '161', '350', '70'),
(415, 307, 2, 10, '120', '220', '120'),
(416, 307, 1, 10, '112.5', '450', '200'),
(417, 307, 4, 10, '110', '60', '10'),
(418, 307, 5, 10, '70', '70', '70'),
(419, 307, 6, 10, '120', '120', '120'),
(420, 308, 6, 4, '48', '48', '48'),
(421, 308, 5, 4, '28', '28', '28'),
(422, 308, 3, 2, '23', '50', '10'),
(423, 308, 2, 5, '60', '110', '60'),
(424, 308, 1, 3, '33.75', '135', '60'),
(425, 309, 6, 100, '1200', '1200', '1200'),
(426, 309, 4, 24, '264', '144', '24'),
(427, 309, 3, 12, '138', '300', '60'),
(428, 309, 2, 6, '72', '132', '72'),
(429, 309, 1, 5, '56.25', '225', '100'),
(430, 310, 3, 10, '115', '250', '50'),
(431, 310, 1, 50, '562.5', '2250', '1000'),
(432, 311, 3, 10, '115', '250', '50'),
(433, 311, 1, 50, '562.5', '2250', '1000'),
(434, 312, 2, 6, '72', '132', '72'),
(435, 312, 3, 12, '138', '300', '60'),
(436, 313, 5, 200, '1400', '1400', '1400'),
(437, 314, 1, 19, '213.75', '855', '380'),
(438, 314, 5, 100, '700', '700', '700'),
(439, 314, 2, 100, '1200', '2200', '1200'),
(440, 315, 6, 1, '12', '12', '12'),
(441, 315, 2, 100, '1200', '2200', '1200'),
(442, 315, 1, 6, '67.5', '270', '120'),
(443, 316, 6, 1, '12', '12', '12'),
(444, 316, 2, 100, '1200', '2200', '1200'),
(445, 316, 1, 6, '67.5', '270', '120'),
(446, 317, 1, 1, '11.25', '45', '20'),
(447, 318, 1, 12, '135', '540', '240'),
(448, 319, 1, 12, '135', '540', '240'),
(449, 320, 1, 12, '135', '540', '240'),
(450, 321, 1, 1, '11.25', '45', '20'),
(451, 322, 1, 1, '11.25', '45', '20'),
(452, 323, 2, 1, '12', '22', '12'),
(453, 324, 2, 1, '12', '22', '12'),
(454, 325, 2, 3, '36', '66', '36'),
(455, 326, 6, 5, '60', '60', '60'),
(456, 326, 2, 1, '12', '22', '12'),
(457, 326, 5, 4, '28', '28', '28'),
(458, 327, 2, 3, '36', '66', '36'),
(459, 328, 1, 1, '11.25', '45', '20'),
(460, 328, 5, 4, '28', '28', '28'),
(461, 329, 1, 12, '135', '540', '240'),
(462, 329, 3, 9, '103.5', '225', '45'),
(463, 330, 1, 12, '135', '540', '240'),
(464, 330, 3, 9, '103.5', '225', '45'),
(465, 331, 1, 5, '56.25', '225', '100'),
(466, 332, 1, 6, '67.5', '270', '120'),
(467, 333, 1, 6, '67.5', '270', '120'),
(468, 334, 1, 1, '11.25', '45', '20'),
(469, 335, 3, 11, '126.5', '275', '55'),
(470, 335, 2, 41, '492', '902', '492'),
(471, 336, 1, 10, '112.5', '450', '200'),
(472, 336, 2, 10, '120', '220', '120'),
(473, 337, 1, 10, '112.5', '450', '200'),
(474, 337, 2, 10, '120', '220', '120'),
(475, 338, 2, 22, '264', '484', '264'),
(476, 338, 1, 7, '78.75', '315', '140'),
(477, 339, 2, 22, '264', '484', '264'),
(478, 339, 1, 7, '78.75', '315', '140'),
(479, 340, 5, 100, '700', '0', '0'),
(480, 341, 5, 1, '7', '0', '0'),
(481, 342, 15, 3, '27', '0', '0'),
(482, 343, 5, 2, '14', '0', '0'),
(483, 344, 7, 2, '22.5', '0', '0'),
(484, 345, 7, 3, '33.75', '0', '0'),
(485, 346, 7, 2, '22.5', '0', '0'),
(486, 346, 5, 3, '21', '0', '0'),
(487, 347, 5, 3, '21', '0', '0'),
(488, 348, 15, 5, '45', '0', '0'),
(489, 349, 5, 4, '28', '0', '0'),
(490, 349, 7, 1, '11.25', '0', '0'),
(491, 350, 7, 1, '11.25', '0', '0'),
(492, 351, 7, 1, '11.25', '0', '0'),
(493, 352, 8, 1, '12', '0', '0'),
(494, 353, 7, 1, '11.25', '0', '0'),
(495, 354, 7, 1, '11.25', '0', '0'),
(496, 355, 7, 1, '11.25', '0', '0'),
(497, 356, 8, 1, '12', '0', '0'),
(498, 357, 8, 1, '12', '0', '0'),
(499, 358, 8, 1, '12', '0', '0'),
(500, 359, 7, 1, '11.25', '0', '0'),
(501, 360, 8, 1, '12', '0', '0'),
(502, 361, 7, 1, '11.25', '0', '0'),
(503, 362, 7, 1, '11.25', '0', '0'),
(504, 363, 8, 3, '36', '0', '0'),
(505, 364, 7, 19, '213.75', '0', '0'),
(506, 364, 5, 4, '28', '0', '0'),
(507, 365, 8, 5, '60', '0', '0'),
(508, 366, 15, 12, '108', '0', '0'),
(509, 367, 15, 1, '9', '0', '0'),
(510, 368, 14, 1, '11.5', '0', '0'),
(511, 369, 15, 5, '45', '0', '0'),
(512, 370, 4, 3, '0', '0', '0'),
(513, 370, 5, 3, '21', '0', '0'),
(514, 371, 5, 5, '35', '0', '0'),
(515, 372, 8, 8, '96', '0', '0'),
(516, 372, 14, 1, '11.5', '0', '0'),
(517, 373, 5, 6, '42', '0', '0'),
(518, 374, 5, 2, '14', '0', '0'),
(519, 375, 13, 3, '34.5', '0', '0'),
(520, 376, 8, 9, '108', '0', '0'),
(521, 376, 13, 11, '126.5', '0', '0'),
(522, 377, 5, 1, '7', '0', '0'),
(523, 378, 5, 1, '7', '0', '0'),
(524, 379, 15, 2, '18', '0', '0'),
(525, 379, 5, 1, '7', '0', '0'),
(526, 380, 5, 4, '28', '0', '0'),
(527, 381, 8, 8, '96', '0', '0'),
(528, 381, 7, 1, '11.25', '0', '0'),
(529, 381, 13, 8, '92', '0', '0'),
(530, 381, 15, 7, '63', '0', '0'),
(531, 382, 15, 6, '54', '0', '0'),
(532, 383, 13, 5, '57.5', '0', '0'),
(533, 383, 5, 8, '56', '0', '0'),
(534, 384, 14, 10, '115', '0', '0'),
(535, 384, 13, 10, '115', '0', '0'),
(536, 384, 8, 5, '60', '0', '0'),
(537, 385, 7, 4, '45', '0', '0'),
(538, 386, 5, 1, '7', '0', '0'),
(539, 386, 7, 18, '202.5', '0', '0'),
(540, 387, 13, 6, '69', '0', '0'),
(541, 387, 5, 6, '42', '0', '0'),
(542, 387, 7, 1, '11.25', '0', '0'),
(543, 388, 13, 20, '230', '0', '0'),
(544, 389, 15, 7, '63', '0', '0'),
(545, 389, 5, 9, '63', '0', '0'),
(546, 390, 15, 50, '450', '0', '0'),
(547, 390, 7, 20, '225', '0', '0'),
(548, 390, 5, 100, '700', '0', '0'),
(549, 391, 5, 15, '105', '0', '0'),
(550, 392, 8, 13, '156', '0', '0'),
(551, 393, 5, 14, '98', '0', '0'),
(552, 394, 5, 12, '84', '0', '0'),
(553, 395, 5, 5, '35', '0', '0'),
(554, 400, 5, 3, '21', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `promocode`
--

CREATE TABLE `promocode` (
  `id` int(15) NOT NULL,
  `title` varchar(100) NOT NULL,
  `promo_code` varchar(100) NOT NULL,
  `type` varchar(5) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `pids` varchar(45) NOT NULL DEFAULT '0',
  `description` varchar(250) NOT NULL,
  `expiry_date` date NOT NULL,
  `product` varchar(255) NOT NULL,
  `users` text NOT NULL,
  `created_by` int(15) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promocode`
--

INSERT INTO `promocode` (`id`, `title`, `promo_code`, `type`, `discount`, `pids`, `description`, `expiry_date`, `product`, `users`, `created_by`, `created_at`) VALUES
(1, '50% cashback', 'MAIRAK201', 'F', '50.00', '0', 'Twenty Liter water with 50% cashback', '2019-10-10', '', '153', 1, '2019-01-15 14:38:08'),
(2, '25% cashback', 'MAIRAK211', 'F', '5.00', '0', 'Ten Liter water can with 25% cashback', '2022-09-28', '', '', 1, '2018-09-14 13:12:52'),
(3, 'test', 'MRC15', 'F', '15.00', '0', 'flat 15 off', '2018-12-31', '', '', 1, '2018-12-05 16:38:17'),
(7, 'Test 2', 'MRC30', 'F', '30.00', '3', 'MRC30', '2019-01-31', '', '176', 1, '2019-01-07 13:28:07'),
(8, 'Test3', 'MRC40', 'P', '10.00', '1', 'MRC40', '2019-01-31', '1,2,3,4,5,6', '103', 1, '2019-01-08 22:14:43'),
(9, 'FREEZONE USER', 'MRFZE', 'P', '55.00', '0', 'Use this code for grab your special attractive price.', '2019-01-31', '15', '177,180,187', 1, '2019-01-26 14:01:11'),
(10, 'TestIOS', 'IOSMAI123', 'P', '60.00', '0', 'testtt', '2019-02-20', '5,7,8', '153', 1, '2019-02-04 15:09:31'),
(11, 'IOSTEST', 'IOSTEST121', 'P', '50.00', '0', 'ghfdh', '2019-02-27', '5,7,13,14', '153', 1, '2019-02-04 15:26:11'),
(13, 'IOSTEST2', 'IOS_002', 'F', '10.00', '0', 'ryy', '2019-04-04', '5,7,8,13,14,15', '215,208,198,199,95,177,180,205,200,26,167,171,153,210,211,212,203,204,206,209,217,202,207,214,216,197,186,51,176,31,50,179,213,181,196,222,201,221,223,116,159,187,117,218,219,220', 1, '2019-04-03 14:16:16'),
(14, 'IOSTEST3', 'IOS_003', 'F', '20.00', '0', 'test', '2019-04-04', '5,7,8,13,14,15', '215,208,198,199,95,177,180,205,200,26,167,171,153,210,211,212,203,204,206,209,217,202,207,214,216,197,186,51,176,31,50,179,213,181,196,222,201,221,223,116,159,187,117,218,219,220', 1, '2019-04-03 15:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `promocode_order`
--

CREATE TABLE `promocode_order` (
  `id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `promo_code` varchar(100) NOT NULL,
  `latitude` varchar(200) NOT NULL,
  `longitude` varchar(200) NOT NULL,
  `ime_number` varchar(200) NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promocode_order`
--

INSERT INTO `promocode_order` (`id`, `user_id`, `promo_code`, `latitude`, `longitude`, `ime_number`, `ip_address`, `created_at`) VALUES
(22, 146, 'MRC40', '0.0', '0.0', '1234', '59.95.75.185', '2019-01-02'),
(23, 175, 'MRC40', '25.7911317', '55.9436579', '358461076983388', '31.215.181.177', '2019-01-02'),
(24, 175, 'MRC30', '25.7911389', '55.9435989', '358461076983388', '31.215.181.177', '2019-01-02'),
(25, 170, 'MRC40', '25.7896894', '55.9416008', '860648037294365', '87.201.182.218', '2019-01-03'),
(26, 170, 'MRC30', '25.7909316', '55.9475179', '860648037294365', '94.206.152.57', '2019-01-03'),
(27, 170, 'MAIRAK201', '8.5393925', '76.9454739', '357488083186730', '137.97.146.219', '2019-01-03'),
(28, 165, 'MRC40', '25.7784204', '55.9677087', '352152098236402', '5.31.175.213', '2019-01-05'),
(29, 165, 'MRC30', '25.7782325', '55.969355', '352152098236402', '5.31.175.213', '2019-01-05'),
(30, 165, 'MAIRAK201', '25.7782325', '55.969355', '352152098236402', '5.31.175.213', '2019-01-05'),
(31, 176, 'MRC40', '5.012222', '3.22222', '435435345345345', '202.88.236.44', '2019-01-07'),
(32, 176, 'MRC30', '5.012222', '3.22222', '435435345345345', '202.88.236.44', '2019-01-12'),
(33, 176, 'MAIRAK211', '5.012222', '3.22222', '435435345345345', '202.88.236.44', '2019-01-12'),
(34, 177, 'MRNEW', '25.7893648', '55.9416008', '860648037294365', '5.31.183.246', '2019-01-15'),
(35, 153, 'MAIRAK201', '8.5193653', '76.9599177', '865980024704709', '59.95.73.197', '2019-01-15'),
(36, 153, 'MAIRAK201', '8.5193652', '76.9599178', '865980024704709', '59.95.73.197', '2019-01-15'),
(37, 153, 'MAIRAK201', '8.5193652', '76.9599177', '865980024704709', '59.95.73.197', '2019-01-15'),
(38, 153, 'MAIRAK201', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(39, 153, 'MAIRAK201', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(40, 153, 'MAIRAK201', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(41, 153, 'MAIRAK201', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(42, 153, 'MAIRAK201', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(43, 177, 'MRNEW', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(44, 177, 'MRNEW', '0.0', '0.0', '1234', '59.95.73.197', '2019-01-15'),
(45, 177, 'MRNEW', '25.7893648', '55.9416008', '860648037294365', '5.31.183.246', '2019-01-15'),
(46, 177, 'MRNEW', '25.7893648', '55.9416008', '860648037294365', '5.31.183.246', '2019-01-15'),
(47, 177, 'MRNEW', '25.7893648', '55.9416008', '860648037294365', '5.31.183.246', '2019-01-15'),
(48, 177, 'MRNEW', '25.7893648', '55.9416008', '860648037294365', '5.31.183.246', '2019-01-15'),
(49, 177, 'MRNEW', '25.7924703', '55.9422969', '860648037294365', '5.31.183.246', '2019-01-15'),
(50, 177, 'MRNEW', '25.7924703', '55.9422969', '860648037294365', '5.31.183.246', '2019-01-15'),
(51, 177, 'MRNEW', '25.7924703', '55.9422969', '860648037294365', '5.31.183.246', '2019-01-15'),
(52, 177, 'MRNEW', '25.7924703', '55.9422969', '860648037294365', '5.31.183.246', '2019-01-15'),
(53, 180, 'MRFZE', '25.7901542', '55.9409047', '860648037294365', '87.201.186.239', '2019-01-26'),
(54, 180, 'MRFZE', '25.7908033', '55.9409047', '860648037294365', '87.201.186.239', '2019-01-26'),
(55, 153, 'IOSTEST121', '8.5193684', '76.9599183', 'e', '111.92.27.128', '2019-02-04'),
(56, 153, 'IOSMAI123', '8.5193721', '76.9599237', 'e', '111.92.27.200', '2019-02-05'),
(57, 153, 'IOS_002', '8.560400898499564', '76.88055171097142', 'Unknown', '202.88.237.221', '2019-04-03');

-- --------------------------------------------------------

--
-- Table structure for table `pushnotification`
--

CREATE TABLE `pushnotification` (
  `id` int(15) NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` varchar(250) NOT NULL,
  `fcmlog` longtext NOT NULL,
  `created_by` int(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `status` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pushnotification`
--

INSERT INTO `pushnotification` (`id`, `title`, `message`, `fcmlog`, `created_by`, `created_at`, `status`) VALUES
(6, 'test fcm', 'test fcm  10-12-2018', '', 1, '2018-12-10 16:10:35', 1),
(7, 'test2', 'test', '', 1, '2018-12-10 16:46:02', 0),
(8, 'Test 2', '44444444', '', 1, '2018-12-10 16:49:28', 0),
(9, 'test', 'yyyyyyyyyyyyy', '', 1, '2018-12-10 16:50:23', 0),
(10, 'test', 'ffffffffffffffff', '', 1, '2018-12-10 16:51:59', 0),
(11, 'test', 'ffffffffffffffff', '', 1, '2018-12-10 16:53:14', 0),
(12, 'test', 'ffffffffffffffff', '', 1, '2018-12-10 16:54:38', 0),
(13, 'test', 'trrtrttrtrtrtr', '', 1, '2018-12-10 16:57:03', 0),
(14, 'testffffffffff', 'rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr', '', 1, '2018-12-10 16:58:29', 0),
(15, 'test', 'test', '', 1, '2018-12-19 00:58:03', 0),
(16, 'Again testing Notification', 'Checking Push Notification System', '', 1, '2018-12-19 00:58:26', 0),
(17, 'tesrt', 'test', '', 1, '2018-12-19 12:13:36', 0),
(18, 'TEsting Push', 'TEsting Puch', '', 1, '2018-12-20 15:27:37', 0),
(19, 'tested', 'testss', '', 1, '2018-12-20 15:28:20', 0),
(20, 'test', 'test', '', 1, '2019-01-03 12:28:35', 0),
(21, 'hi ', 'happy new year', '', 1, '2019-01-03 18:15:49', 0),
(22, 'new year', 'happy new year', '', 1, '2019-01-03 18:16:07', 0),
(23, 'new year', 'happy new year', '', 1, '2019-01-03 18:16:38', 0),
(29, 'New Year', 'Happy new Year', '', 1, '2019-01-04 11:44:54', 0),
(30, 'Happy New Year To Ajmal', 'Happy New Year to Ajmal', '', 1, '2019-01-05 22:20:58', 1),
(31, 'Happy New Year To Ajmal', 'Happy New Year To Ajmal', '', 1, '2019-01-06 00:03:52', 1),
(34, 'happy  new year', 'happy new year', '', 1, '2019-01-08 22:16:17', 1),
(35, 'test', 'tester', '', 1, '2019-01-10 16:54:15', 1),
(36, 'Welcome New User', 'us this promo code for discounts', '', 1, '2019-01-15 14:20:20', 1),
(37, 'Test', 'Tessttt', '', 1, '2019-01-15 14:34:30', 1),
(38, 'neew Test', 'Ths is a test', '', 1, '2019-01-15 14:46:56', 0),
(39, 'welcome to mai rk', 'claim u r botles', '', 1, '2019-01-16 17:00:12', 0),
(43, 'Test', 'Test message', '', 1, '2019-01-17 13:33:24', 1),
(44, 'Message', 'Message', '', 1, '2019-01-17 13:34:00', 1),
(45, 'Tester', 'test message', '', 1, '2019-01-17 15:32:42', 1),
(46, 'Testttt', 'this is a testeer', '', 1, '2019-01-18 16:30:19', 1),
(47, 'Welcome New User', 'welcome', '', 1, '2019-01-19 15:12:09', 1),
(48, '50% cashback', 'get 50%', '', 1, '2019-01-19 15:14:52', 1),
(49, 'test', 'tester', '', 1, '2019-01-21 11:01:43', 1),
(50, 'Testing', 'Test Notification', '', 1, '2019-01-21 11:02:39', 1),
(51, 'Welcome', 'Welcome message', '', 1, '2019-01-21 11:03:15', 1),
(52, 'Cashback', '50% cashback for 5l can', '', 1, '2019-01-21 11:29:31', 1),
(53, '10% cashback', '10% cashback for 5l can', '', 1, '2019-01-21 11:29:56', 1),
(54, '20% cashback', '20% cashback for 20l can', '', 1, '2019-01-21 11:30:19', 1),
(55, '30% cashback', '30% cashback for 5l can', '', 1, '2019-01-21 11:30:43', 1),
(56, '40% cashback', '40% cashback for 5l can', '', 1, '2019-01-21 11:31:07', 1),
(57, 'neew Test', 'tesrtrrrrrrr', '', 1, '2019-01-21 14:14:27', 0),
(58, '20% eXTRA off', '20% off on all products', '', 1, '2019-01-21 14:15:29', 1),
(59, '50% off ', 'Products with discount', '', 1, '2019-01-21 14:18:18', 1),
(60, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'd7jPRyU_gMY:APA91bGdP6mCFjxhS7x0dezrU7577a89c37qdN3ZyUAkSmWGLzQvm6AB-y489IFB07kgjq5DvIeRgNlP3zjbMyz5QX9DhrFbiGXyAc07BNhI7lBaYHyXMIYjKroTWck0E9ksziGGyMWQ', 176, '2019-01-21 16:24:20', 1),
(61, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'd7jPRyU_gMY:APA91bGdP6mCFjxhS7x0dezrU7577a89c37qdN3ZyUAkSmWGLzQvm6AB-y489IFB07kgjq5DvIeRgNlP3zjbMyz5QX9DhrFbiGXyAc07BNhI7lBaYHyXMIYjKroTWck0E9ksziGGyMWQ', 176, '2019-01-21 16:25:10', 1),
(62, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'cOOCUUagBp8:APA91bFBsKAOJ_u5N5Ap8qdxgezg4JKirqGLBa73W1GOxuTNBuVqm9tBT5UbgaMTdIXIctnayarSdBvf5hkxG5mPuX00pBvrICZY8EfeVbFe3-vq4bBaLzig7uYZPqOPx02ukdzBy5f1', 182, '2019-01-21 16:36:50', 1),
(63, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'fpgN_dZ-CBw:APA91bG33Uuj3U6_SJc1uDRZjfC8JrmSoiU-GxWCxsf_IVu9f9UaKRngFYwymZRGhwykuJJ69rADso6JHiFRk1mCQ_c6S_cm_zMkqW1Vof5ozTTZQA5lpvbrSoi-IQcgMrJi7GmMvNPB', 184, '2019-01-22 12:53:42', 1),
(64, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'f-vkxObW5uY:APA91bFIC1DxPu6uHe8Mr7LqBAaHzQYKB1Oz2VY9pTKCIU_PRceFtYN0U38iL6J8rvpqf3m06TRGvDvF1VxdoWpIHIoQ-hiXfqJpJIZCtyhyK7wfDTTO5zj3kBaF4bzu2MxWHwID4ikV', 186, '2019-01-23 12:23:03', 1),
(65, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'c1LHJR9G7rE:APA91bEqc4F9qZFGw0QcP50Q2zuSLwX97lyEeFuPDOt3AFaILlNi5DClhAuDsOEynjhISM1Z7QQb2UJjZcNMKyjQ8NqQ4Ez-GdXgBbY5v9pAPYjOrjOdH_0wlmicr1-cSzf_Dl88o-mG', 187, '2019-01-23 12:53:03', 1),
(66, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'ddiTN-aOO-U:APA91bGwN3kmsSbfmiYXrlDlDczEu3tL4OEtK8WGx79C2ClWPywV2k5AoKV2mAyFlvs5DlOMfp1eKrgQaC_2uhLVPYAieRFysoD2nhOmAMoWfR5c6D0tbf4BG6RtH5NiZuZ9jOVc9fIW', 196, '2019-01-26 11:26:49', 1),
(67, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'c0_feR4j_sQ:APA91bEhZqi1_WU0zWVxxbQnUxdspIfLuUpx_A_ZU7Bk1zrB7xUGgtEAAZft8sAT9ui258fdkjG3x5D35tZoRORd1oAauUZtYmJJmiz1DeoB2dlZQO5CuB4PdTkBzIShTAZ6rCaztWpV', 197, '2019-01-26 15:37:20', 1),
(68, 'Welcome TEsting', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', '', 1, '2019-01-29 18:16:15', 1),
(69, 'Welcome', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', '', 1, '2019-02-01 12:33:42', 1),
(70, 'This is a Test', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', '', 1, '2019-02-01 12:33:57', 1),
(71, 'Testing read', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', '', 1, '2019-02-01 13:47:59', 1),
(72, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'fMxKDRwP0Y4:APA91bH_MBtl9te-S8Iyd_pjnYc7CIAAZIMWPn4RrCEovfcd5FHRZsU9Wcu7zthAQFngdv6XE7D1-OH7Sz3-tR6kOFX-djgSjX4_yU3jxB2FvNr4lWHHy4bgsNL7buF-tzN9xyb8Ys9H', 200, '2019-02-05 19:29:18', 1),
(73, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'fLoiHLJT7r8:APA91bHyssTkyUeIjk9C1IFb9XngDDW7RQNQgumrESV6cfO1xGCb995ySF6LY42b0sAXazNXpImHeBasPa7sLaWHlJU5vjlkb1TLVwCw2mRgYSHkH1E3V-qqNZZuEVoQ3SG1to45owOG', 201, '2019-02-05 19:40:48', 1),
(74, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'd16ntmWTwbo:APA91bFhFmfgNc4a1mc82ikpCd3CtyCckcfzbFpT-RM46aRQI7U_2wMh5t3-s9Y4dzXAx1iiACfedi4A-9L6IEAQgFBBVdxhhE3qvV1dlWiEmVlvMKRHbM4xQmJZsyq7RBOBlwd-MwvC', 202, '2019-02-05 19:58:57', 1),
(75, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'e88hf0-xAGY:APA91bFrb1OAp22DARy6svwYGZD_E6pAdTOu0o9r0F0IrH-aIfqQqeEKke7lvFozTbvK85wl56W_CN1c8myE6zbNw0HTtFEVUocrjfXrW_i70sxMSlKyry0inA3jKZdrQFFafQBVsRKO', 205, '2019-02-07 14:06:31', 1),
(76, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'dXdkCKJmPvk:APA91bG_kvAXcqD-Lo8j55TDZmKZCD83VSk4609EW0Vi3mGOfzr8yHL1sWdz_YBiMEgMZfEiSXDt2DEQ7ar4MB-6xiZ2KQqR3i9vCzfV4cTRbjOeT8JMaRHOshAIkvuK9257Kx80ZpJE', 206, '2019-02-13 16:25:08', 1),
(77, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'dm_ZMJ_LLuk:APA91bG3j92Glyqo5RtmQocRVY2LqW1VxpiIsiEuqorKz3sRxC7vm_INFvnpgyM1N7kH_hPzfP9_SWJul-XDz1cYxaH5yPNUS2VjHndQp9ocZUZ1tItSYd_B59lvJfDEopf8YIRu4DFD', 207, '2019-02-14 13:42:50', 1),
(78, 'This is a test', 'Test ', '', 1, '2019-02-19 11:35:12', 1),
(79, 'dsg', 'dgsg', '', 1, '2019-02-19 11:40:45', 1),
(80, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'ekdjwZwhQK8:APA91bFgk4LBkOW742QT311RVQil_4aQB5h7_4J6my5TYBwptinxGDAiY3v-81PebNpyh9xlJIdbaNNVK5v3aKL6QQ2dSpiDpglK2YL2wPQaXS-ohI7ZZyrCw9OIWAI3UQlYqbSrdj4u', 209, '2019-02-24 16:32:46', 1),
(81, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'eLm65QQhdNc:APA91bEkCW6Cz7AeAfylATnEJNSef8950THkKrW8Wgj93gdUP9xSmYLvaGJJc9uLwfIAJI4DytKxIONCnKQSlPm6vRi7oKvO4S4Rt58wdoJb0bSCQcQwcRwtyaCF_ejWaHqjrT_pa9Rd', 213, '2019-03-07 03:44:37', 1),
(82, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'dkVt10vUuZA:APA91bGV8u72Dy_2dINHZmfg0Sv92dxHOFgN2E5DXJIy_8lMdcELAHwe2JeRZycDM2gbHTq1Q1vQxwWj785UvFs49VahDPtO5BB4gkBJK21sVddesC6myfrj9fCTWN3jonwfEPhDJ7SP', 215, '2019-03-09 11:40:30', 1),
(83, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'fTci27KFJLA:APA91bF5Jc5a7RxrLHAwT4VcJvIsh049Tk88jCGLCgek68EIaxDewcrd7NSsLjY1KlNFL9zbdSqfS8_vVLA0IcVbt5V6sf-S0UsLwWalPMkvEoIHVNajra8ChDMBjHS3lhDgITJVgN8e', 217, '2019-03-24 20:52:16', 1),
(84, 'This is a test', 'tghissssss', '', 1, '2019-04-02 12:03:56', 1),
(85, 'eewe', 'ewet', '', 1, '2019-04-02 12:21:28', 1),
(86, 'Welcome Mai Rak', 'Thank You for Registering with Mai Rak. Please share with 5 users to avail Exciting offers from Mai Rak', 'cUm5__WoR2M:APA91bHIQ2e8BST5mX7KmAXaKqMMt8YaYH6QP0pkbgsEl6S75TEgji5xjL6tLg3s0oH00LHnw9ophqYErS3qJFEyVsa3eDBQYPmzhvvqWVWbn4wGLk3ewVBOSu16vmh5k2XicBPJflRu', 228, '2019-05-21 06:32:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `push_user`
--

CREATE TABLE `push_user` (
  `pushuser_id` int(15) NOT NULL,
  `push_id` int(15) NOT NULL,
  `user_id` int(15) NOT NULL,
  `push_status` int(15) NOT NULL,
  `read_status` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_user`
--

INSERT INTO `push_user` (`pushuser_id`, `push_id`, `user_id`, `push_status`, `read_status`) VALUES
(1, 43, 51, 1, 0),
(2, 43, 153, 0, 0),
(3, 43, 178, 1, 0),
(4, 44, 153, 0, 0),
(5, 44, 154, 1, 0),
(6, 44, 51, 0, 0),
(7, 45, 153, 0, 0),
(8, 46, 153, 0, 0),
(9, 47, 180, 1, 1),
(10, 48, 180, 0, 1),
(11, 49, 153, 0, 1),
(12, 50, 153, 0, 1),
(13, 51, 153, 0, 1),
(14, 52, 153, 0, 1),
(15, 53, 153, 0, 1),
(16, 54, 153, 0, 1),
(17, 55, 153, 0, 1),
(18, 56, 153, 0, 1),
(19, 57, 153, 1, 1),
(20, 58, 153, 0, 1),
(21, 59, 153, 0, 1),
(22, 60, 176, 1, 0),
(23, 61, 176, 1, 0),
(24, 62, 182, 0, 1),
(25, 63, 184, 1, 1),
(26, 64, 186, 0, 1),
(27, 65, 187, 1, 0),
(28, 66, 196, 1, 1),
(29, 67, 197, 1, 1),
(30, 68, 153, 0, 1),
(31, 69, 153, 0, 1),
(32, 70, 153, 0, 1),
(33, 71, 153, 0, 1),
(34, 72, 200, 1, 1),
(35, 73, 201, 1, 0),
(36, 74, 202, 1, 1),
(37, 75, 205, 1, 1),
(38, 76, 206, 1, 0),
(39, 77, 207, 1, 0),
(40, 78, 153, 0, 0),
(41, 79, 153, 0, 1),
(42, 80, 209, 0, 1),
(43, 81, 213, 1, 1),
(44, 82, 215, 1, 0),
(45, 83, 217, 1, 0),
(46, 84, 153, 0, 1),
(47, 85, 153, 0, 1),
(48, 86, 228, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 1, 1, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(2, 'User', 0, 2, '2017-02-16 18:14:45', '2017-02-16 18:14:45'),
(3, 'Employees', 0, 0, '2017-11-07 18:19:25', '2017-11-07 18:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_sheet_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `driver` int(11) NOT NULL,
  `helper` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `agent_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `driver_commission` int(11) NOT NULL,
  `helper_commission` int(11) NOT NULL,
  `agent_commission` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `payment_collected` int(11) NOT NULL DEFAULT '0',
  `credit` int(11) NOT NULL DEFAULT '0',
  `payment_completed` tinyint(1) NOT NULL,
  `sale_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `stakeholder_commission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `order_sheet_no`, `customer_id`, `driver`, `helper`, `agent_id`, `agent_type`, `driver_commission`, `helper_commission`, `agent_commission`, `quantity`, `amount`, `discount`, `vehicle_id`, `date`, `payment_collected`, `credit`, `payment_completed`, `sale_status`, `created_at`, `updated_at`, `deleted_at`, `stakeholder_commission`) VALUES
(1, '1234', 1, 1, 2, 0, '', 10, 10, 0, 0, 100, 10, 1, '2017-09-07 00:00:00', 0, 0, 0, 1, '2017-09-07 15:35:23', '2017-11-25 15:33:07', '2017-11-25 15:33:07', 0),
(2, '11', 1, 1, 2, 0, '', 0, 0, 0, 0, 200, 10, 1, '2017-09-07 00:00:00', 0, 0, 0, 0, '2017-09-07 15:50:19', '2017-11-05 08:38:21', '2017-11-05 08:38:21', 0),
(3, '1123', 1, 1, 2, 0, '', 0, 0, 0, 0, 250, 0, 1, '2017-09-13 00:00:00', 0, 0, 0, 1, '2017-09-12 20:50:49', '2017-11-25 15:33:07', '2017-11-25 15:33:07', 0),
(4, '145', 1, 1, 2, 0, '', 0, 0, 0, 0, 340, 0, 3, '2017-09-13 00:00:00', 0, 0, 0, 1, '2017-09-12 21:03:51', '2017-11-25 15:33:07', '2017-11-25 15:33:07', 0),
(5, '12345', 2, 1, 2, 0, '', 0, 0, 0, 0, 150, 10, 1, '2017-11-05 00:00:00', 0, 0, 1, 1, '2017-11-05 08:53:08', '2017-11-28 13:57:20', NULL, 0),
(6, '1232', 3, 1, 2, 0, '', 0, 0, 0, 0, 220, 0, 1, '2017-11-05 00:00:00', 0, 0, 0, 1, '2017-11-05 08:58:58', '2017-11-11 15:52:57', NULL, 0),
(7, '145', 4, 1, 2, 0, '', 0, 0, 0, 0, 10, 0, 1, '2017-11-05 00:00:00', 0, 0, 0, 1, '2017-11-05 09:11:30', '2017-11-11 15:53:01', NULL, 0),
(8, '409', 8, 1, 2, 0, '', 0, 0, 0, 0, 350, 12, 1, '2017-11-08 00:00:00', 0, 0, 0, 1, '2017-11-08 12:14:11', '2017-11-11 15:53:06', NULL, 0),
(9, '', 25, 1, 2, 0, '', 0, 0, 0, 0, 80, 0, 1, '2017-11-11 00:00:00', 0, 0, 0, 1, '2017-11-11 15:50:26', '2017-11-11 15:51:41', NULL, 0),
(10, '', 2, 1, 2, 0, '', 0, 0, 0, 0, 40, 0, 1, '2017-11-12 00:00:00', 0, 0, 0, 0, '2017-11-12 14:48:10', '2017-11-12 17:09:07', '2017-11-12 17:09:07', 0),
(11, '121', 6, 1, 2, 0, '', 0, 0, 0, 0, 600, 0, 1, '2017-12-08 00:00:00', 0, 0, 0, 1, '2017-12-08 08:19:19', '2018-01-10 14:42:27', NULL, 0),
(12, '111', 2, 1, 2, 0, '', 0, 0, 0, 0, 60, 0, 1, '2017-12-13 00:00:00', 0, 0, 1, 0, '2017-12-13 17:38:25', '2017-12-13 17:41:19', NULL, 0),
(13, '123', 12, 1, 2, 0, '', 0, 0, 0, 0, 160, 0, 1, '2018-01-10 00:00:00', 0, 0, 1, 1, '2018-01-10 14:39:13', '2018-01-10 14:40:56', NULL, 0),
(14, '12', 6, 1, 2, 0, '', 0, 0, 0, 0, 500, 0, 2, '2017-08-25 00:00:00', 0, 0, 0, 0, '2018-01-10 15:15:20', '2018-01-10 15:15:20', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sales_details`
--

CREATE TABLE `sales_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `free_quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `out_stock_details_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_details`
--

INSERT INTO `sales_details` (`id`, `sale_id`, `category_id`, `quantity`, `amount`, `free_quantity`, `created_at`, `updated_at`, `deleted_at`, `out_stock_details_id`) VALUES
(1, 2, 1, 17, 200, 0, '2017-09-07 15:50:19', '2017-11-05 08:38:21', '2017-11-05 08:38:21', 33),
(2, 3, 1, 8, 100, 0, '2017-09-12 20:50:49', '2017-09-12 20:50:49', NULL, 33),
(3, 4, 1, 15, 200, 0, '2017-09-12 21:03:51', '2017-09-12 21:06:17', '2017-09-12 21:06:17', 33),
(4, 4, 2, 12, 140, 0, '2017-09-12 21:03:51', '2017-09-12 21:06:17', '2017-09-12 21:06:17', 34),
(5, 4, 1, 15, 200, 0, '2017-09-12 21:06:17', '2017-09-12 21:06:17', NULL, 0),
(6, 4, 2, 12, 140, 0, '2017-09-12 21:06:17', '2017-09-12 21:06:17', NULL, 0),
(7, 5, 1, 15, 150, 0, '2017-11-05 08:53:08', '2017-11-05 08:53:08', NULL, 38),
(8, 6, 1, 22, 220, 0, '2017-11-05 08:58:58', '2017-11-05 08:58:58', NULL, 38),
(9, 7, 1, 1, 10, 0, '2017-11-05 09:11:30', '2017-11-05 09:11:30', NULL, 39),
(10, 8, 1, 35, 350, 0, '2017-11-08 12:14:11', '2017-11-08 12:14:11', NULL, 42),
(11, 9, 1, 10, 80, 0, '2017-11-11 15:50:26', '2017-11-11 15:50:26', NULL, 42),
(12, 10, 1, 4, 40, 0, '2017-11-12 14:48:10', '2017-11-12 17:09:07', '2017-11-12 17:09:07', 42),
(13, 11, 1, 50, 600, 0, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL, 42),
(14, 12, 1, 1, 60, 1, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL, 42),
(15, 13, 1, 20, 160, 0, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL, 42);

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

CREATE TABLE `sales_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `credit_payment_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`id`, `sale_id`, `amount`, `credit_payment_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 20, 0, '2017-09-11 17:00:10', '2017-09-11 17:00:10', NULL),
(2, 2, 30, 0, '2017-09-11 17:46:00', '2017-09-11 17:46:00', NULL),
(3, 3, 100, 0, '2017-09-12 20:52:54', '2017-09-12 20:52:54', NULL),
(4, 3, 150, 0, '2017-09-12 20:53:59', '2017-09-12 20:53:59', NULL),
(5, 4, 120, 0, '2017-09-12 21:04:21', '2017-09-12 21:04:21', NULL),
(6, 2, 50, 0, '2017-10-22 06:00:00', '2017-10-22 06:00:00', NULL),
(7, 2, 100, 0, '2017-10-22 06:02:59', '2017-10-22 06:02:59', NULL),
(8, 5, 120, 0, '2017-11-28 13:54:22', '2017-11-28 13:54:22', NULL),
(9, 5, 30, 0, '2017-11-28 13:57:20', '2017-11-28 13:57:20', NULL),
(10, 11, 110, 0, '2017-12-08 08:46:53', '2017-12-08 08:46:53', NULL),
(11, 11, 40, 0, '2017-12-08 08:47:53', '2017-12-08 08:47:53', NULL),
(12, 11, 10, 0, '2017-12-13 17:40:12', '2017-12-13 17:40:12', NULL),
(13, 12, 10, 0, '2017-12-13 17:40:35', '2017-12-13 17:40:35', NULL),
(14, 12, 50, 2, '2017-12-13 17:41:19', '2017-12-13 17:41:19', NULL),
(15, 13, 0, 0, '2018-01-10 14:39:38', '2018-01-10 14:39:38', NULL),
(16, 11, 200, 6, '2018-01-10 14:46:15', '2018-01-10 14:46:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sale_detail_outstock_details`
--

CREATE TABLE `sale_detail_outstock_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `sale_detail_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `outstock_details_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sale_detail_outstock_details`
--

INSERT INTO `sale_detail_outstock_details` (`id`, `sale_detail_id`, `quantity`, `outstock_details_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 15, 37, '2017-11-05 08:53:08', '2017-11-05 08:53:08', NULL),
(2, 8, 10, 37, '2017-11-05 08:58:58', '2017-11-05 08:58:58', NULL),
(3, 8, 12, 38, '2017-11-05 08:58:58', '2017-11-05 08:58:58', NULL),
(4, 9, 0, 37, '2017-11-05 09:11:30', '2017-11-05 09:11:30', NULL),
(5, 9, 1, 38, '2017-11-05 09:11:30', '2017-11-05 09:11:30', NULL),
(6, 10, 0, 37, '2017-11-08 12:14:11', '2017-11-08 12:14:11', NULL),
(7, 10, 0, 38, '2017-11-08 12:14:11', '2017-11-08 12:14:11', NULL),
(8, 10, 30, 39, '2017-11-08 12:14:11', '2017-11-08 12:14:11', NULL),
(9, 10, 5, 40, '2017-11-08 12:14:11', '2017-11-08 12:14:11', NULL),
(10, 11, 0, 37, '2017-11-11 15:50:26', '2017-11-11 15:50:26', NULL),
(11, 11, 0, 38, '2017-11-11 15:50:26', '2017-11-11 15:50:26', NULL),
(12, 11, 0, 39, '2017-11-11 15:50:26', '2017-11-11 15:50:26', NULL),
(13, 11, 10, 40, '2017-11-11 15:50:26', '2017-11-11 15:50:26', NULL),
(14, 12, 0, 37, '2017-11-12 14:48:10', '2017-11-12 14:48:10', NULL),
(15, 12, 0, 38, '2017-11-12 14:48:10', '2017-11-12 14:48:10', NULL),
(16, 12, 0, 39, '2017-11-12 14:48:10', '2017-11-12 14:48:10', NULL),
(17, 12, 4, 40, '2017-11-12 14:48:10', '2017-11-12 14:48:10', NULL),
(18, 13, 0, 37, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL),
(19, 13, 0, 38, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL),
(20, 13, 0, 39, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL),
(21, 13, 1, 40, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL),
(22, 13, 26, 41, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL),
(23, 13, 23, 42, '2017-12-08 08:19:19', '2017-12-08 08:19:19', NULL),
(24, 14, 0, 37, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL),
(25, 14, 0, 38, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL),
(26, 14, 0, 39, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL),
(27, 14, 0, 40, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL),
(28, 14, 0, 41, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL),
(29, 14, 2, 42, '2017-12-13 17:38:25', '2017-12-13 17:38:25', NULL),
(30, 15, 0, 37, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL),
(31, 15, 0, 38, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL),
(32, 15, 0, 39, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL),
(33, 15, 0, 40, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL),
(34, 15, 0, 41, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL),
(35, 15, 1, 42, '2018-01-10 14:39:13', '2018-01-10 14:39:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(15) NOT NULL,
  `admin_email_id` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `admin_email_id`) VALUES
(1, 'nithinraj.bgi@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stakeholders`
--

CREATE TABLE `stakeholders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `batch_no` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `returned_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `damaged_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sample_quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `available_quantity` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `packing_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `batch_no`, `category_id`, `quantity`, `returned_quantity`, `damaged_quantity`, `sample_quantity`, `available_quantity`, `value`, `packing_date`, `expiry_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '500-001', 2, 1500, '', '', '', 1500, 10125, '2017-03-05 00:00:00', '2018-03-05 00:00:00', '2017-03-05 17:03:25', '2017-03-05 17:14:18', '2017-03-05 17:14:18'),
(2, '1.5-002', 1, 500, '', '', '', 500, 3375, '2017-03-05 00:00:00', '2018-03-05 00:00:00', '2017-03-05 17:04:47', '2017-03-05 17:14:27', '2017-03-05 17:14:27'),
(3, '500-003', 2, 500, '', '', '', 500, 3375, '2017-03-01 00:00:00', '2018-03-01 00:00:00', '2017-03-05 17:10:36', '2017-03-05 17:14:31', '2017-03-05 17:14:31'),
(4, '1.5-004', 1, 500, '', '', '', 100, 3725, '2017-03-05 00:00:00', '2018-03-04 00:00:00', '2017-03-10 11:27:53', '2017-04-08 18:38:20', '2017-04-08 18:38:20'),
(5, '500-005', 2, 1000, '', '', '', 1000, 6750, '2017-03-05 00:00:00', '2018-03-04 00:00:00', '2017-03-10 11:29:21', '2017-04-08 18:38:24', '2017-04-08 18:38:24'),
(6, '1.5-006', 1, 1000, '', '', '', 0, 6750, '2017-03-08 00:00:00', '2018-03-07 00:00:00', '2017-03-10 11:32:57', '2017-04-08 18:38:32', '2017-04-08 18:38:32'),
(7, '1.5-007', 1, 500, '', '', '', 500, 3725, '2017-03-05 00:00:00', '2018-03-04 00:00:00', '2017-03-10 11:38:05', '2017-04-08 18:38:36', '2017-04-08 18:38:36'),
(8, '1.5-008', 1, 1000, '', '', '', 775, 100, '2017-03-23 00:00:00', '2018-03-22 00:00:00', '2017-03-10 13:46:00', '2017-04-08 18:38:40', '2017-04-08 18:38:40'),
(9, '500-009', 2, 2304, '', '', '', 88, 15552, '2017-04-03 00:00:00', '2018-04-02 00:00:00', '2017-04-14 18:59:57', '2017-09-07 15:31:44', '2017-09-07 15:31:44'),
(10, '330-010', 3, 1008, '', '', '', 704, 6804, '2017-04-19 00:00:00', '2018-04-18 00:00:00', '2017-04-24 18:20:07', '2017-09-07 15:31:48', '2017-09-07 15:31:48'),
(11, '1.5-011', 1, 1584, '', '', '', 1034, 10692, '2017-04-22 00:00:00', '2018-04-21 00:00:00', '2017-04-24 18:22:18', '2017-09-07 15:31:54', '2017-09-07 15:31:54'),
(12, '1.5-012', 1, 1584, '', '', '', 1584, 10692, '2017-04-27 00:00:00', '2018-04-26 00:00:00', '2017-04-28 19:06:02', '2017-09-07 15:32:05', '2017-09-07 15:32:05'),
(13, '500-013', 2, 2304, '', '', '', 2304, 15552, '2017-04-30 00:00:00', '2018-04-29 00:00:00', '2017-05-10 14:33:02', '2017-09-07 15:32:13', '2017-09-07 15:32:13'),
(14, '200-014', 5, 780, '', '', '', 780, 3315, '2017-05-03 00:00:00', '2018-05-02 00:00:00', '2017-05-10 14:41:34', '2017-09-07 15:32:18', '2017-09-07 15:32:18'),
(15, '200-015', 5, 780, '', '', '', 780, 3315, '2017-05-09 00:00:00', '2018-05-08 00:00:00', '2017-05-10 14:42:51', '2017-09-06 18:04:10', '2017-09-06 18:04:10'),
(16, '200-016', 5, 780, '', '', '', 780, 3315, '2017-05-11 00:00:00', '2018-05-10 00:00:00', '2017-05-12 09:24:37', '2017-09-06 18:03:58', '2017-09-06 18:03:58'),
(17, '1.5-017', 1, 100, '2', '', '2', 75, 980, '2017-09-07 00:00:00', '2018-09-07 00:00:00', '2017-09-07 15:36:27', '2017-11-05 08:37:55', '2017-11-05 08:37:55'),
(18, '500-018', 2, 250, '', '', '', 200, 1688, '2017-09-13 00:00:00', '2018-09-12 00:00:00', '2017-09-12 19:39:14', '2017-11-05 08:37:45', '2017-11-05 08:37:45'),
(19, '1.5-019', 1, 250, '', '', '', 250, 1688, '2017-09-13 00:00:00', '2018-08-13 00:00:00', '2017-09-12 19:39:17', '2017-09-12 19:41:33', '2017-09-12 19:41:33'),
(20, '500-020', 2, 500, '13', '', '', 500, 5000, '2017-10-11 00:00:00', '2018-10-30 00:00:00', '2017-10-11 15:33:54', '2017-11-05 08:37:27', '2017-11-05 08:37:27'),
(21, '500-021', 2, 350, '11', '11', '15', 324, 3500, '2017-10-11 00:00:00', '2018-10-23 00:00:00', '2017-10-11 15:37:02', '2017-11-05 08:37:09', '2017-11-05 08:37:09'),
(22, '500-022', 2, 640, '', '', '', 640, 6400, '2017-10-16 00:00:00', '2018-10-16 00:00:00', '2017-10-16 14:22:40', '2017-11-05 08:37:00', '2017-11-05 08:37:00'),
(23, '500-023', 2, 340, '', '8', '7', 340, 3400, '2017-10-22 00:00:00', '2018-10-22 00:00:00', '2017-10-22 04:13:45', '2017-11-05 08:36:44', '2017-11-05 08:36:44'),
(24, '1.5-024', 1, 550, '4', '9', '11', 430, 5000, '2017-10-22 00:00:00', '2018-10-22 00:00:00', '2017-10-22 04:17:09', '2017-11-05 08:36:36', '2017-11-05 08:36:36'),
(25, '1.5-025', 1, 700, '', '', '14', 700, 7000, '2017-10-25 00:00:00', '2018-10-25 00:00:00', '2017-10-25 05:23:04', '2017-11-05 08:36:19', '2017-11-05 08:36:19'),
(26, '1.5-026', 1, 100, '', '', '5', 67, 1000, '2017-11-05 00:00:00', '2018-11-05 00:00:00', '2017-11-05 08:39:38', '2017-11-05 09:14:23', NULL),
(27, '500-027', 2, 50, '', '7', '', 50, 500, '2017-11-05 00:00:00', '2018-11-05 00:00:00', '2017-11-05 08:40:18', '2017-11-05 08:47:52', NULL),
(28, '1.5-028', 1, 80, '', '', '', 25, 800, '2017-11-01 00:00:00', '2018-11-01 00:00:00', '2017-11-05 08:41:26', '2017-11-05 09:04:51', NULL),
(29, '1.5-029', 1, 150, '', '', '', 98, 1500, '2017-11-03 00:00:00', '2018-11-03 00:00:00', '2017-11-05 09:15:47', '2017-11-05 09:21:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `api_token` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `api_token`) VALUES
(1, 'Admin Istrator', 'admin@ajmal.com', '$2y$10$BjZTFNtQB7Pz2a9NY26t0O8i7f44Bw0kIpMdhAi8MVjsubkYmCgrC', 1, '0ee6e2c73eaf66013c8f29d5bcd5d7cd', 1, 'TQ2xC2tJykOEBHLNlHYA4txYPE2MSpiwfCdFMLo7J4HjqJ1zUQE3qY0xR3zV', '2017-02-16 18:14:45', '2018-01-10 16:08:40', NULL, 'a5PXUtY2MNgmyvi8RMV8ABATGZiYCyomxkny2nHeEu'),
(2, 'Default User', 'user@user.com', '$2y$10$SqJAeJlAiEZQV1tcFSm9QuHDcUxxi9V43bep.RG3kZvb6I7/C8ueK', 1, 'c957b310ee22a6e384ad9b85bf757b06', 1, NULL, '2017-02-16 18:14:45', '2017-02-16 18:14:45', NULL, 'a5Pi8RMV8ABATGZiYCyoY2MNgmyvmxkny2nHeEuXUt'),
(3, 'Ajmal Ansari', 'ajmalbinansari@gmail.com', '$2y$10$T5l.UrzAwMp8pfC/ihBC2eTESHRo3jy/DtaOzCHZ0zL/1beyjed8q', 1, '4bbe5a730a550f7258c8498de5e6e308', 1, 'PRoohxKYSAHnit6FyQCmGqdnGhSsmgqPPZA071yS7SVRQmXHOJpsRmYCXUJ9', '2017-09-03 16:20:39', '2017-10-10 18:32:40', NULL, 'a5PXUtY2MNgmyvi8RMATGZiYCyomxkny2nHeEuV8ABiIKnwTh0krMLNfrVWX'),
(4, 'fazil', 'fazil.anc@gmail.com', '$2y$10$2CMcOjxffZfSyg27IctBOenGoDgAYfLnVGBTFvcy8KlVDz5GDGmWe', 1, 'd83728a7b7e8e25092a0d9df9c2673c9', 1, '09enjeGjI0bqw3kBd7mz0v1BHCIHSFcpog5wPcLdEWO7WXOS6vbPh9PPYdzW', '2017-11-08 12:07:00', '2017-12-08 08:53:40', NULL, NULL),
(5, 'Test', 'test@test.com', '$2y$10$YMnfHCoAq9gZglhlMFph2e/IkAmqwS0cJxjbbLJuKbSjGb8mQqgJG', 1, '54fab5db645e671ca41f188bd7bb36bd', 1, 'gaQRQz4f9xWdba9TAtGErBDCpHQaJJ3L7w5SdBlBD2kVGVQXazsWu9Q5Nb5i', '2018-01-10 16:06:13', '2018-01-29 11:28:43', NULL, 'UQ7gu51an4cwPqsg4xVxHtyjQcgiUr4wCdryYFRDFDczGJEmdcgy5HsKAZOA');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `registration` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `name`, `registration`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'SHINAD', '33098', '2017-04-14 19:01:17', '2017-04-14 19:01:17', NULL),
(2, 'NIZAR', '33906', '2017-04-14 19:01:38', '2017-04-14 19:01:38', NULL),
(3, 'LOCAL SALE', '00000', '2017-04-14 19:02:07', '2017-04-14 19:02:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_settings`
--

CREATE TABLE `web_settings` (
  `id` int(11) NOT NULL,
  `field_name` varchar(250) NOT NULL,
  `field_value` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_settings`
--

INSERT INTO `web_settings` (`id`, `field_name`, `field_value`) VALUES
(1, 'tax', '5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assigned_roles_user_id_foreign` (`user_id`),
  ADD KEY `assigned_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_payments`
--
ALTER TABLE `credit_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_payment_details`
--
ALTER TABLE `credit_payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_categories`
--
ALTER TABLE `customer_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_phones`
--
ALTER TABLE `customer_phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_attendance`
--
ALTER TABLE `deli_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_feedback`
--
ALTER TABLE `deli_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_login`
--
ALTER TABLE `deli_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_product_order`
--
ALTER TABLE `deli_product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_staff_stocks`
--
ALTER TABLE `deli_staff_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_stocks`
--
ALTER TABLE `deli_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deli_user`
--
ALTER TABLE `deli_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employeetypes`
--
ALTER TABLE `employeetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expensetypes`
--
ALTER TABLE `expensetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_api`
--
ALTER TABLE `login_api`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_order`
--
ALTER TABLE `offer_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `out_stocks`
--
ALTER TABLE `out_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `out_stock_details`
--
ALTER TABLE `out_stock_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_dependencies`
--
ALTER TABLE `permission_dependencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_dependencies_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_dependencies_dependency_id_foreign` (`dependency_id`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`),
  ADD KEY `permission_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_buy`
--
ALTER TABLE `product_buy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order_detail`
--
ALTER TABLE `product_order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promocode`
--
ALTER TABLE `promocode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promocode_order`
--
ALTER TABLE `promocode_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pushnotification`
--
ALTER TABLE `pushnotification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_user`
--
ALTER TABLE `push_user`
  ADD PRIMARY KEY (`pushuser_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_details`
--
ALTER TABLE `sales_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_detail_outstock_details`
--
ALTER TABLE `sale_detail_outstock_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_foreign` (`user_id`);

--
-- Indexes for table `stakeholders`
--
ALTER TABLE `stakeholders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_settings`
--
ALTER TABLE `web_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `user_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `credit_payments`
--
ALTER TABLE `credit_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `credit_payment_details`
--
ALTER TABLE `credit_payment_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
--
-- AUTO_INCREMENT for table `customer_categories`
--
ALTER TABLE `customer_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_phones`
--
ALTER TABLE `customer_phones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `deli_attendance`
--
ALTER TABLE `deli_attendance`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `deli_feedback`
--
ALTER TABLE `deli_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `deli_product_order`
--
ALTER TABLE `deli_product_order`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401;
--
-- AUTO_INCREMENT for table `deli_staff_stocks`
--
ALTER TABLE `deli_staff_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deli_stocks`
--
ALTER TABLE `deli_stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `deli_user`
--
ALTER TABLE `deli_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employeetypes`
--
ALTER TABLE `employeetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expensetypes`
--
ALTER TABLE `expensetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_api`
--
ALTER TABLE `login_api`
  MODIFY `user_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `offer_order`
--
ALTER TABLE `offer_order`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT for table `out_stocks`
--
ALTER TABLE `out_stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `out_stock_details`
--
ALTER TABLE `out_stock_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `permission_dependencies`
--
ALTER TABLE `permission_dependencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `product_buy`
--
ALTER TABLE `product_buy`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=396;
--
-- AUTO_INCREMENT for table `product_order_detail`
--
ALTER TABLE `product_order_detail`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=555;
--
-- AUTO_INCREMENT for table `promocode`
--
ALTER TABLE `promocode`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `promocode_order`
--
ALTER TABLE `promocode_order`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `pushnotification`
--
ALTER TABLE `pushnotification`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `push_user`
--
ALTER TABLE `push_user`
  MODIFY `pushuser_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sales_details`
--
ALTER TABLE `sales_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `sales_payments`
--
ALTER TABLE `sales_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sale_detail_outstock_details`
--
ALTER TABLE `sale_detail_outstock_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stakeholders`
--
ALTER TABLE `stakeholders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `web_settings`
--
ALTER TABLE `web_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_dependencies`
--
ALTER TABLE `permission_dependencies`
  ADD CONSTRAINT `permission_dependencies_dependency_id_foreign` FOREIGN KEY (`dependency_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_dependencies_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
