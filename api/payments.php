<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/payments.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$payment = new PaymentList($db);

// get keywords
$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
$customerId=isset($_REQUEST["customerId"]) ? $_REQUEST["customerId"] : "";
if($jwt){
  try{
  //decode jwt detailes
  $decoded = JWT::decode($jwt, $key, array('HS256'));
  $user->id = $decoded->data->id;
  $stmt = $payment->paymentList($user->id,$customerId);
  $num = $stmt->rowCount();
  if($num>0){
    // orders array
    $orders_arr=array();
    $orders_arr["Orders"]=array();
$sum = 0;
$paidAmount =0;
$pendingAmount = 0;
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
      // extract row
      // this will make $row['name'] to
      // just $name only
      extract($row);
      $sum += $row['total_price'];
      $paidAmount += $row['amount_paid'];
      $pendingAmount += $row['pending_amount'];

      //$sumArray[$totalPrice]+=$value;

      $order_item=array(
        "order_id" => $order_id,
        "user_id" => $user_id,
        "quantity" =>$quantity,
        "product_id" => $product_id,
        "address" => $address,
        "delivered_status" => $delivered_status,
        "delivered_status" => $delivered_status,
        "address"=>$address_one.','.$address_two.','.$place,
        "total_price" => $total_price,
        "pending_amount" =>$total_price-$amount_paid
      );


      array_push($orders_arr["Orders"], $order_item);
    }
//echo $sum; die();

$pendingAmountVal = $sum-$paidAmount;
    $orders_arr['Response']['Total'] =$sum;
    $orders_arr['Response']['PaidAmount'] =$paidAmount;
    $orders_arr['Response']['PendingAmount'] =$pendingAmountVal;
    $orders_arr['Response']['status'] =1;

    // set response code - 200 OK
    http_response_code(200);

    // show products data in json format
    echo json_encode($orders_arr);



  }
  else{
    $status= array('status' => "0","message" => "No orders found.");
    // set response code - 404 Not found
    http_response_code(401);

    // tell the user no products found
    echo json_encode(
      array("Response"=> $status)
    );
  }
  //print_r($num); die();

}
// if decode fails, it means jwt is invalid
catch (Exception $e){

// set response code
http_response_code(401);

// show error message
echo json_encode(array(
    "message" => "invalid data.",
    "error" => $e->getMessage()
));
}
}
else{

    // set response code
    http_response_code(401);

    // tell the user access denied
    echo json_encode(array("message" => "Access denied."));
}
