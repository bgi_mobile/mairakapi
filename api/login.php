<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate user object
$user = new User($db);

// get posted data
$data = $_REQUEST;
//print_r($data);

// set product property values
$user->uid = $data['uid'];
//echo $user->password; exit;
//$userPassword = password_hash('ddd');
$password = $data['pwd'];
 //$password =password_hash($password);
 //echo $password;

$username_exists = $user->phoneNumberExists();

// generate json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
//print_r($data);
// check if email exists and if password is correct
if($username_exists && $password==$user->pwd){

    $token = array(
       "iss" => $iss,
       "aud" => $aud,
       "iat" => $iat,
       "nbf" => $nbf,
       "data" => array(
           "id" => $user->id,
           "uid" => $user->uid,
           "pwd" => $user->pwd
       )
    );


    // set response code
    http_response_code(200);
    $data =[];
    // generate jwt
    $jwt = JWT::encode($token, $key);
    $status= array('status' => "1","message" => "Successful login.","jwt" => $jwt);
    echo json_encode(
            array(
                "Response"=> $status
            )
        );

}

// login failed
else{
    $status= array('status' => "0","message" => "Login failed.");
    // set response code
    http_response_code(401);

    // tell the user login failed
    echo json_encode(array("Response"=> $status));
}
?>
