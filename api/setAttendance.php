<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/attendance.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$setAttendance = new SetAttendance($db);


// get keywords
$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
$status=isset($_REQUEST["status"]) ? $_REQUEST["status"] : "";
$date=isset($_REQUEST["date"]) ? $_REQUEST["date"] : "";
$time=isset($_REQUEST["time"]) ? $_REQUEST["time"] : "";
//echo $date.'df'.$time; die();
if($jwt && $status){
  try{
    //decode jwt detailes
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    $user->id = $decoded->data->id;
    if($setAttendance->updateAttendance($user->id,$status,$date,$time)){
      // set response code
      http_response_code(200);

      // response in json format
      echo json_encode(
        array(
          "message" => "Staff attendance was updated."
        )
      );



    }
    else{
      $status= array('status' => "0","message" => "No attendance updated.");
      // set response code - 404 Not found
      http_response_code(401);

      // tell the user no products found
      echo json_encode(
        array("Response"=> $status)
      );
    }
  }
  // if decode fails, it means jwt is invalid
  catch (Exception $e){

    // set response code
    http_response_code(401);

    // show error message
    echo json_encode(array(
      "message" => "invalid data.",
      "error" => $e->getMessage()
    ));
  }
}
else{

  // set response code
  http_response_code(401);

  // tell the user access denied
  echo json_encode(array("message" => "Access denied."));
}
