<?php
class CronAttendance{

    // database connection and table name
    private $conn;
    private $table_name = "deli_attendance";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;

    }


    function attendanceList(){
      $query = "SELECT
                  *
              FROM
                  " . $this->table_name . " da
                  where da.status = ?";



      // prepare query statement
      $stmt = $this->conn->prepare($query);
      $status  = "on";
      // bind id of product to be updated
        $stmt->bindParam(1, $status);

      // execute query
      $stmt->execute();

      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          extract($row);
            $id = $row['id'];
            $query1 = "UPDATE
            " . $this->table_name . "
            SET
            status = :status
            WHERE
            id = :id";

            // prepare query statement
            $stmt1 = $this->conn->prepare($query1);
            $status = "off";
            // bind new values
            $stmt1->bindParam(':status', $status);
            $stmt1->bindParam(':id', $id);
            // execute the query
          $stmt1->execute();

      }
      //return $stmt;
    }
 }
