<?php

//dashboard object
class SetAttendance{

    // database connection and table name
    private $conn;
    private $table_name = "deli_attendance";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    function updateAttendance($userid,$status,$date,$time){

      $userIdExist = $this->userIdExist($userid);
      if($userIdExist>0){
        $currentDate  = 	date('d-M-Y');
        $attendanceExist = $this->attendanceExist("".$currentDate."",$userid);

        if($attendanceExist>0){
          // update query
          $query = "UPDATE
          " . $this->table_name . "
          SET
          status = :status,
          updated_at =:updated_at,
          updated_time = :updated_time
          WHERE
          staff_id = :staff_id
          and created_at = :created_at";

          // prepare query statement
          $stmt = $this->conn->prepare($query);

          // sanitize
          //  $this->delivered_status=htmlspecialchars(strip_tags($status));
          //$this->id=htmlspecialchars(strip_tags($orderId));

          // bind new values
          $stmt->bindParam(':staff_id', $userid);
          $stmt->bindParam(':created_at', $currentDate);
          $stmt->bindParam(':status', $status);
          $stmt->bindParam(':updated_at', $date);
          $stmt->bindParam(':updated_time', $time);
          //print_r($stmt); die();
          // execute the query
          if($stmt->execute()){
            return true;
          }

          return false;
        }else {
          $query = "INSERT INTO
          " . $this->table_name . "
          SET
          staff_id=:staff_id, status=:status,created_at=:created_at,created_time=:created_time";

          // prepare query statement
          $stmt = $this->conn->prepare($query);

          // bind new values
          $stmt->bindParam(':staff_id', $userid);
          $stmt->bindParam(':status', $status);
          $stmt->bindParam(':created_at', $date);
          $stmt->bindParam(':created_time', $time);

          // execute the query
          if($stmt->execute()){
            return true;
          }

          return false;
        }

      }else {
        // query to insert record
        $query = "INSERT INTO
        " . $this->table_name . "
        SET
        staff_id=:staff_id, status=:status,created_at=:created_at,created_time=:created_time";
      }
      // prepare query statement
      $stmt = $this->conn->prepare($query);

      // bind new values
      $stmt->bindParam(':staff_id', $userid);
      $stmt->bindParam(':status', $status);
      $stmt->bindParam(':created_at', $date);
      $stmt->bindParam(':created_time', $time);

      // execute the query
      if($stmt->execute()){
        return true;
      }

      return false;

    }
    function userIdExist($userid){
      $query  = "select staff_id from
      deli_attendance
      where staff_id= ?";
      // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$userid);

    // execute query
    $stmt->execute();
    return $stmt->rowCount();
    // get retrieved row
  //  return  $row = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    function attendanceExist($currentDate,$userid){
      $query  = "select id from
      deli_attendance
      where created_at= ? and staff_id = ?";
      // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$currentDate);
    $stmt->bindParam(2,$userid);

    // execute query
    $stmt->execute();
    return $stmt->rowCount();
    }
  }
