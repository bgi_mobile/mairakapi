<?php

//dashboard object
class GetFeedback{

    // database connection and table name
    private $conn;
    private $table_name = "deli_feedback";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    function getfeedbackDetailes($userid,$customerId){
      $query  = "select comment,created_at from
      " . $this->table_name . "
      where staff_id= ?
      and user_id = ?";
      // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$userid);
    $stmt->bindParam(2,$customerId);
    $stmt->execute();
    // execute query
    return  $row = $stmt->fetch(PDO::FETCH_ASSOC);
    }
  }
