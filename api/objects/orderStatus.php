<?php

//dashboard object
class OrderStatus{

    // database connection and table name
    private $conn;
    private $table_name = "deli_product_order";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    // update the product
function updateOrderStatus($orderId,$status,$amountPaid,$userId){
      //get totalPrice amount
      $totalPriceDetailes  = $this->getTotalPrice($orderId);
      $productId = $totalPriceDetailes['product_id'];
      $quantity = $totalPriceDetailes['quantity'];
      $totalPrice  = $totalPriceDetailes['total_price'];
      $pendingAmount = $totalPrice-$amountPaid;

      $updateQuantity  = $this->updateQuantity($userId,$productId,$quantity);

    // update query
    $query = "UPDATE
                " . $this->table_name . "
            SET
                delivered_status = :delivered_status,
                amount_paid = :amount_paid,
                pending_amount = :pending_amount
            WHERE
                id = :id";

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // sanitize
  //  $this->delivered_status=htmlspecialchars(strip_tags($status));
//$this->id=htmlspecialchars(strip_tags($orderId));

    // bind new values
    $stmt->bindParam(':delivered_status', $status);
    $stmt->bindParam(':amount_paid', $amountPaid);
    $stmt->bindParam(':pending_amount', $pendingAmount);
    $stmt->bindParam(':id', $orderId);
    // execute the query
    if($stmt->execute()){
    //  $updateQuantity  = $this->updateQuantity($userId,$productId,$quantity);
        return true;
    }

    return false;
}
  function getTotalPrice($orderId){
    $query  = "select po.total_price,po.quantity,p.id as product_id from
    product_order_detail po
    LEFT JOIN
    product as p on po.product_id=p.id
    where po.product_order_id= ?";
    // prepare query statement
  $stmt = $this->conn->prepare( $query );

  // bind id of product to be updated
  $stmt->bindParam(1,$orderId);

  // execute query
  $stmt->execute();

  // get retrieved row
  return  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  }

  function updateQuantity($userId,$productId,$quantity){
    $totalQuantityDetailes  = $this->totalQuantity($userId,$productId);
    $totalQuantity = $totalQuantityDetailes['quantity'];
    $currentQuantity  = $totalQuantity-$quantity;
    $currentDate  = date('Y-m-d');
    // update query
    $query = "UPDATE
              deli_staff_stocks
            SET
                current_quantity = :current_quantity
            WHERE
                deli_staff_id = :deli_staff_id and
                product_id = :product_id
                and date(created_at)= :created_at";

    // prepare query statement
    $stmt = $this->conn->prepare($query);
    // bind new values
    $stmt->bindParam(':current_quantity', $currentQuantity);
    $stmt->bindParam(':deli_staff_id', $userId);
    $stmt->bindParam(':product_id', $productId);
    $stmt->bindParam(':created_at', $currentDate);
    // execute the query
    if($stmt->execute()){
        return true;
    }

    return false;
  }
  function totalQuantity($userId,$productId){
    $currentDate  = date('Y-m-d');
    $query  = "select ds.quantity from
    deli_staff_stocks ds
    where ds.deli_staff_id= ?
    and ds.product_id= ? and
    date(created_at)= ?";
    // prepare query statement
  $stmt = $this->conn->prepare( $query );

  // bind id of product to be updated
  $stmt->bindParam(1,$userId);
  $stmt->bindParam(2,$productId);
  $stmt->bindParam(3,$currentDate);

  // execute query
  $stmt->execute();

  // get retrieved row
  return  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  }
  }
