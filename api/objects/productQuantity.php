<?php

//dashboard object
class ProductQuantity{

    // database connection and table name
    private $conn;
    private $table_name = "product_order_detail";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    // update the product
function insertOrderQuantity($orderId,$productId,$quantity){
    $productPrice= $this->getproductPrice($productId);
    $price  = $productPrice['price'];
    $special_price = $productPrice['special_price'];
    $discount_price = $productPrice['discount_price'];
    $totalPrice  = $quantity*$price;
    // update query
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                product_order_id=:product_order_id, product_id=:product_id, quantity=:quantity, total_price=:total_price,
                 total_special_price=:total_special_price,total_discount_price=:total_discount_price";
    // prepare query statement
    $stmt = $this->conn->prepare($query);



    // sanitize
      $this->product_order_id=htmlspecialchars(strip_tags($orderId));
      $this->product_id=htmlspecialchars(strip_tags($productId));
      $this->quantity=htmlspecialchars(strip_tags($quantity));
      $this->total_price=htmlspecialchars(strip_tags($totalPrice));
      $this->total_special_price=htmlspecialchars(strip_tags($special_price));
      $this->total_discount_price=htmlspecialchars(strip_tags($discount_price));

      // bind values
      $stmt->bindParam(":product_order_id", $this->product_order_id);
      $stmt->bindParam(":product_id", $this->product_id);
      $stmt->bindParam(":quantity", $this->quantity);
      $stmt->bindParam(":total_price", $this->total_price);
      $stmt->bindParam(":total_special_price", $this->total_special_price);
      $stmt->bindParam(":total_discount_price", $this->total_discount_price);
    // execute the query
    if($stmt->execute()){
        return true;
    }

    return false;
}
  function getproductPrice($productId){
    $query  = "select price,special_price,discount_price from
    product
    where id= ?";
    // prepare query statement
  $stmt = $this->conn->prepare( $query );

  // bind id of product to be updated
  $stmt->bindParam(1,$productId);

  // execute query
  $stmt->execute();

  // get retrieved row
return  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  // set values to object properties
  //return $this->price = $row['price'];
  //return $this->special_price = $row['special_price'];

  }
  }
