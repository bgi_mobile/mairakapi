<?php

//dashboard object
class Dashboard{

    // database connection and table name
    private $conn;
    private $table_name = "deli_product_order";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    // read orders
function orderlist($userid,$longitude,$latitude,$status){
    //  echo $longitude; die();
    // select all query
    $query = "SELECT
                p.order_id,p.user_id,p.preorder_date,p.preorder_time,p.total_price,p.address,p.location_lat,
                p.location_long,p.delivered_status,p.order_status,c.name,c.address_one,c.address_two,c.place
            FROM
                " . $this->table_name . " p
                LEFT JOIN
                    customers c
                        ON p.user_id = c.id
                where p.deli_staff_id = ?
                ";

    if(!empty($status)){
      $query .=" and p.delivered_status = ?";
    }

//     else {
//       $query .=" and p.delivered_status = :Ordered";
// }

    // prepare query statement
    $stmt = $this->conn->prepare($query);

    // bind id of product to be updated
      $stmt->bindParam(1, $userid);
        if(!empty($status)){
      $stmt->bindParam(2, $status);
    }
    // else {
    //   $stmt->bindParam(2, $status);
    // }

    // execute query
    $stmt->execute();

    return $stmt;
}



function customersDetailes($userid,$lon,$lat){
  //set @lat=51.891648;
  //set @lng=0.244799;
  $query = "SELECT c.id, p.id as order_id,c.name, p.location_lat, p.location_long,
  111.045 * DEGREES(ACOS(COS(RADIANS(location_lat)) * COS(RADIANS(location_lat)) * COS(RADIANS(location_long) - RADIANS($lon)) + SIN(RADIANS($lat)) * SIN(RADIANS(location_lat)))) AS distance_in_km
  FROM customers AS c
  LEFT join deli_product_order p ON p.user_id = c.id
  where p.deli_staff_id = ? and p.delivered_status = ?
  GROUP by c.id
  ORDER BY distance_in_km ASC LIMIT 0,10";

  // prepare query statement
  $stmt = $this->conn->prepare($query);
  $deliveredStatus = 'Ordered';
  // bind id of product to be updated
    $stmt->bindParam(1, $userid);
    $stmt->bindParam(2, $deliveredStatus);
    // execute query
    $stmt->execute();

    return $stmt;
}



  }
