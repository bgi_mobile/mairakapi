<?php

//dashboard object
class AddOrder{

  // database connection and table name
  private $conn;
  private $table_name = "deli_product_order";

  // object properties
  public $id;
  public $uid;
  public $pwd;
  public $created;

  // constructor
  public function __construct($db){
    $this->conn = $db;
  }

  function addOrderData($deliveryStaffId,$productId,$quantity,$userid,$location_lat,$location_long){
    $getLastOrderId = $this->lastOrderId();
    $getPriceDetailes  = $this->getPrice($productId);
    $customerDetailes  = $this->customerDetailes($userid);
    $address_one = $customerDetailes['address_one'];
    $address_two = $customerDetailes['address_two'];
    $place = $customerDetailes['place'];
    $zip_code = $customerDetailes['zip_code'];
    $address  = $address_one.','.$address_two.','.$place.','.$zip_code;
    $price =  $getPriceDetailes['price'];
    $special_price =  $getPriceDetailes['special_price'];
    $discount_price =  $getPriceDetailes['discount_price'];

    $totalPrice = $price*$quantity;
    $lastOrderId = $getLastOrderId['orderId'];
    $orderId =$lastOrderId+1;
    $query = "INSERT INTO
    " . $this->table_name . "
    SET
    order_id= :order_id,user_id	=:user_id,total_price =:total_price,address= :address,
    location_lat =:location_lat,location_long = :location_long,delivered_status = :delivered_status,
    order_status = :order_status, deli_staff_id = :deli_staff_id";

    // prepare query statement
    $stmt = $this->conn->prepare($query);
    $delivered_status  = 'Ordered';
    $Order  = 'Order';


    // bind new values
    $stmt->bindParam(':order_id', $orderId);
    $stmt->bindParam(':user_id', $userid);
    $stmt->bindParam(':total_price', $totalPrice);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':location_lat', $location_lat);
    $stmt->bindParam(':location_long', $location_long);
    $stmt->bindParam(':delivered_status', $delivered_status);
    $stmt->bindParam(':order_status', $Order);
    $stmt->bindParam(':deli_staff_id', $deliveryStaffId);

    // execute the query
    if($stmt->execute()){
      $getLastInsertId  = $this->lastInsertId($orderId);
      $id = $getLastInsertId['id'];
      $productOrderDetailes  = $this->productOrderDetailes($id,$productId,$quantity,$totalPrice,$special_price,$discount_price);
      return true;
    }

    return false;
  }

  function lastOrderId(){
    $query = "SELECT max(order_id) as orderId FROM " . $this->table_name . "";
    $stmt = $this->conn->prepare($query);
    $stmt->execute();
    return $row = $stmt->fetch(PDO::FETCH_ASSOC);

  }
  function getPrice($productId){
    $query  = "select price,special_price,discount_price from
    product
    where id= ?";
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$productId);

    // execute query
    $stmt->execute();
    return $row = $stmt->fetch(PDO::FETCH_ASSOC);
  }
  function customerDetailes($userid){
    $query  = "select address_one,address_two,place,zip_code from
    customers
    where id= ?";
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$userid);

    // execute query
    $stmt->execute();
    return $row = $stmt->fetch(PDO::FETCH_ASSOC);
  }
  function productOrderDetailes($id,$productId,$quantity,$totalPrice,$special_price,$discount_price){
    $query = "INSERT INTO
    product_order_detail
    SET
    product_order_id= :product_order_id,product_id	=:product_id,quantity	 =:quantity	,total_price= :total_price,
    total_special_price =:total_special_price,total_discount_price = :total_discount_price";
    $stmt = $this->conn->prepare($query);
    $stmt->bindParam(':product_order_id', $id);
    $stmt->bindParam(':product_id', $productId);
    $stmt->bindParam(':quantity', $quantity);
    $stmt->bindParam(':total_price', $totalPrice);
    $stmt->bindParam(':total_special_price', $special_price);
    $stmt->bindParam(':total_discount_price', $discount_price);

    // execute the query
    if($stmt->execute()){
      return true;
    }

    return false;
  }

  function lastInsertId($orderId){
    $query  = "select id from
    " . $this->table_name . "
    where order_id= ?";
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$orderId);

    // execute query
    $stmt->execute();
    return $row = $stmt->fetch(PDO::FETCH_ASSOC);
  }

}
