<?php
//dashboard object
class nearByCustomers{

    // database connection and table name
    private $conn;
    private $table_name = "deli_product_order";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    function customersDetailes($userid,$lon,$lat){
      //set @lat=51.891648;
      //set @lng=0.244799;
      $query = "SELECT c.id, p.id as order_id,c.name, p.location_lat, p.location_long,
      111.045 * DEGREES(ACOS(COS(RADIANS(location_lat)) * COS(RADIANS(location_lat)) * COS(RADIANS(location_long) - RADIANS($lon)) + SIN(RADIANS($lat)) * SIN(RADIANS(location_lat)))) AS distance_in_km
      FROM customers AS c
      LEFT join deli_product_order p ON p.user_id = c.id
      where p.deli_staff_id = ? and p.delivered_status = ?
      GROUP by c.id
      ORDER BY distance_in_km ASC LIMIT 0,10";

      // prepare query statement
      $stmt = $this->conn->prepare($query);
      $deliveredStatus = 'Ordered';
      // bind id of product to be updated
        $stmt->bindParam(1, $userid);
        $stmt->bindParam(2, $deliveredStatus);
        // execute query
        $stmt->execute();

        return $stmt;
    }
  }
