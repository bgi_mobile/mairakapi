<?php

//dashboard object
class GetAttendance{

    // database connection and table name
    private $conn;
    private $table_name = "deli_attendance";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    function getAttendanceDetailes($userid){
      $currentDate  = 	date('d-M-Y'); 
      //date('d-F-Y');
      $query  = "select status from
      deli_attendance
      where staff_id= ?
      and created_at = ?";
      // prepare query statement
    $stmt = $this->conn->prepare( $query );
    // bind id of product to be updated
    $stmt->bindParam(1,$userid);
    $stmt->bindParam(2,$currentDate);
    $stmt->execute();
  //  print_r($stmt); die();
    // execute query
    return $row = $stmt->fetch(PDO::FETCH_ASSOC);

    }
  }
