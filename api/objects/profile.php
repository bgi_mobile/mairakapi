<?php

//dashboard object
class Profile{

  // database connection and table name
  private $conn;
  private $table_name = "deli_login";

  // object properties
  public $id;
  public $uid;
  public $pwd;
  public $created;

  // constructor
  public function __construct($db){
    $this->conn = $db;
  }

  function profileDetailes($userId){
    $query  = "select * from
    " . $this->table_name . "
    where id= ?";
    // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$userId);

    // execute query
    $stmt->execute();
    // get retrieved row
    return  $row = $stmt->fetch(PDO::FETCH_ASSOC);
  }

}
