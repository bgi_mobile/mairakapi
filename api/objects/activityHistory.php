<?php

//dashboard object
class ActivityHistory{

    // database connection and table name
    private $conn;
    private $table_name = "deli_attendance";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    function ActivityHistoryList($userid){
      $query  = "select * from
      " . $this->table_name . "
      where staff_id= ?";
      // prepare query statement
    $stmt = $this->conn->prepare( $query );

    // bind id of product to be updated
    $stmt->bindParam(1,$userid);
    $stmt->execute();
    // execute query
    return $stmt;
    //return  $row = $stmt->fetch(PDO::FETCH_ASSOC);
    }
  }
