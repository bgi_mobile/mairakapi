<?php

//dashboard object
class Feedback{

    // database connection and table name
    private $conn;
    private $table_name = "deli_feedback";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    // create product
function create($userid,$customerId,$comment,$status='1'){

    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                staff_id=:staff_id, user_id=:user_id, comment=:comment, status=:status";

    // prepare query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->staff_id=htmlspecialchars(strip_tags($userid));
    $this->user_id=htmlspecialchars(strip_tags($customerId));
    $this->comment=htmlspecialchars(strip_tags($comment));
    $this->status=htmlspecialchars(strip_tags($status));

    // bind values
    $stmt->bindParam(":staff_id", $this->staff_id);
    $stmt->bindParam(":user_id", $this->user_id);
    $stmt->bindParam(":comment", $this->comment);
    $stmt->bindParam(":status", $this->status);

    // execute query
    if($stmt->execute()){
        return true;
    }

    return false;

}

  }
