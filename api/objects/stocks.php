<?php

//dashboard object
class Stocks{

    // database connection and table name
    private $conn;
    private $table_name = "deli_staff_stocks";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    function stocksList($staffId){
      $query = "SELECT
                  p.id,p.batch_no,p.product_id,p.quantity,p.current_quantity,p.deli_staff_id,pr.product_name,pr.images,pr.price
              FROM
                  " . $this->table_name . " p
                  LEFT JOIN
                      product pr
                          ON p.product_id = pr.id
                  where p.deli_staff_id = ?
                  and `current_quantity`!=0";

      // prepare query statement
      $stmt = $this->conn->prepare($query);
      // bind id of product to be updated
        $stmt->bindParam(1, $staffId);
      //  print_r($stmt); die();

      // execute query
      $stmt->execute();

      return $stmt;
    }
  }
