<?php
// 'user' object
class User{

    // database connection and table name
    private $conn;
    private $table_name = "deli_login";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }


    // check if given email exist in the database
function phoneNumberExists(){

    // query to check if email exists
    $query = "SELECT id, uid,created, pwd
            FROM " . $this->table_name . "
            WHERE uid = ?
            LIMIT 0,1";

    // prepare the query
    $stmt = $this->conn->prepare( $query );

    // sanitize
    $this->uid=htmlspecialchars(strip_tags($this->uid));

    // bind given email value
    $stmt->bindParam(1, $this->uid);

    // execute the query
    $stmt->execute();

    // get number of rows
    $num = $stmt->rowCount();
    // if email exists, assign values to object properties for easy access and use for php sessions
    if($num>0){

        // get record details / values
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // assign values to object properties
        $this->id = $row['id'];
        $this->uid = $row['uid'];
        $this->pwd = $row['pwd'];
        $this->created = $row['created'];
        // return true because email exists in the database
        //echo $this->password;
        return true;
    }

    // return false if email does not exist in the database
    return false;
}
}
