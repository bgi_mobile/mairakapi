<?php
class StaffStocks{

    // database connection and table name
    private $conn;
    private $table_name = "deli_staff_stocks";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;

    }
    function assignStocks($userid){
      $orderList = $this->getOrderList($userid=1);
      $orderArr = array();
      $orderArr["Orders"]=array();
      while ($row = $orderList->fetch(PDO::FETCH_ASSOC)){ print_r($row); die();
        extract($row);

        $customers=array(
          "id"=>$id,
          "order_id"=>$order_id,
          "name" => $name,
          "location_lat" => $location_lat,
          "location_long" => $location_long,
          //"distance_in_km"=>$distance_in_km
        );

        array_push($orderArr["Orders"], $customers);
      }
      print_r($orderArr); die();

        print_r($orderList);
    }
    function getOrderList($userid){
      $query = "SELECT
                  p.deli_staff_id,p.order_id,p.user_id,c.phone,p.preorder_date,p.preorder_time,p.total_price,p.address,p.location_lat,
                  p.location_long,p.delivered_status,p.order_status,c.name,c.address_one,c.address_two,c.place

              FROM
                  deli_product_order p
                  LEFT JOIN
                      customers c
                          ON p.user_id = c.id
                  where p.deli_staff_id = ? and p.delivered_status = ?
                  ";
                  // prepare query statement
                  $stmt = $this->conn->prepare($query);
                  $deliveredStatus = 'Ordered';
                  // bind id of product to be updated
                    $stmt->bindParam(1, $userid);
                    $stmt->bindParam(2, $deliveredStatus);

                  // execute query
                  $stmt->execute();

                  return $stmt;
    }

    function attendanceList(){
      $query = "SELECT
                  *
              FROM
                  " . $this->table_name . " da
                  where da.status = ?";



      // prepare query statement
      $stmt = $this->conn->prepare($query);
      $status  = "on";
      // bind id of product to be updated
        $stmt->bindParam(1, $status);

      // execute query
      $stmt->execute();

      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
          extract($row);
            $id = $row['id'];
            $query1 = "UPDATE
            " . $this->table_name . "
            SET
            status = :status
            WHERE
            id = :id";

            // prepare query statement
            $stmt1 = $this->conn->prepare($query1);
            $status = "off";
            // bind new values
            $stmt1->bindParam(':status', $status);
            $stmt1->bindParam(':id', $id);
            // execute the query
          $stmt1->execute();

      }
      //return $stmt;
    }
 }
