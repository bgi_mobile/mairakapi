<?php

//dashboard object
class PendingOrder{

    // database connection and table name
    private $conn;
    private $table_name = "deli_product_order";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }

    // read orders
function pendingOrderlist($userid,$longitude,$latitude){
    //  echo $longitude; die();
    // select all query
    $query = "SELECT
                p.order_id,p.user_id,c.phone,p.preorder_date,p.preorder_time,p.total_price,p.address,p.location_lat,
                p.location_long,p.delivered_status,p.order_status,c.name,c.address_one,c.address_two,c.place,
                DEGREES(ACOS(COS(RADIANS(location_lat)) * COS(RADIANS(location_lat)) * COS(RADIANS(location_long) - RADIANS($longitude)) + SIN(RADIANS($latitude)) * SIN(RADIANS(location_lat)))) AS distance_in_km
            FROM
                " . $this->table_name . " p
                LEFT JOIN
                    customers c
                        ON p.user_id = c.id
                where p.deli_staff_id = ? and p.delivered_status = ?
                group by p.order_id";



    // prepare query statement
    $stmt = $this->conn->prepare($query);
    $deliveredStatus = 'Ordered';
    // bind id of product to be updated
      $stmt->bindParam(1, $userid);
      $stmt->bindParam(2, $deliveredStatus);
      //print_r($stmt); die();

    // execute query
    $stmt->execute();

    return $stmt;
}



  }
