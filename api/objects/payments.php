<?php

//dashboard object
class PaymentList{

    // database connection and table name
    private $conn;
    private $table_name = "deli_product_order";

    // object properties
    public $id;
    public $uid;
    public $pwd;
    public $created;

    // constructor
    public function __construct($db){
        $this->conn = $db;
    }
    function paymentList($userId,$customerId){
      $query  = "SELECT
      d.id as orderid,d.order_id,d.user_id,d.delivered_status,c.id as cusid,c.name,c.phone,c.address_one,c.address_two,c.place,d.location_lat,d.location_long,
                  pd.product_id,pd.quantity,pd.total_price as total_price, d.amount_paid,d.pending_amount
      FROM
      " . $this->table_name . " d
      LEFT JOIN
      customers as c on d.user_id=c.id
      INNER JOIN
       product_order_detail pd on d.id=pd. product_order_id
      where d.deli_staff_id = ?
      and  c.id = ?
      and d.delivered_status= ?
      GROUP by d.id
      LIMIT 0, 25";
      // $query  = "SELECT
      // d.id as orderid,d.order_id,c.id as cusid,pd.product_order_id,pd.product_id as productid,c.name,c.phone,c.address_one,c.address_two,c.place,d.location_lat,d.location_long,
      //             p.product_name,pd.quantity,pd.total_price as price, sum(pd.total_price) as total_price
      // FROM
      // " . $this->table_name . " d
      // LEFT JOIN
      // customers as c on d.user_id=c.id
      // INNER JOIN
      //  product_order_detail pd on d.id=pd. product_order_id
      // LEFT JOIN
      //  product as p on pd.product_id=p.id
      // where d.deli_staff_id = ?
      // and c.id = ?
      // and d.delivered_status= ?
      // LIMIT 0, 25";
      // prepare query statement
      $stmt = $this->conn->prepare($query);
      $deliveredStatus = 'completed';
      // bind id of product to be updated
        $stmt->bindParam(1, $userId);
        $stmt->bindParam(2, $customerId);
        $stmt->bindParam(3, $deliveredStatus);
      // execute query
      $stmt->execute();
      //print_r($stmt); die();
      return $stmt;
    }
  }
