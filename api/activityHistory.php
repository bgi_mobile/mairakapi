<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/activityHistory.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$activityHistory = new ActivityHistory($db);

// get keywords
$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";

if($jwt){
  try{
  //decode jwt detailes
  $decoded = JWT::decode($jwt, $key, array('HS256'));
  $user->id = $decoded->data->id;
  $stmt = $activityHistory->ActivityHistoryList($user->id);
  $num = $stmt->rowCount();
  if($num>0){
    // orders array
    $activityHistory_arr=array();
    $activityHistory_arr["activityHistory"]=array();

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ //print_r($row); die();
      // extract row
      // this will make $row['name'] to
      // just $name only
      extract($row);

      // $created_time  = $created_at;
      // $updated_time  = $updated_at;
// $created_at= date('Y-m-d', strtotime($created_at));
// $created_ats = date('d-F-Y', strtotime($created_at));
//
// $updated_at = date('Y-m-d', strtotime($updated_at));
// $updated_ats = date('d-F-Y', strtotime($updated_at));

      $activity=array(
        "created_date"=>$created_at,
        "updated_date"=>$updated_at,
        "created_time" => $created_time,
        "updated_time" => $updated_time
        //date('H:i:s', strtotime($updated_time))
      );
      //.print_r($activity);die();

      array_push($activityHistory_arr["activityHistory"], $activity);
    }


    $activityHistory_arr['Response']['status'] =1;

    // set response code - 200 OK
    http_response_code(200);

    // show products data in json format
    echo json_encode($activityHistory_arr);



  }
  else{
    $status= array('status' => "0","message" => "No orders found.");
    // set response code - 404 Not found
    http_response_code(401);

    // tell the user no products found
    echo json_encode(
      array("Response"=> $status)
    );
  }
  //print_r($num); die();

}
// if decode fails, it means jwt is invalid
catch (Exception $e){

// set response code
http_response_code(401);

// show error message
echo json_encode(array(
    "message" => "invalid data.",
    "error" => $e->getMessage()
));
}
}
else{

    // set response code
    http_response_code(401);

    // tell the user access denied
    echo json_encode(array("message" => "Access denied."));
}
