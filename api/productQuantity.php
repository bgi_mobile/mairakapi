<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/productQuantity.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$OrderStatus = new ProductQuantity($db);

// get keywords
//$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
$orderId=isset($_REQUEST["orderId"]) ? $_REQUEST["orderId"] : "";
$productId=isset($_REQUEST["productId"]) ? $_REQUEST["productId"] : "";
$quantity=isset($_REQUEST["quantity"]) ? $_REQUEST["quantity"] : "";

if($orderId && $quantity && $quantity){
  //try{

    // create the product
    if($OrderStatus->insertOrderQuantity($orderId,$productId,$quantity)){

      // set response code
      http_response_code(200);

      // response in json format
      echo json_encode(
        array(
          "message" => "Quantity was added."
        )
      );
    }
    else{
      $status= array('status' => "0","message" => "No Quantity added.");
      // set response code - 404 Not found
      http_response_code(401);

      // tell the user no products found
      echo json_encode(
        array("Response"=> $status)
      );
    }
    //print_r($num); die();

  //

}
else{

  // set response code
  http_response_code(401);

  // tell the user access denied
  echo json_encode(array("message" => "Access denied."));
}
