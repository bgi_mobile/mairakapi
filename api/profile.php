<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/profile.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$Profile = new Profile($db);

$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";

if($jwt){
  try{
    //decode jwt detailes
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    $user->id = $decoded->data->id;
    $stmt = $Profile->profileDetailes($user->id);
    $name = $stmt['name'];
    $created = $stmt['created'];
    $modified = $stmt['modified'];
    $phone = $stmt['phone'];
    $address = $stmt['address'];
    if($stmt){
      // orders array
      $user_arr=array();
      $user_arr["userDetailes"]=array();


      $order_item=array(
        "name" => $name,
        "phone" => $phone,
        "address" => $address,
        "created_date" => date('Y-m-d', strtotime($created)),
        "updated_date"=>date("Y-m-d",strtotime($modified)),
        "created_time" => date('Y-m-d', strtotime($created)),
        "updated_time"=>date("H:i:s",strtotime($modified))
      );

      array_push($user_arr["userDetailes"], $order_item);
      //  array_push($orders_arr["TotalPrice"], $order_item);
      $user_arr['Response']['status'] =1;

      // set response code - 200 OK
      http_response_code(200);

      // show products data in json format
      echo json_encode($user_arr);

    }else {
      $status= array('status' => "0","message" => "No data found.");
      // set response code - 404 Not found
      http_response_code(401);

      // tell the user no products found
      echo json_encode(
        array("Response"=> $status)
      );
    }
  }
  // if decode fails, it means jwt is invalid
  catch (Exception $e){

    // set response code
    http_response_code(401);

    // show error message
    echo json_encode(array(
      "message" => "invalid data.",
      "error" => $e->getMessage()
    ));
  }

}
else{

  // set response code
  http_response_code(401);

  // tell the user access denied
  echo json_encode(array("message" => "Access denied."));
}
