<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/lastOrder.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$lastOrderdetailes = new Lastorder($db);

$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
$customerId = isset($_REQUEST["customerId"]) ? $_REQUEST["customerId"] : "";

if($jwt){
  try{
    //decode jwt detailes
    $decoded = JWT::decode($jwt, $key, array('HS256'));
    $user->id = $decoded->data->id;
    $stmt = $lastOrderdetailes->lastOrderData($user->id,$customerId);
    $num = $stmt->rowCount();
    if($num>0){
      // orders array
      $orders_arr=array();
      $orders_arr["Orders"]=array();
      //if(!empty($product_name)){
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $sum += $row['price'];
        $name = isset($name)? $name:'';
        $cutomersItem  = array(
          "orderid" =>$orderid,
          "name" => $name,
          "phone" => $phone,
          "address" => $address_one.','.$address_two.','.$place,
          "location_lat" => $location_lat,
          "location_long" => $location_long,
        );
        $order_item=array(
          "productid" => $productid,
          "product_name" => $product_name,
          "quantity" => $quantity,
          "images"=> "http://inviewmart.com/mairak_api/assets/images/product/".$images,
          "price" =>$price
        );

        array_push($orders_arr["Orders"], $order_item);
      //  array_push($orders_arr["TotalPrice"], $order_item);
      }
      //}
          $cutomersItem["total"] = $sum;
          $orders_arr['customerDetailes']=$cutomersItem;
          $orders_arr['Response']['status'] =1;

          // set response code - 200 OK
          http_response_code(200);

          // show products data in json format
          echo json_encode($orders_arr);

    }else {
      $status= array('status' => "0","message" => "No orders found.");
      // set response code - 404 Not found
      http_response_code(401);

      // tell the user no products found
      echo json_encode(
        array("Response"=> $status)
      );
    }
  }
  // if decode fails, it means jwt is invalid
  catch (Exception $e){

  // set response code
  http_response_code(401);

  // show error message
  echo json_encode(array(
      "message" => "invalid data.",
      "error" => $e->getMessage()
  ));
  }

}
else{

    // set response code
    http_response_code(401);

    // tell the user access denied
    echo json_encode(array("message" => "Access denied."));
}
