<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/completedOrder.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$CompletedOrder = new CompletedOrder($db);

// get keywords
$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
if($jwt){
  try{
  //decode jwt detailes
  $decoded = JWT::decode($jwt, $key, array('HS256'));
  $user->id = $decoded->data->id;
  $stmt = $CompletedOrder->completedOrderlist($user->id);
  $num = $stmt->rowCount();
  if($num>0){
    // orders array
    $orders_arr=array();
    $orders_arr["Orders"]=array();

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){ //print_r($row); die();
      // extract row
      // this will make $row['name'] to
      // just $name only
      extract($row);
      if(!empty($name)){
      $order_item=array(
        "order_id" => $order_id,
        "user_id" => $user_id,
        "preorder_date" => $preorder_date,
        "preorder_time" => $preorder_time,
        "total_price" => $total_price,
        "address" => $address,
        "location_lat" => $location_lat,
        "location_long" => $location_long,
        "delivered_status" => $delivered_status,
        "order_status" => $order_status,
        "name" => $name,
        "phone"=>$phone,
        "address"=>$address_one.','.$address_two.','.$place,
        "distance_in_km"=>round($distance_in_km,2)
      );
}
      array_push($orders_arr["Orders"], $order_item);
    }


    $orders_arr['Response']['status'] =1;

    // set response code - 200 OK
    http_response_code(200);

    // show products data in json format
    echo json_encode($orders_arr);



  }
  else{
    $status= array('status' => "0","message" => "No orders found.");
    // set response code - 404 Not found
    http_response_code(401);

    // tell the user no products found
    echo json_encode(
      array("Response"=> $status)
    );
  }
  //print_r($num); die();

}
// if decode fails, it means jwt is invalid
catch (Exception $e){

// set response code
http_response_code(401);

// show error message
echo json_encode(array(
    "message" => "invalid data.",
    "error" => $e->getMessage()
));
}
}
else{

    // set response code
    http_response_code(401);

    // tell the user access denied
    echo json_encode(array("message" => "Access denied."));
}
