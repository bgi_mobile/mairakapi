<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');


// required to encode json web token
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// files needed to connect to database
include_once 'config/database.php';
include_once 'objects/nearByCustomers.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
// prepare dashboard object
$nearByCustomers = new nearByCustomers($db);

$jwt=isset($_REQUEST["jwt"]) ? $_REQUEST["jwt"] : "";
$longitude=isset($_REQUEST["longitude"]) ? $_REQUEST["longitude"] : "";
$latitude=isset($_REQUEST["latitude"]) ? $_REQUEST["latitude"] : "";
$decoded = JWT::decode($jwt, $key, array('HS256'));
$user->id = $decoded->data->id;
$stmt = $nearByCustomers->customersDetailes($user->id,$longitude,$latitude);
$num = $stmt->rowCount();

if($num>0){
  $customers_arr=array();
  $customers_arr["Customers"]=array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    //  print_r($row); die();

    extract($row);

    $customers=array(
      "id"=>$id,
      "order_id"=>$order_id,
      "name" => $name,
      "location_lat" => $location_lat,
      "location_long" => $location_long
    );

    array_push($customers_arr["Customers"], $customers);
  }
  $customers_arr['Response']['status'] =1;
  // set response code - 200 OK
  http_response_code(200);

  // show products data in json format
  echo json_encode($customers_arr);
}else {
  $status= array('status' => "0","message" => "No customers found.");
  // set response code - 404 Not found
  http_response_code(401);

  // tell the user no products found
  echo json_encode(
    array("Response"=> $status)
  );
}
